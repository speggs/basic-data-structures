#include "printers.h"
using namespace std;

int TestNo = 1;

void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void printVector(vector<vector<int>> vec){
	int vecSize = vec.size();
	for (int i = 0; i < vecSize; i++){
		printVector(vec.at(i));
		printf("\n");
	}
}