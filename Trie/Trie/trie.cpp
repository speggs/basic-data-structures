#include "trie.h"
using namespace std;

/** Initialize your data structure here. */
Trie::Trie() {
	root = new TrieNode("");
}

/** Inserts a word into the trie. */
void Trie::insert(string word) {
}

/** Returns if the word is in the trie. */
bool Trie::search(string word) {
	return false;
}

/** Returns if there is any word in the trie that starts with the given prefix. */
bool Trie::startsWith(string prefix) {
	return false;
}