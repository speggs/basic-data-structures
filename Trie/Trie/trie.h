#ifndef TRIE_H
#define TRIE_H

#include <stddef.h>
#include "stddef.h"
#include <string>
#include <vector>
#include <queue>
#include <unordered_map>

struct TrieNode {
	unordered_map<char, TrieNode*> children;
	std::string prefix;
	bool isWord;
	TrieNode(std::string str) : prefix(str), isWord(false){}
	TrieNode() : prefix("", isWord(false)){}
};

class Trie {
public:
	Trie();
	void insert(std::string word);
	bool search(std::string word);
	bool startsWith(std::string prefix);
private:
	TrieNode * root;
};

#include "printers.h"
#include "utils.h"


#endif //TRIE_H