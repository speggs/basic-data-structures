#ifndef PRINTERS_H
#define PRINTERS_H

#include <stdio.h>
#include <iostream>

#include "trie.h"

//printers
void printVector(std::vector<int> vec);
void printVector(std::vector<std::vector<int>> vec);

//test printers
#endif //PRINTERS_H