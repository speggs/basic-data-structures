import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
/*
Given a matrix of m x n elements (m rows, n columns), return all elements of the matrix in spiral order.

Example 1:

Input:
[
 [ 1, 2, 3 ],
 [ 4, 5, 6 ],
 [ 7, 8, 9 ]
]
Output: [1,2,3,6,9,8,7,4,5]

Example 2:

Input:
[
  [1, 2, 3, 4],
  [5, 6, 7, 8],
  [9,10,11,12]
]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]

How do we know when we are done? Simple counter might work
Edge cases: right, bottom left edge
Need to make sure we don't hit same row/column twice
how do we keep track
*/
public class SpiralMatrixSln {

    public List<Integer> spiralOrder(int[][] matrix) {
        List<Integer> ret = new ArrayList<Integer>();
        int m = matrix.length;
        if(m == 0){
        	return ret;
        }
        int n = matrix[0].length;
        if(n == 0){
        	return ret;
        }
        int topEdgeIndex = -1;
        int rightEdgeIndex = n;
        int bottomEdgeIndex = m;
        int leftEdgeIndex = -1;
        char direction = 'R';
        int i = 0, j = 0, count = 0;
        
        
        while(count < m*n){
        	ret.add(matrix[i][j]);
        	count++;
        	switch(direction){
        	case 'R':
        		if(j < rightEdgeIndex - 1){
        			j++;
        		}else{
        			direction = 'D';
        			i++;
        			topEdgeIndex++;
        		}
        		break;
        	case 'D':
        		if(i < bottomEdgeIndex - 1){
        			i++;
        		}else{
        			direction = 'L';
        			j--;
        			rightEdgeIndex--;
        		}
        		break;
        	case 'L':
        		if(j > leftEdgeIndex + 1){
        			j--;
        		}else{
        			direction = 'U';
        			i--;
        			bottomEdgeIndex--;
        		}
        		break;
        	case 'U':
        		if(i > topEdgeIndex + 1){
        			i--;
        		}else{
        			direction = 'R';
        			j++;
        			leftEdgeIndex++;
        		}
        		break;
        	}
        }
        
        return ret;
    }
    
    public void printList(List<Integer> list){
		System.out.print("{");
		
/*		Iterator<Integer> it = list.iterator();
		while(it.hasNext()){
			System.out.print("" + it.next()+ ", ");
		}*/
		
		for(Integer item : list){
			System.out.print("" + item + ", ");
		}
		
		System.out.println("}");
	}
	
	public void printArray2D(int[][] array){
		if(array.length == 0){
			System.out.println("{}");
			return;
		}
		
		System.out.println("\n{");
		for(int i = 0; i < array.length; i++){
			System.out.print("  {");
			for(int j = 0; j < array[i].length; j++){
				System.out.print("" + array[i][j] + ", ");
			}
			System.out.print("}");
			System.out.println("");
		}

		System.out.println("}");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		SpiralMatrixSln sln = new SpiralMatrixSln();
		
			int[][] input = {
				 { 1, 2, 3 },
				 { 4, 5, 6 },
				 { 7, 8, 9 }
				};
		
		System.out.print("Ex1 Input: " );
		sln.printArray2D(input);
		System.out.print("Ex1 Answer: " );
		sln.printList(sln.spiralOrder(input));
		
		int[][] input2 = {
			  	{1}
			  	 };
		             //    Output: {1,2,3,6,9,8,7,4,5}
		
		System.out.print("Ex2 Input: " );
		sln.printArray2D(input2);
		System.out.print("Ex2 Answer: " );
		sln.printList(sln.spiralOrder(input2));
		
		int[][] input3 = {
		                  {1, 2, 3, 4},
		                  {5, 6, 7, 8},
		                  {9,10,11,12}
		                };
//		                Output: {1,2,3,4,8,12,11,10,9,5,6,7}
		
		System.out.print("Ex3 Input: " );
		sln.printArray2D(input3);
		System.out.print("Ex3 Answer: " );
		sln.printList(sln.spiralOrder(input3));
		

		
		int[][] input4 = 
			{
				{}
			};
		
		System.out.print("Ex4 Input: " );
		sln.printArray2D(input4);
		System.out.print("Ex4 Answer: " );
		sln.printList(sln.spiralOrder(input4));

		int[][] input5 = 
			{};
		
		System.out.print("Ex5 Input: " );
		sln.printArray2D(input5);
		System.out.print("Ex5 Answer: " );
		sln.printList(sln.spiralOrder(input5));
		
		int[][] input6 = 
			{
			 {},
			 {},
			 {}
			};
		
		System.out.print("Ex6 Input: " );
		sln.printArray2D(input6);
		System.out.print("Ex6 Answer: " );
		sln.printList(sln.spiralOrder(input6));
		
		int[][] input7 = 
			 {
			  {1},
			  {2},
			  {3},
			  {4},
			  {5}
			 };
		
		System.out.print("Ex7 Input: " );
		sln.printArray2D(input7);
		System.out.print("Ex7 Answer: " );
		sln.printList(sln.spiralOrder(input7));
		
		
		
		int[][] input8 = 
			{
			 	{11,12,13,14,15},
			 	{21,22,23,24,25},
			 	{31,32,33,34,35}
		    };
		
		System.out.print("Ex8 Input: " );
		sln.printArray2D(input8);
		System.out.print("Ex8 Answer: " );
		sln.printList(sln.spiralOrder(input8));
		
		int[][] input9 = 
			{
			 	{00, 01, 02},
			 	{10, 11, 12},
			 	{20, 21, 22},
			 	{30, 31, 32},
			 	{40, 41, 42}
		   };
		
		System.out.print("Ex9 Input: " );
		sln.printArray2D(input9);
		System.out.print("Ex9 Answer: " );
		sln.printList(sln.spiralOrder(input9));
		
		int[][] input10 = 
			{
			  {1,2,3,4,5}
			};
		
		System.out.print("Ex10 Input: " );
		sln.printArray2D(input10);
		System.out.print("Ex10 Answer: " );
		sln.printList(sln.spiralOrder(input10));
		
		int[][] input11 = 
				{
				 {1,2,3,4,5},
				 {6,7,8,9,10},
				 {11,12,13,14,15},
				 {16,17,18,19,20},
				 {21,22,23,24,25}
						 };
		
		System.out.print("Ex11 Input: " );
		sln.printArray2D(input11);
		System.out.print("Ex11 Answer: " );
		sln.printList(sln.spiralOrder(input11));

	}

}
