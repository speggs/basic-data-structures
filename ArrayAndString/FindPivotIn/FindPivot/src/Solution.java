
public class Solution {
	
	public int pivotIndex(int[] nums){
		//return simpleButInefficient(nums);
		return trickyButEfficient(nums);
	}
	
	//o(n^2)
	private int simpleButInefficient(int[] nums){
			
		int i, j;
		int leftSum, rightSum;
		
		for(i = 0; i < nums.length; i++){
			leftSum = 0;
			for(j=0; j < i + 1; j++){
				leftSum += nums[j];
			}
			
			rightSum = 0;
			for(j = i; j < nums.length; j++){
				rightSum += nums[j];
			}
			
			if(leftSum == rightSum){
				return i;
			}
		
		}
		return -1;
	}
	
	//Sum whole array, then go left to right, subtracting leftSum until 
	//whole sum - leftSum = leftSum
	private int trickyButEfficient(int[] nums){
		int total = 0, i, leftSum = 0;
		for(i = 0; i < nums.length; i++){
			total += nums[i];
		}
		
		for(i = 0; i < nums.length; i++){
			if((total - (leftSum + nums[i])) == leftSum){
				return i;
			}
			
			leftSum += nums[i];
		}
		return -1;
	}
	
	
	public static void main(String[] args) {
		int ans;
		int[] input = {1,7,3,6,5,6};
		//answer shoudl be 3
		Solution sln = new Solution();
		System.out.println(sln.pivotIndex(input));
	}

}
