import java.util.ArrayList;
import java.util.List;

public class PTSolution {

	public List<List<Integer>> generate(int numRows) {
		List<List<Integer>> ret = new ArrayList<List<Integer>>();
		List<Integer> curRow;
		int count = 1;
		
		if(numRows == 0){
			return ret;
		}
		
		curRow = new ArrayList<Integer>();
		curRow.add(1);
		ret.add(curRow);
		while(count < numRows){
			curRow = generateRow(curRow);
			ret.add(curRow);
			count++;
		}
		
		return ret;
	}
	
	
	private List<Integer> generateRow(List<Integer> curRow) {
		List<Integer> ret = new ArrayList<Integer>();
		int prev = 0;
		
		for(Integer item : curRow){
			ret.add(prev + item);
			prev = item;
		}
		ret.add(1);
		return ret;
	}


	public void printList2D(List<List<Integer>> list2D){
		System.out.print("{");
		for(List<Integer> item : list2D){
			printList(item );
			System.out.println(",");
		}
		System.out.println("}");
	}
	
    public void printList(List<Integer> list){
		System.out.print("{");
		for(Integer item : list){
			System.out.print("" + item + ", ");
		}
		System.out.print("}");
	}
	
	public static void main(String[] args) {
		PTSolution sln = new PTSolution();
		
		System.out.println("Ex0 Input: 0" );
		System.out.println("Ex0 Answer: " );
		sln.printList2D(sln.generate(0));
		
		System.out.println("Ex1 Input: 1" );
		System.out.println("Ex1 Answer: " );
		sln.printList2D(sln.generate(1));
		
		System.out.println("Ex2 Input: 2" );
		System.out.println("Ex2 Answer: " );
		sln.printList2D(sln.generate(2));
		
		System.out.println("Ex3 Input: 3" );
		System.out.println("Ex3 Answer: " );
		sln.printList2D(sln.generate(3));
		
		System.out.println("Ex4 Input: 4" );
		System.out.println("Ex4 Answer: " );
		sln.printList2D(sln.generate(4));
		
		System.out.println("Ex5 Input: 5" );
		System.out.println("Ex5 Answer: " );
		sln.printList2D(sln.generate(5));

	}

}
