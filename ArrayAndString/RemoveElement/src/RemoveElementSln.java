/*Given an array nums and a value val, remove all instances of that value in-place and return the new length.
Do not allocate extra space for another array, you must do this by modifying the input array in-place with O(1) extra memory.
The order of elements can be changed. It doesn't matter what you leave beyond the new length.
Example 1:
Given nums = [3,2,2,3], val = 3,
Your function should return length = 2, with the first two elements of nums being 2.
It doesn't matter what you leave beyond the returned length.
Example 2:
Given nums = [0,1,2,2,3,0,4,2], val = 2,
Your function should return length = 5, with the first five elements of nums containing 0, 1, 3, 0, and 4.
Note that the order of those five elements can be arbitrary.
It doesn't matter what values are set beyond the returned length.
*/

public class RemoveElementSln {
    public int removeElement(int[] nums, int val) {
    	
    	int j = 0;
    	for(int i = 0; i<nums.length; i++){
    		if(nums[i] != val){
    			nums[j] = nums[i];
    			j++;
    		}
    	}
    	
        return j;
    }
    
    
    
    
	static void printArray(int[] array){
		System.out.print("{");
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i]+ ", ");
		}
		System.out.println("}");
	}
    
	static void printResults(int testNo, int[] inNums, int inVal, int out){
    	
    	System.out.println(String.format("Test no: %d\n"+
    			"inNums: ", testNo));
    	printArray(inNums);
    	System.out.println(String.format("inVal: %s\nout: %s\n\n",inVal, out));
    }

	public static void main(String[] args) {
		RemoveElementSln res = new RemoveElementSln();
		
		int[] in1Nums = {1,4,3,2};
		int in1Val = 3;
		
		System.out.print("in1Nums before: ");
		printArray(in1Nums);
		printResults(1, in1Nums, in1Val, res.removeElement(in1Nums, in1Val));
		
		int[] in2Nums = {3,2,2,3};
		int in2Val = 3;

		System.out.print("in2Nums before: ");
		printArray(in2Nums);
		printResults(2, in2Nums, in2Val, res.removeElement(in2Nums, in2Val));
		
		int[] in3Nums = {0,1,2,2,3,0,4,2};
		int in3Val = 2;

		System.out.print("in3Nums before: ");
		printArray(in3Nums);
		printResults(3, in3Nums, in3Val, res.removeElement(in3Nums, in3Val));
	}

}
