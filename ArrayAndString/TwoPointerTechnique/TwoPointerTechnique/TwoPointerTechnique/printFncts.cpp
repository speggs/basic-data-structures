
#include "printFncts.h"


void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void swap(vector<int>& vec, int i, int j){
	if (i < vec.size() && j < vec.size() && i >0 && j > 0){
		try{
			int temp = vec.at(i);
			vec.at(i) = vec.at(j);
			vec.at(j) = temp;
		}
		catch(const std::out_of_range& oor){
			printf("Out of range error: %s", oor.what());
		}
	}
}

int gcd(int a, int b){
	if (b == 0){
		return a;
	}
	else{
		return gcd(b, a%b);
	}
}