#include "printFncts.h"

vector<int> getRow(int rowIndex);
void printResultsPT(int testNo, int inRowIndex, vector<int> out);
int BinomialCoefficient(int n, int k);
int factorial(int n);