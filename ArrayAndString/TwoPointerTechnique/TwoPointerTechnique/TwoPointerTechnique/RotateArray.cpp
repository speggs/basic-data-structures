#include "RotateArray.h"
using namespace std;

//Given an array, rotate the array to the right by k steps, where k is non - negative.
//
//Example 1:
//
//Input : [1, 2, 3, 4, 5, 6, 7] and k = 3
//Output : [5, 6, 7, 1, 2, 3, 4]
//	 Explanation :
//				 rotate 1 steps to the right : [7, 1, 2, 3, 4, 5, 6]
//				 rotate 2 steps to the right : [6, 7, 1, 2, 3, 4, 5]
//				 rotate 3 steps to the right : [5, 6, 7, 1, 2, 3, 4]
//
//				 Example 2 :
//
//			 Input : [-1, -100, 3, 99] and k = 2
//				 Output : [3, 99, -1, -100]
//					  Explanation :
//								  rotate 1 steps to the right : [99, -1, -100, 3]
//								  rotate 2 steps to the right : [3, 99, -1, -100]
//
//							  Note :
//
//								   Try to come up as many solutions as you can, there are at least 3 different ways to solve this problem.
//								   Could you do it in - place with O(1) extra space ?

void RotateArray::rotate(vector<int>& nums, int k){
	if (nums.size() == 0 || nums.size() == 1 || (k % nums.size() == 0)){
		return;
	}
	k = k % nums.size();
	int gCD = gcd(k, nums.size());
	//In order to do an in place swap we need to divide set into subsets
	//equal to the greatest common denominator
	for (int i = 0; i < gCD; i++){

		//First we save initial value for the last swap
		//at the end this will be written to the final toInd
		int temp = nums.at(i);
		//initialize index to be updated
		int toInd = i;

		while(true){
			//fromInd is what gets written from (left of to ind)
			int fromInd = toInd - k;

			//rotate back around the array
			if (fromInd < 0){
				fromInd += nums.size();
			}

			//Back at start
			if (fromInd == i){
				break;
			}

			//write from from to to
			nums.at(toInd) = nums.at(fromInd);
			//update to to from
			toInd = fromInd;

		}
		//update starting point
		nums.at(toInd) = temp;
	}
}

void RotateArray::printResults(int testNo, vector<int>& inNums, int inK){
	printf("\nTest No :%d\ninNums: ", testNo);
	printVector(inNums);
	printf("\ninK: %d\n\n", inK);

}