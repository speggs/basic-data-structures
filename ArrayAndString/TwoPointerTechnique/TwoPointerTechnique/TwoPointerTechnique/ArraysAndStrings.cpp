#include "ArraysAndStrings.h"

using namespace std;

void reverseWords(string &s) {
	string ret = string();
	int i, j = s.length();

	if (j == 0){
		return;
	}

	//strip leading spaces
	int start = s.find_first_not_of(" ");
	if (start == string::npos){
		s.clear();
		return;
	}

	//Strip extra spaces at the end
	while(s.at(j - 1) == ' '){
		j--;
	}
	//find start of last word
	i = s.find_last_of(" ", j - 1);
	if (i == string::npos){
		if (j != start){
			ret.append(s.substr(start, j - start));
			s.swap(ret);
		}
		//if no spaces then just return the entire word
		return;
	}



	while (i != string::npos && i > start){
		//going from back to front
		//append words from end of s to start of ret
		ret.append(s.substr(i + 1, j - (i + 1)));
		ret.append(" ");

		//Strip multispace word delimeters
		while (s.at(i - 1) == ' '){
			i--;
		}

		//update end of next word to copy
		j = i;
		//find start of next word to copy
		i = s.find_last_of(" ", j - 1);
	}
	//append the final word
	ret.append(s.substr(start, j - start));

	s.swap(ret);
}

string reverseWords3(string s){

	int i = 0;
	int j = s.find_first_of(" ");
	while(j != string::npos){
		reverseSubString(s, i, j - 1 );
		i = j + 1;
		j = s.find_first_of(" ", i);
	}

	reverseSubString(s, i, s.length() - 1);

	return s;
}

void reverseSubString(string &s, int i, int j) {
	for (int k = i; k < j; k++){
		swap(s, k, j);
		j--;
	}
}

void swap(string &str, int i, int j){
	char temp = str[i];
	str[i] = str[j];
	str[j] = temp;
}

void printResultsRW3(int testNo, string inStr, string outStr){
	printf("TestNo: %d\ninStr: %s\noutStr: %s\n\n", testNo, inStr.c_str(), outStr.c_str());
}

int removeDuplicates(vector<int>& nums){
	printf("\nNums Before: ");
	printVector(nums);

	if (nums.size() < 2){
		return nums.size();
	}

	int j = 1;
	int prev = nums.at(0);
	for (int i = 1; i < nums.size(); i++){
		while (nums.at(i) == prev){
			i++;
			if (i >= nums.size()){
				break;
			}
		}

		if (i >= nums.size()){
			break;
		}

		nums.at(j) = nums.at(i);
		prev = nums.at(i);
		j++;
	}
	printf("\nNums after: ");
	printVector(nums);
	return j;
}

void moveZeroes(vector<int>& nums){
	printf("\nNums Before: ");
	printVector(nums);

	if (nums.size() < 2){
		return;
	}

	int j = 0;
	for (int i = 0; i < nums.size(); i++){
		while (nums.at(i) == 0){
			i++;
			if (i >= nums.size()){
				break;
			}
		}

		if (i >= nums.size()){
			break;
		}

		nums.at(j) = nums.at(i);
		j++;
	}
	for (; j < nums.size(); j++){
		nums.at(j) = 0;
	}
	printf("\nNums after: ");
	printVector(nums);
}