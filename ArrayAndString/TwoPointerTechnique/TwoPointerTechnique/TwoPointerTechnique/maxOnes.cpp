#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <Windows.h>
#include "printFncts.h"
using namespace std;

//Given a binary array, find the maximum number of consecutive 1s in this array.
//
//Example 1:
//
//Input : [1, 1, 0, 1, 1, 1]
//Output : 3
//	 Explanation : The first two digits or the last three digits are consecutive 1s.
//				   The maximum number of consecutive 1s is 3.
//
//			   Note :
//
//					The input array will only contain 0 and 1.
//					The length of input array is a positive integer and will not exceed 10, 000


class Solution{
public:
	int findMaxConsecutiveOnes(vector<int>& nums) {
		if (nums.size() == 0){
			return 0;
		}
		int max = 0;
		int j = 0;
		bool counting = false;
		for (int i = 0; i < nums.size(); i++){
			if (!counting && nums[i] == 1){
				j = i;
				counting = true;
			}
			if (counting && nums[i] == 0){
				if (max < i - j){
					max = i - j;
				}
				counting = false;
			}
		}
		if (counting){
			if (max < nums.size() - j){
				max = nums.size() - j;
			}
		}
		return max;
	}

};


void printResults(int testNo, vector<int> inVec, int out){
	printf("Test No:%d\ninVec: ", testNo);
	printVector(inVec);
	printf("\nout: %d\n\n", out);
}

//int main(){
//	Solution sln = Solution();
//
//	vector<int> inVec1 = vector<int>({ 1, 1, 0, 1, 1, 1 });
//	printResults(1, inVec1, sln.findMaxConsecutiveOnes(inVec1));
//
//
//	vector<int> inVec2 = vector<int>({ 1, 1, 0, 1, 1, 1, 1, 0, 1, 1, 1, 0 });
//	printResults(2, inVec2, sln.findMaxConsecutiveOnes(inVec2));
//
//	vector<int> inVec3 = vector<int>({ 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 0 });
//	printResults(3, inVec3, sln.findMaxConsecutiveOnes(inVec3));
//
//	//vector<int> inVec4 = vector<int>({});
//	//printResults(4, inVec4, sln.findMaxConsecutiveOnes(inVec4));
//
//	vector<int> inVec5 = vector<int>({0, 0, 0, 0, 0, 0});
//	printResults(5, inVec5, sln.findMaxConsecutiveOnes(inVec5));
//}