
#include "MinSizeSubarraySum.h"
using namespace std;

//Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum >= s.If there isn't one, return 0 instead.
//Example:
//Input : s = 7, nums = [2, 3, 1, 2, 4, 3]
//Output : 2
//	 Explanation : the subarray[4, 3] has the minimal length under the problem constraint.
//
//				   Follow up :
//		If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log n).




int MSSSSolution::minSubArrayLen(int s, vector<int>& nums) {
	int minRange = nums.size();
	int j = 0, sum = 0;
	for (int i = 0; i < nums.size(); i++){
		sum += nums[i];
		if (sum >= s){
			if ((i - j) + 1 < minRange){
				minRange = (i - j) + 1;
			}
			while (sum >= s){
				sum -= nums[j];
				j++;
				if (sum >= s){
					if ((i - j) + 1 < minRange){
						minRange = (i - j) + 1;
					}
				}
			}
		}

		if (minRange == 1){
			return minRange;
		}
	}

	if(j == 0 && sum < s){
		return 0;
	}
	return minRange;
}


void MSSSSolution::printResults(int testNo, int inSum, vector<int> inVec, int out){
	printf("Test No :%d\ninVec: ", testNo);
	printVector(inVec);
	printf("\ninSum: %d\nout: %d\n\n",inSum, out);
}

//int main(){
//	Solution sln = Solution();
//
//	int inSum1 = 7;
//	vector<int> inVec1 = vector<int>({ 2, 3, 1, 2, 4, 3 });
//	printResults(1, inSum1, inVec1, sln.minSubArrayLen(inSum1, inVec1));
//
//	int inSum2 = 7;
//	vector<int> inVec2 = vector<int>({ 2, 3, 1, 1});
//	printResults(2, inSum2, inVec2, sln.minSubArrayLen(inSum2, inVec2));
//
//	int inSum3 = 7;
//	vector<int> inVec3 = vector<int>({ 2, 3, 1, });
//	printResults(3, inSum3, inVec3, sln.minSubArrayLen(inSum3, inVec3));
//
//	int inSum4 = 7;
//	vector<int> inVec4 = vector<int>({ 2, 3, 1, 8, 4, 3 });
//	printResults(4, inSum4, inVec4, sln.minSubArrayLen(inSum4, inVec4));
//
//	int inSum5 = 7;
//	vector<int> inVec5 = vector<int>({});
//	printResults(5, inSum5, inVec5, sln.minSubArrayLen(inSum5, inVec5));
//}