#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
using namespace std;

class Solution {
public:
	string reverseString(string s) {
		if (s.length() == 0){
			return s;
		}
		int j = s.length() - 1;
		for (int i = 0; i < j; i++){
			swap(s, i, j);
			j--;
		}
		return s;
	}

	void swap(string &str, int i, int j){
		char temp = str[i];
		str[i] = str[j];
		str[j] = temp;
	}



};

void printResults( int testNo, string in, string out){
	printf("Test No:%d\n"
		"input: %s\n"
		"output: %s\n", testNo, in.c_str(), out.c_str());
}

//int main(){
//	Solution sln = Solution();
//
//	string in1("hello");
//	printResults(1, in1, sln.reverseString(in1));
//
//	string in2("");
//	printResults(2, in2, sln.reverseString(in2)); 
//	
//	string in3("abcd");
//	printResults(3, in3, sln.reverseString(in3));
//}