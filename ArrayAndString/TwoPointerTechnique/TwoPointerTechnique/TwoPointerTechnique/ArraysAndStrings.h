#include "printFncts.h"

string reverseWords3(string s);
void reverseWords(string &s);
void reverseSubString(string &s, int i, int j);
void swap(string &str, int i, int j);
void printResultsRW(string s);
void printResultsRW3(int testNo, string inStr, string outStr);
int removeDuplicates(vector<int>& nums);
void moveZeroes(vector<int>& nums);