#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include <Windows.h>
using namespace std;

//Given an array of integers that is already sorted in ascending order,
//find two numbers such that they add up to a specific target number.

//	The function twoSum should return indices of the two numbers such 
//that they add up to the target, where index1 must be less than index2.

//Note:
//Your returned answers(both index1 and index2) are not zero - based.
//	You may assume that each input would have exactly one solution and you may not use the same element twice.

//Example :
//	Input : numbers = [2, 7, 11, 15], target = 9
//		Output : [1, 2]
//			 Explanation : The sum of 2 and 7 is 9. Therefore index1 = 1, index2 = 2.
class Solution{
public:
	vector<int> twoSum(vector<int>& numbers, int target) {
		int i = 0;
		int j = numbers.size() - 1;

		vector<int> ret = vector<int>();
		if (j == 0){
			return ret;
		}

		while (i < j){
			if (numbers.at(j) + numbers.at(i) > target){
				j--;
			}
			else if (numbers.at(j) + numbers.at(i) < target){
				i++;
			}
			else{
				ret.push_back(i + 1);
				ret.push_back(j + 1);
				return ret;
			}
		}

		return ret;
	}

};

//string sprintVector(vector<int> vec){
//	char buf[500];
//	buf[0] = '{';
//	int i = 0;
//	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
//		i = sprintf_s(buf, 500, "%s%d, ",buf, *it);
//	}
//	if (i != -1){
//		buf[i] = '}';
//	}
//}
//
//string sprintResults(int testNo, vector<int> inVec, int inTar, vector<int> out){
//	printf("Test No:%d\n", testNo);
//	printf("inVec: ");
//	printVector(inVec);
//	printf("\ninTar: %d\nout: ", inTar);
//	printVector(out);
//	printf("\n\n");
//	return "";
//}

//int main(){
//	Solution sln = Solution();
//
//	vector<int> inVec1 = vector<int>({ 2, 7, 11, 15 });
//	int inTar1 = 9;
//	printResults(1, inVec1, inTar1, sln.twoSum(inVec1, inTar1));
//
//	//OutputDebugStringA(sprintResults(1, inVec1, inTar1, sln.twoSum(inVec1, inTar1)).c_str());
//}