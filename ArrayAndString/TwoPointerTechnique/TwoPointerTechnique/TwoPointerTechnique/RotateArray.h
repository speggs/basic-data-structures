#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "printFncts.h"

class RotateArray {
public:
public:
	void rotate(vector<int>& nums, int k);
	void printResults(int testNo, vector<int>& inNums, int inK);
};