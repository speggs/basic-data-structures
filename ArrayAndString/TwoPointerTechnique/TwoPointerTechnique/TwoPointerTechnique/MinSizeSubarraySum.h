#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>
#include "printFncts.h"

class MSSSSolution {
public:
	int minSubArrayLen(int s, vector<int>&nums);
	void printResults(int testNo, int inSum, vector<int> inVec, int out);
};

