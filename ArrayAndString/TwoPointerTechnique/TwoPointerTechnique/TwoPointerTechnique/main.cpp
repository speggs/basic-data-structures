#include "printFncts.h"
#include "MinSizeSubarraySum.h"
#include "RotateArray.h"
#include "PascalsTriangle.h"
#include  "ArraysAndStrings.h"
using namespace std;

int main(){
/*	MSSSSolution sln = MSSSSolution();

	int inSum1 = 7;
	vector<int> inVec1 = vector<int>({ 2, 3, 1, 2, 4, 3 });
	printResults(1, inSum1, inVec1, sln.minSubArrayLen(inSum1, inVec1));

	int inSum2 = 7;
	vector<int> inVec2 = vector<int>({ 2, 3, 1, 1 });
	printResults(2, inSum2, inVec2, sln.minSubArrayLen(inSum2, inVec2));

	int inSum3 = 7;
	vector<int> inVec3 = vector<int>({ 2, 3, 1, });
	printResults(3, inSum3, inVec3, sln.minSubArrayLen(inSum3, inVec3));

	int inSum4 = 7;
	vector<int> inVec4 = vector<int>({ 2, 3, 1, 8, 4, 3 });
	printResults(4, inSum4, inVec4, sln.minSubArrayLen(inSum4, inVec4));

	int inSum5 = 7;
	vector<int> inVec5 = vector<int>({});
	printResults(5, inSum5, inVec5, sln.minSubArrayLen(inSum5, inVec5));*/

	/*RotateArray ra = RotateArray();

	int inK1 = 1;
	vector<int> inNums1 = vector<int>({ 0,1,2,3,4,5,6,7});

	printf("inNums Before: ");
	printVector(inNums1);
	ra.rotate(inNums1, inK1);
	ra.printResults(1, inNums1, inK1);

	int inK2 = 2;
	vector<int> inNums2 = vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7 });

	printf("inNums Before: ");
	printVector(inNums2);
	ra.rotate(inNums2, inK2);
	ra.printResults(2, inNums2, inK2);

	int inK3 = 1;
	vector<int> inNums3 = vector<int>({ 0, 1 });

	printf("inNums Before: ");
	printVector(inNums3);
	ra.rotate(inNums3, inK3);
	ra.printResults(3, inNums3, inK3);

	int inK4 = 1;
	vector<int> inNums4 = vector<int>({ });

	printf("inNums Before: ");
	printVector(inNums4);
	ra.rotate(inNums4, inK4);
	ra.printResults(4, inNums4, inK4);

	int inK5 = 3;
	vector<int> inNums5 = vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7 , 8});

	printf("inNums Before: ");
	printVector(inNums5);
	ra.rotate(inNums5, inK5);
	ra.printResults(5, inNums5, inK5);

	int inK6 = 5;
	vector<int> inNums6 = vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7 });

	printf("inNums Before: ");
	printVector(inNums6);
	ra.rotate(inNums6, inK6);
	ra.printResults(6, inNums6, inK6);*/

	//int inRowIndex1 = 0;
	//printResultsPT(1, inRowIndex1, getRow(inRowIndex1));

	//int inRowIndex2 = 1;
	//printResultsPT(2, inRowIndex2, getRow(inRowIndex2));

	//int inRowIndex3 = 2;
	//printResultsPT(3, inRowIndex3, getRow(inRowIndex3));
/*
	int inRowIndex4 = 4;
	printResultsPT(4, inRowIndex4, getRow(inRowIndex4));*/

	//string inS1 = string("the sky is blue");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n",1, inS1.c_str());
	//reverseWords(inS1);
	//printf("Out: '%s'", inS1.c_str());

	//string inS2 = string("the sky is blue  ");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 2, inS2.c_str());
	//reverseWords(inS2);
	//printf("Out: '%s'", inS2.c_str());

	//string inS3 = string("  the sky is");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 3, inS3.c_str());
	//reverseWords(inS3);
	//printf("Out: '%s'", inS3.c_str());

	//string inS4 = string("the  sky  is  blue");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 4, inS4.c_str());
	//reverseWords(inS4);
	//printf("Out: '%s'", inS4.c_str());

	//string inS5 = string("  bigdumb   ");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 5, inS5.c_str());
	//reverseWords(inS5);
	//printf("Out: '%s'", inS5.c_str());

	//string inS6 = string("  bigdumb   ");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 6, inS6.c_str());
	//reverseWords(inS6);
	//printf("Out: '%s'", inS6.c_str());

	//string inS7 = string("1 ");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 7, inS7.c_str());
	//reverseWords(inS7);
	//printf("Out: '%s'", inS7.c_str());

	//string inS8 = string(" ");
	//printf("\n\nTestNo: %d\n, inS: '%s'\n", 8, inS8.c_str());
	//reverseWords(inS8);
	//printf("Out: '%s'", inS8.c_str());

	//string inStr1 = string("Hello cruel World");
	//printResultsRW3(1, inStr1, reverseWords3(inStr1));

	//string inStr2 = string("Hello");
	//printResultsRW3(2, inStr2, reverseWords3(inStr2));

	/*vector<int> inNums1 = vector<int>({ 1, 1, 1, 2, 2, 2, 3, 3, 3 });
	printf("\nTestNo: %d\nnew length: %d\n", 1, removeDuplicates(inNums1));



	vector<int> inNums2 = vector<int>({});
	printf("\nTestNo: %d\nnew length: %d\n", 2, removeDuplicates(inNums2));

	vector<int> inNums3 = vector<int>({ 1, 2, 3 });
	printf("\nTestNo: %d\nnew length: %d\n", 3, removeDuplicates(inNums3));

	vector<int> inNums4 = vector<int>({ 1, 2, 2, 2, 3 });
	printf("\nTestNo: %d\nnew length: %d\n", 4, removeDuplicates(inNums4));

	vector<int> inNums5 = vector<int>({2});
	printf("\nTestNo: %d\nnew length: %d\n", 5, removeDuplicates(inNums5));*/

	vector<int> inNums1 = vector<int>({ 1, 1, 0, 2, 0, 2, 3, 0, 3 });
	printf("\nTestNo: %d\n", 1);
	moveZeroes(inNums1);

	vector<int> inNums2 = vector<int>({ });
	printf("\nTestNo: %d\n", 2);
	moveZeroes(inNums2);

	vector<int> inNums3 = vector<int>({ 1});
	printf("\nTestNo: %d\n", 3);
	moveZeroes(inNums3);

	vector<int> inNums4 = vector<int>({ 1, 0});
	printf("\nTestNo: %d\n", 4);
	moveZeroes(inNums4);

	vector<int> inNums5 = vector<int>({ 0, 0, 2, 0, 2, 3, 0, 3 });
	printf("\nTestNo: %d\n", 5);
	moveZeroes(inNums5);

	vector<int> inNums6 = vector<int>({  0, 1 });
	printf("\nTestNo: %d\n", 6);
	moveZeroes(inNums6);
}