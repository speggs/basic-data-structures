#include "PascalsTriangle.h"
using namespace std;

vector<int> getRow(int rowIndex){
	vector<int> ret = vector<int>();

	//for (int i = 0; i <= rowIndex; i++){
	//	ret.push_back(1);

	//	int temp, temp1 = 1;
	//	for (int j = 1; j < i; j++){
	//		temp = ret.at(j);
	//		ret.at(j) = temp1 + ret.at(j);
	//		temp1 = temp;
	//	}
	//}

	//for (int i = 0; i < rowIndex + 1; i++){
	//	ret.push_back(BinomialCoefficient(rowIndex, i));
	//}

	//each index = rowIndex Choose index =
	// BC(rowIndex, i) =	(rowIndex!) / i!     *   (rowIndex - i)!
	//BC(rowIndex, i - 1) = (rowIndex!) / (i - 1)! * (rowIndex - (i - 1))!
	//therefore BC(ri, i) = BC(ri, i - 1) * (ri - i + 1) / i
	//Thus we can use the previous BC to calculate the next BC without re evaluating the whole
	//Binomial coefficient function
	int BC = 1;
	ret.push_back(BC);
	for (int i = 1; i < rowIndex + 1; i++){
		BC = BC * (rowIndex - i + 1) / i;
		ret.push_back(BC);
	}

	return ret;
}

int BinomialCoefficient(int n, int k){
	int ret = (factorial(n) / (factorial(k)*factorial(n - k)));
	return ret;
}

int factorial(int n){
	int ret = 1;
	for (int i = 2; i < n + 1; i++){
		ret *= i;
	}
	return ret;
}

void printResultsPT(int testNo, int inRowIndex, vector<int> out){

	printf("\nTest No :%d\ninRowIndex: %d\nout: ", testNo, inRowIndex);
	printVector(out);
	printf("\n\n");
}