/* Given an array of 2n integers, your task is to group these integers into n pairs of integer, say (a1, b1), (a2, b2), ..., (an, bn) which makes sum of min(ai, bi) for all i from 1 to n as large as possible.

Example 1:

Input: [1,4,3,2]

Output: 4
Explanation: n is 2, and the maximum sum of pairs is 4 = min(1, 2) + min(3, 4).

Note:

    n is a positive integer, which is in the range of [1, 10000].
    All the integers in the array will be in the range of [-10000, 10000].
*/
public class ArrayPartitionI {
	
    public int arrayPairSum(int[] nums) {
    	if(nums.length == 0){
    		return 0;
    	}
    	
    	sort(nums);
    	int sum = 0;
    	for(int i = 0; i < nums.length; i+=2){
    		sum += Math.min(nums[i], nums[i+1]);
    	}
    	
        return sum;
    }
    
    int partition(int array[], int low, int high){
		int pivot = array[high];
		int i = low - 1;
		
		for(int j = low; j < high; j++){
			if(array[j] <= pivot){
				i++;
				swap(array, i, j);
			}
		}
		swap(array, i+1, high);
		
		return i+1;
	}

	void sort(int array[], int low, int high){
		//we know we are done when low and high meet
		if(low < high){
			int partitionInd = partition(array, low, high);
			
			sort(array, low, partitionInd - 1);
			sort(array, partitionInd + 1, high);
		}
	}
	
	void sort(int array[]){
		sort(array, 0, array.length -1);
	}
	
	void swap(int array[], int i, int j){
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}

	static void printArray(int[] array){
		System.out.print("{");
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i]+ ", ");
		}
		System.out.println("}");
	}
    
    static void printResults(int testNo, int[] in1, int out){
    	
    	System.out.println(String.format("Test no: %d\n"+
    			"in: ", testNo));
    	printArray(in1);
    	System.out.println(String.format("out: %s\n\n", out));
    }
    
	public static void main(String[] args) {
		ArrayPartitionI api = new ArrayPartitionI();
		
		int[] in1 = {1,4,3,2};
		printResults(1, in1, api.arrayPairSum(in1));

	}

}
