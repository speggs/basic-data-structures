#include <string>
#include <stdio.h>
#include <iostream>
using namespace std;

class Solution {
public:
	int strStr(string haystack, string needle){
		if (needle == ""){
			return 0;
		}

		for (int i = 0; i < haystack.length(); i++){
			if (haystack[i] == needle[0]){
				for (int j = 0; j < needle.length(); j++){
					if (haystack[i] != needle[j]){
						i -= j;
						break;
					}
					else{
						if (j == needle.length() - 1){
							return i - j;
						}
						i++;
					}
				}
			}
		}

		return -1;
	}


};

void printResults(string inputA, string inputB, int testNo, int output){
	printf("Test No:%d\n"
		"input1: %s\n"
		"input2: %s\n"
		"output: %d\n", testNo, inputA.c_str(), inputB.c_str(), output);
}

int main(){
	Solution sln = Solution();

	string in1a("hello");
	string in1b("ll");
	printResults(in1a, in1b, 1, sln.strStr(in1a, in1b));

	string in2a("aaaaa");
	string in2b("bba");
	printResults(in2a, in2b, 2, sln.strStr(in2a, in2b));
}