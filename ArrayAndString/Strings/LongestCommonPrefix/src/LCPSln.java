
/*Write a function to find the longest common prefix string amongst an array of strings.

If there is no common prefix, return an empty string "".

Example 1:

Input: ["flower","flow","flight"]
Output: "fl"

Example 2:

Input: ["dog","racecar","car"]
Output: ""
Explanation: There is no common prefix among the input strings.

Note:

All given inputs are in lowercase letters a-z.
*/


public class LCPSln {
    public String longestCommonPrefix(String[] strs) {
    	if(strs.length == 0){
    		return "";
    	}
    	
        String shortest = strs[0];
        for(int i = 0; i < strs.length; i++){
        	if(strs[i].length() < shortest.length()){
        		shortest = strs[i];
        	}
        }
    	
        for(int j = 0; j < shortest.length(); j++)
        {
        	for(int i = 0; i < strs.length; i++)
        	{
        		if(strs[i].charAt(j) != shortest.charAt(j))
        		{
        			return shortest.substring(0, j);
        		}
        	}
        }
        
        return shortest;
    }
    
	static void printArray(String[] array){
		System.out.print("{");
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i]+ ", ");
		}
		System.out.println("}");
	}
    
    static void printResults(int testNo, String in1[], String out){
    	
    	System.out.println(String.format("Test no: %d\n"+
    			"in: ", testNo));
    	printArray(in1);
    	
    	System.out.println(String.format("out: %s\n\n", out));
    }

	public static void main(String[] args) {
		LCPSln sln = new LCPSln();
		
		String[] in1 = {"flower","flow","flight"};
		printResults(1, in1, sln.longestCommonPrefix(in1));
		
		String[] in2 = {"dog","racecar","car"};
		printResults(2, in2, sln.longestCommonPrefix(in2));
		
		String[] in3 = {"aa", "a"};
		printResults(3, in3, sln.longestCommonPrefix(in3));
		

		String[] in4 = {"abc"};
		printResults(4, in4, sln.longestCommonPrefix(in4));
		
		String[] in5 = {"ca", "a"};
		printResults(5, in5, sln.longestCommonPrefix(in5));

	}

}
