#include "stdafx.h"
#include <iostream>
#include <stdio.h>
#include <string>
using namespace std;

//add two strings containing binary numbers:
//a = "10101"
//b = "1010"
//a + b = "1111"

//111
//111
//1110

class Solution {
public:
	string addBinary(string a, string b){
		string ret = "0";
		if (a.length() == 0){
			if(b.length() == 0){
				return ret;
			}
			stripLeadingZeors(b);
			return b;
		}
		if (b.length() == 0){
			stripLeadingZeors(a);
			return a;
		}
		if (a.length() < b.length()){
			string temp = a;
			a = b;
			b = temp;
		}
		ret += a;

		int ai = ret.length() - 1, bi = b.length() - 1;

		//cases: 3
		// 1 bi = 0 => do nothing
		// 2 ai = 0, bi = 1 => ai = bi
		// 3 ai = 1, bi = 1 => ai = 0 and carry the one
		while (bi >= 0){
			//! case 1
			if (b[bi] != '0'){
				//case 2
				if (ret[ai] == '0')
				{
					ret[ai] = b[bi];
				}//case 3
				else
				{
					ret[ai] = '0';
					carryTheOne(ret, ai - 1);
				}
			}

			ai--;
			bi--;
		}
		stripLeadingZeors(ret);
		return ret;
	}

	void carryTheOne(string& binary, int index){
		//2 cases 
		//1 binary[index] = 0 => binary[index] = 1; return;
		//2 binary[index] = 1 => binary[index] = 0; carry the one;
		while (index >= 0){
			if ((binary)[index] == '0'){
				(binary)[index] = '1';
				return;
			}
			else{
				binary[index] = '0';
			}
			index--;
		}
	}

	void stripLeadingZeors(string& binary){
		int firstOne = binary.find_first_of('1');
		if (firstOne == string::npos){
			firstOne = binary.length() - 1;
		}
		binary.erase(0, firstOne);

	}

};

int main(){
	Solution sln = Solution();

	string in1a("111");
	string in1b("11");

	printf("Ex1 input: A: %s; B: %s\n", in1a.c_str(), in1b.c_str());
	string out1 = sln.addBinary(in1a, in1b);
	printf("Ex1 output: %s\n", out1.c_str());

	string in2a("");
	string in2b("1");

	printf("Ex2 input: A: %s; B: %s\n", in2a.c_str(), in2b.c_str());
	string out2 = sln.addBinary(in2a, in2b);
	printf("Ex2 output: %s\n", out2.c_str());

	string in3a("1");
	string in3b("");

	printf("Ex3 input: A: %s; B: %s\n", in3a.c_str(), in3b.c_str());
	string out3 = sln.addBinary(in3a, in3b);
	printf("Ex3 output: %s\n", out3.c_str());

	string in4a("");
	string in4b("");

	printf("Ex4 input: A: %s; B: %s\n", in4a.c_str(), in4b.c_str());
	string out4 = sln.addBinary(in4a, in4b);
	printf("Ex4 output: %s\n", out4.c_str());

	string in5a("11111");
	string in5b("11111");

	printf("Ex5 input: A: %s; B: %s\n", in5a.c_str(), in5b.c_str());
	string out5 = sln.addBinary(in5a, in5b);
	printf("Ex5 output: %s\n", out5.c_str());

	string in6a("101010101110");
	string in6b("0011011011101");

	printf("Ex6 input: \nA: %s; \nB: %s\n", in6a.c_str(), in6b.c_str());
	string out6 = sln.addBinary(in6a, in6b);
	printf("Ex6 output: %s\n", out6.c_str());

	string in7a("00000");
	string in7b("00000");

	printf("Ex7 input: \nA: %s \nB: %s\n", in7a.c_str(), in7b.c_str());
	string out7 = sln.addBinary(in7a, in7b);
	printf("Ex7 output: \n%s\n", out7.c_str());

}