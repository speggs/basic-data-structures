/*Plus One
 * English: Given non-empty array of digits representing a 
 * non-negative integer, plus one to the integer
 * 
 * The digits are stored s.t. the most significant digit 
 * is at the head of the list
 * and each element in the array contains a single digit
 * 
 * You may assume the integer does not contain any leading
 * zero except the number 0 itself
 * 
 * Provided Examples:
 * Example 1
 * In: {1,2,3}
 * out: {1,2,4}
 * 
 * Example 2
 * In: {4,3,2,1}
 * out: {4,3,2,2}
 * 
 * Inputs: int[] digits
 * Outputs: int[] resultDigits = number(digits) + 1
 * Constraints: No leading zeros, except zero.
 * 		1 digit per element
 * 		positive
 * 		
 * 
 * Edge Cases
 * Ex3
 * In: {9,9,9}
 * out:{1,0,0,0}
 * 
 * Ex4
 * In: {0}
 * out: {1}
 * 
 * Ex5
 * In: {5,9,9}
 * out: {6,0,0}
 * 
 * Tricky optimizations:
 * O(1)
 * If(digits[digits.length -1] != 9){
 * 		digits[digits.length - 1] += 1;
 * 		return digits;
 * }
 * 
 * Complexity:
 * Best Case O(1)
 * Worst Case need to read every digit back to front until we find a non-nine
 * 
 * What did I miss?
 * 
 * HashSets I dont think so
 * 
 * Parallelization: Could do to find first non-nine seems liek overkill worst case reduced 
 * to O(1) w/ enough procs
 * 
 * large inputs -> Chunking?
 * reduced to O(logsubm(n)) where m is your procs
 * 
 * don't forget your comments
 * */
public class Solution {
    public int[] plusOne(int[] digits) {
    	
    	int firstNonNineInd = findFirstNonNine(digits);
    	
    	//all nines
    	if(firstNonNineInd == -1){
    		int[] ret = new int[digits.length + 1];
    		ret[0] = 1;
    		for(int i = 1; i < ret.length; i++){
    			ret[i] = 0;
    		}
    		return ret;
    	}
    	
    	digits[firstNonNineInd] += 1;
    	for(int i = firstNonNineInd + 1; i < digits.length; i++){
    		digits[i] = 0;
    	}
        return digits;
    }
	
	private int findFirstNonNine(int[] digits){
		
		for(int i = digits.length - 1; i >= 0; i--){
			if(digits[i] != 9){
				return i;
			}
		}
		return -1;
	}

	public void printArray(int[] digits){
		for(int i = 0; i < digits.length; i++){
			System.out.print("" + digits[i]);
		}
		System.out.println("");
	}
	
	public static void main(String[] args) {
		int[] input = {1,2,3};//should return{1,2,4}
		int[] input2 = {9};//should return {4,3,2,2}
		int[] input3 = {9,9,9};//should return {1,0,0,0}
		int[] input4 = {0};//should return {1}
		int[] input5 = {5,9,9};//should return {6,0,0}
		
		Solution sln = new Solution();

		System.out.print("Ex1 Input: " );
		sln.printArray(input);
		System.out.print("Ex1 Answer: " );
		sln.printArray(sln.plusOne(input));

		System.out.print("Ex2 Input: " );
		sln.printArray(input2);
		System.out.print("Ex2 Answer: " );
		sln.printArray(sln.plusOne(input2));

		System.out.print("Ex3 Input: " );
		sln.printArray(input3);
		System.out.print("Ex3 Answer: " );
		sln.printArray(sln.plusOne(input3));

		System.out.print("Ex4 Input: " );
		sln.printArray(input4);
		System.out.print("Ex4 Answer: " );
		sln.printArray(sln.plusOne(input4));

		System.out.print("Ex5 Input: " );
		sln.printArray(input5);
		System.out.print("Ex5 Answer: " );
		sln.printArray(sln.plusOne(input5));
	}
}
