//Give int[] nums - there is always exactly one largest element
//Input: int[] nums
//Output: if(max(nums) >= 2*max(nums.exclude(max(nums)){ return index(max(nums))} else { return -1 }
//Constraints: - exactly 1 largest element in nums
// 1 <= nums.length <= 50
// 0 <= nums[i] <= 99 for all 0 <= i < nums.length
//
//Example in: nums = [3, 6, 1, 0]
//Example out: 1
//
//Example 2 in: nums = [1, 2, 3 ,4]
// Example 2 out: -1
//
//complexity analysis
//size of nums will be n
//believe o(n) is optimal for time
//o(n) also optimal for space
//every index must be looked at at least once I think.
//we can stop looking if we see two numbers >= 50?
//
//could use parallel execution to speed things up by processing the array piecewise, then merging maxes, 2nd maxes
//get nlogn? or logn?
// maybe with a hashset even better?
//
//Mistake: not updating second Largest properly
public class Solution {

	public int dominantIndex(int[] nums){
		return firstTry(nums);
	}
	//we can just iterate over, find the largest two numbers, and be done.
	//stop looking if we see two fiftys?
	int firstTry(int[] nums){
		int largest = 0, secondLargest = 0, largestInd = 0;
		
		if(nums.length == 1){
			return 0;
		}
		
		for(int i = 0; i < nums.length; i++){
			if(nums[i] > largest){
				secondLargest = largest;
				largest = nums[i];
				largestInd = i;
			}else if(nums[i] > secondLargest){
				secondLargest = nums[i];
			}
		}
		
		if(largest >= 2*secondLargest){
			return largestInd;
		}
		
		return -1;
	}
	
	public static void main(String[] args) {
		int[] input = {3, 6, 1, 0};//should return 1
		int[] input2 = {1, 2, 3 ,4};//should return -1
		int[] input3 = {0, 0, 3 ,2};//should return -1
		
		Solution sln = new Solution();
		
//		System.out.println("Ex1 Answer: " + sln.dominantIndex(input));
//		System.out.println("Ex2 Answer: " + sln.dominantIndex(input2));
		System.out.println("Ex3 Answer: " + sln.dominantIndex(input3));
	}

}
