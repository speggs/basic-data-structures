/*English: Given an MxN rowsxcolumns matrix of ints 'matrix' return all elements of the 
 * matrix in diagonal order as shown in the image in the diagonal traverse
 * directory. The total number of elements of the given matrix will not exceed 10,000
 * 
 * Provided Examples:
 * {
 { 1, 2, 3 },
 { 4, 5, 6 },
 { 7, 8, 9 }
}
Output:  {1,2,4,7,5,3,6,8,9}
 * 
 * 
 * matrix[1][0] = 4; = M(2,1)
 * 
 * Inputs: MxN matrix of ints (array of rows) int[][] matrix
 * Outputs: int[] ret; ret.length == M*N == matrix.length * matrix[0].length
 * Constraints: <= 10,000 elements
 * 
 * Edge Cases
 * Ex2:
 * in:
 * {
  	{1}
 * }
 * 
 * out:
 * {1}
 * 
 * Ex3:
 * in:
 * {
  	{1,2,3,4,5}
 * }
 * 
 * out:
 * {1,2,3,4,5}
 *TO?
 * 
 * Ex4:
 * in:
 * {
  	{}
  }
 * 
 * out:
 * {}
 * 
 * Ex5
 * in:
 * {}
 * 
 * out:
 * {}
 * 
 * Ex6:
 * in:
 * {
 * {},
 * {},
 * {}
 * }
 * 
 * out:
 * {}
 * 
 * 
 * Ex7:
 * in:
 * {
	  {1},
	  {2},
	  {3},
	  {4},
	  {5}
  }
 * 
 * out:
 * {1,2,3,4,5}
 * TO?
 * 
 * Ex8:
 * in:
 * {
 	{11,12,13,14,15},
 	{21,22,23,24,25},
 	{31,32,33,34,35}
    }
 * 
 * out:
 * {11, 12,21, 31,22,13, 14,23,32, 33,24,15, 25,34, 35}
 * 
 * Ex9:
 * {
 	{00, 01, 02},
 	{10, 11, 12},
 	{20, 21, 22},
 	{30, 31, 32},
 	{40, 41, 42}
   }
 * 
 * out:
 * {00, 10,01, 20,11,02, 12,21,30, 40,31,22, 32,41, 42}
 * 
 * Tricky Optimizations:
 * Mx1 / 1/M matrices
 * 
 * Complexity O(MxN)
 * Worst Case
 * Best Case
 * Average Case
 * 
 * What Did I miss? 
 * don't forget it's zero indexed
 * 
 * HashSets? Doesn't seem relevant here.
 * 
 * Parallelization? Loadbalance
 * could do, hard to split square matricies? but if very rectangular, 
 * or could go from front to back/back to front
 * but that wouldn't decrease the memory needed
 * could go front to back back to front and only take some of the rows/columns too,
 * split by distance from edge that would work for two machines, could it be expanded further?
 * easy to split very rectangular matrix.
 * yes it could be further expanded, the work would be in the splitting not the merging too
 * formula would be based on M,N so O(1) to split, if split right O(1) to merge too? That can'
 * t be right
 * 
 * large inputs
 * 
 * comments
 * 
 * Roughly:
 * start at i=0, j=0;
 * */
public class Solution {
	public int[] findDiagonalOrder(int[][] matrix) {
		int[] ret;
		boolean upRight = true;
		int m,n;
		if(matrix == null || matrix.length == 0 || matrix[0].length == 0){
			ret = new int[0];
			return ret;
		}
		
		//int[][] copy = copy2dArray(matrix);
		
		m = matrix.length;
		n = matrix[0].length;
		ret = new int[m*n];
		
		int i = 0, j = 0, k = 0;
		//While not at bottom right
		while(!(i == m - 1 && j == n - 1)){
			
//			System.out.println("(" + i + ", " + j+") = " + matrix[i][j]);
//			if(upRight){
//				System.out.println("Up Right");
//			}else{
//				System.out.println("Down Left");
//			}
//			copy[i][j] = -1;
//			printArray2D(copy);
			
			ret[k] = matrix[i][j];
			//1 no edge - move upright
			//2 top edge - move right reverse
			//3 right edge - move down reverse
			//4 top-right corner - move down reverse
			if(upRight){
				//right edge/top right corner - move down reverse
				if(j == n - 1){
					i++; 
					upRight = false;
				}//top edge - move right reverse
				else if(i == 0){
					j++;
					upRight = false;
				}//No edge reached
				else{
					i--;
					j++;
				}
			//left down
			//no-edge - continue
			//left edge - move down reverse
			//bottom edge - move right reverse
			//bottom left corner - move right reverse
			}else{
				//bottom edge - move right reverse	
				if(i == m - 1){
					upRight = true;
					j++;
				//left edge - move down reverse
				}else if(j == 0){
					i++;
					upRight = true;
				//no edge - continue
				}else {
					i++;
					j--;
				}
			}
			k++;
		}
		
		ret[ret.length -1 ] = matrix[i][j];
		
		return ret;
	}
	
	public void printArray(int[] array){
		System.out.print("{");
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i]+ ", ");
		}
		System.out.println("}");
	}
	
	public void printArray2D(int[][] array){
		if(array.length == 0){
			System.out.println("{}");
			return;
		}
		
		System.out.println("\n{");
		for(int i = 0; i < array.length; i++){
			System.out.print("  {");
			for(int j = 0; j < array[i].length; j++){
				System.out.print("" + array[i][j] + ", ");
			}
			System.out.print("}");
			System.out.println("");
		}

		System.out.println("}");
		System.out.println("");
	}
	
	//Array 'rows' must be of uniform length
	private int[][] copy2dArray(int[][] array){
		int[][] ret;
		if(array.length == 0){
			ret = new int[0][];
			return ret;
		}
		
		ret = new int[array.length][array[0].length];
		
		for(int i = 0; i < array.length; i++){
			for(int j = 0; j < array[i].length; j++){
				ret[i][j] = array[i][j];
			}
		}Solution sln = new Solution();
		
		int[][] input = {
				 { 1, 2, 3 },
				 { 4, 5, 6 },
				 { 7, 8, 9 }
				};
		//should return {1,2,4,7,5,3,6,8,9}
		
		System.out.print("Ex1 Input: " );
		sln.printArray2D(input);
		System.out.print("Ex1 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input));

		return ret;
	}
	
	public static void main(String[] args) {
		
		
		
		int[][] input2 = {
			  	{1}
			  	 };
		
		System.out.print("Ex2 Input: " );
		sln.printArray2D(input2);
		System.out.print("Ex2 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input2));

		int[][] input3 = 
			{
			  {1,2,3,4,5}
			};
		
		System.out.print("Ex3 Input: " );
		sln.printArray2D(input3);
		System.out.print("Ex3 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input3));

		int[][] input4 = 
			{
				{}
			};
		
		System.out.print("Ex4 Input: " );
		sln.printArray2D(input4);
		System.out.print("Ex4 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input4));

		int[][] input5 = 
			{};
		
		System.out.print("Ex5 Input: " );
		sln.printArray2D(input5);
		System.out.print("Ex5 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input5));
		
		int[][] input6 = 
			{
			 {},
			 {},
			 {}
			};
		
		System.out.print("Ex6 Input: " );
		sln.printArray2D(input6);
		System.out.print("Ex6 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input6));
		
		int[][] input7 = 
			 {
			  {1},
			  {2},
			  {3},
			  {4},
			  {5}
			 };
		
		System.out.print("Ex7 Input: " );
		sln.printArray2D(input7);
		System.out.print("Ex7 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input7));
		
		int[][] input8 = 
			{
			 	{11,12,13,14,15},
			 	{21,22,23,24,25},
			 	{31,32,33,34,35}
		    };
		
		System.out.print("Ex8 Input: " );
		sln.printArray2D(input8);
		System.out.print("Ex8 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input8));
		
		int[][] input9 = 
			{
			 	{00, 01, 02},
			 	{10, 11, 12},
			 	{20, 21, 22},
			 	{30, 31, 32},
			 	{40, 41, 42}
		   };
		
		System.out.print("Ex9 Input: " );
		sln.printArray2D(input9);
		System.out.print("Ex9 Answer: " );
		sln.printArray(sln.findDiagonalOrder(input9));
	}
}
