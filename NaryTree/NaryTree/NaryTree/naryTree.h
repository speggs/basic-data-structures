#ifndef NARYTREE_H
#define NARYTREE_H

#include <stddef.h>
#include "stddef.h"
#include <vector>
#include <queue>
#include <unordered_map>

class Node {
public:
	int val;
	std::vector<Node*> children;

	Node() {}

	Node(int _val, std::vector<Node*> _children) {
		val = _val;
		children = _children;
	}
};

#include "printers.h"
#include "utils.h"

std::vector<int> preorder(Node* root);
void addVecPreorder(Node * root, std::vector<int>& vec);

std::vector<int> postorder(Node* root);
void addVecPostorder(Node * root, std::vector<int>& vec);

std::vector<std::vector<int>> levelOrder(Node* root);

int maxDepth(Node* root);

#endif// NARYTREE_H