#include "utils.h"
using namespace std;

Node * make3NT(int height){
	Node * ret = new Node();
	int curVal = 1;
	queue<Node*> addTo;
	queue<int> addToLevels;
	if (height == 0){
		return NULL;
	}
	ret->val = curVal;
	curVal++;
	ret->children = vector<Node*>();
	addTo.push(ret);
	addToLevels.push(1);
	while (1){
		Node * newNode;
		Node * curNode = addTo.front();
		addTo.pop();
		int curLevel = addToLevels.front();
		addToLevels.pop();

		if (curLevel >= height){
			return ret;
		}

		newNode = new Node(curVal++, vector<Node *>());
		curNode->children.push_back(newNode);
		addTo.push(newNode);
		addToLevels.push(curLevel + 1);

		newNode = new Node(curVal++, vector<Node *>());
		curNode->children.push_back(newNode);
		addTo.push(newNode);
		addToLevels.push(curLevel + 1);

		newNode = new Node(curVal++, vector<Node *>());
		curNode->children.push_back(newNode);
		addTo.push(newNode);
		addToLevels.push(curLevel + 1);
	}

	return ret;
}