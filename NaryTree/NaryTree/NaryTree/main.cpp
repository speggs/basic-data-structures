#include "naryTree.h"
#include "printers.h"
#include "utils.h"
#include <stdio.h>

using namespace std;

void testMakeNT3(){
	Node * in = make3NT(3);
	printNTLvlOrder(in);
}

void testPreorder(){
	Node * in = make3NT(3);
	printPreorderTest(in);
}

void testPostorder(){
	Node * in = make3NT(3);
	printPostorderTest(in);
}

void testLevelOrder(){
	Node * in = make3NT(3);
	printLevelOrderTest(in);
}

void testMaxDepth(){
	Node * in = make3NT(2);
	printMaxDepthTest(in);

	in->children.at(0)->children.push_back(new Node(4, vector<Node *>()));
	printMaxDepthTest(in);
}

int main(){
	//testMakeNT3();
	//testPreorder();
	//testPostorder();
	//testLevelOrder();
	testMaxDepth();
}