#include "naryTree.h"
using namespace std;

vector<int> preorder(Node* root) {
	vector<int> ret = vector<int>();
	if (root == NULL){
		return ret;
	}

	addVecPreorder(root, ret);
	return ret;
}

void addVecPreorder(Node * root, vector<int>& vec){
	vec.push_back(root->val);
	if (root->children.size() > 0){
		vector<Node *>::iterator cit = root->children.begin();
		while (cit != root->children.end()){
			addVecPreorder(*cit, vec);
			cit++;
		}
	}
}

vector<int> postorder(Node* root) {
	vector<int> ret = vector<int>();
	if (root == NULL){
		return ret;
	}

	addVecPostorder(root, ret);
	return ret;
}

void addVecPostorder(Node * root, vector<int>& vec){
	if (root->children.size() > 0){
		vector<Node *>::iterator cit = root->children.begin();
		while (cit != root->children.end()){
			addVecPostorder(*cit, vec);
			cit++;
		}
	}
	vec.push_back(root->val);
}

vector<vector<int>> levelOrder(Node* root) {
	vector<vector<int>> ret = vector<vector<int>>();
	if (root == NULL){
		return ret;
	}

	queue<Node *> toAdd;
	queue<int> toAddLevels;
	toAdd.push(root);
	toAddLevels.push(1);
	ret.push_back(vector<int>());

	while (!toAdd.empty()){
		Node * curNode = toAdd.front();
		toAdd.pop();
		int curLevel = toAddLevels.front();
		toAddLevels.pop();

		if (curLevel > ret.size()){
			ret.push_back(vector<int>());
		}

		ret.at(curLevel - 1).push_back(curNode->val);
		if (curNode->children.size() > 0){
			vector<Node *>::iterator cit = curNode->children.begin();
			while (cit != curNode->children.end()){
				toAdd.push(*cit);
				toAddLevels.push(curLevel + 1);
				cit++;
			}
		}
	}
	return ret;
}

int maxDepth(Node* root) {
	int ret = 1;

	if (root == NULL){
		return 0;
	}

	if (root->children.size() > 0){
		vector<Node *>::iterator cit = root->children.begin();
		while (cit != root->children.end()){
			ret = max(ret,1 + maxDepth(*cit));
			cit++;
		}
	}
	return ret;
}