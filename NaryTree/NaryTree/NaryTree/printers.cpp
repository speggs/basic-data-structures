#include "printers.h"
using namespace std;

int TestNo = 1;

void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void printVector(vector<vector<int>> vec){
	int vecSize = vec.size();
	for (int i = 0; i < vecSize; i++){
		printVector(vec.at(i));
		printf("\n");
	}
}

void printNTLvlOrder(Node * root){
	if (root == NULL){
		printf("NULL");
		return;
	}

	queue<Node *> toPrint;
	queue<int> toPrintLevels;
	queue<int> toPrintParents;
	int printLevel = 0;
	toPrint.push(root);
	toPrintLevels.push(0);
	toPrintParents.push(-1);

	while (!toPrint.empty()){
		Node * curNode = toPrint.front();
		toPrint.pop();
		int curLevel = toPrintLevels.front();
		toPrintLevels.pop();
		int curParent = toPrintParents.front();
		toPrintParents.pop();

		if (curLevel > printLevel){
			printf("\n");
			printLevel = curLevel;
		}

		printf("(%d)->(%d) ",curParent, curNode->val);


		vector<Node*>::iterator cit = curNode->children.begin();
		while (cit != curNode->children.end()){
			toPrint.push(*cit);
			toPrintLevels.push(curLevel + 1);
			toPrintParents.push(curNode->val);
			cit++;
		}
	}
}

void printPreorderTest(Node * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printNTLvlOrder(in);

	vector<int> out = preorder(in);

	printf("\noutput Vec: ");
	printVector(out);
	printf("\n\n");

	TestNo++;
}

void printPostorderTest(Node * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printNTLvlOrder(in);

	vector<int> out = postorder(in);

	printf("\noutput Vec: ");
	printVector(out);
	printf("\n\n");

	TestNo++;
}

void printLevelOrderTest(Node * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printNTLvlOrder(in);

	vector<vector<int>> out = levelOrder(in);

	printf("\noutput Vec: \n");
	printVector(out);
	printf("\n\n");

	TestNo++;
}

void printMaxDepthTest(Node * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printNTLvlOrder(in);

	int out = maxDepth(in);

	printf("\noutput Depth: %d\n", out);
	printf("\n\n");

	TestNo++;

}