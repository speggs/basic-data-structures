#ifndef PRINTERS_H
#define PRINTERS_H

#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "naryTree.h"

//printers
void printNTLvlOrder(Node * root);
void printVector(std::vector<int> vec);
void printVector(std::vector<std::vector<int>> vec);

//test printers
void printPreorderTest(Node * in);
void printPostorderTest(Node * in);
void printLevelOrderTest(Node * in);
void printMaxDepthTest(Node * in);

#endif //PRINTERS_H