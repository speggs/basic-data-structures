import java.util.Hashtable;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.Queue;
import java.util.Vector;

/* Breadth First Search of Grid
 * Given a grid, each cell is empty, blocked or has the goal.
 * you are given starting coordinates
 * no diagonal movement
 * 
 * Example Input:
 * S E E
 * E E E
 * E E G
 * 
 * Output: 4;
 * 
 * General description:
 * We start by adding start to queue,
 * loop over all elements in queue
 * 		Next we check all it's neighbors 1 by 1
 * 			if(it empty we add it to the queue)
 * 			if(it is the goal we stop)
 * 			if(it is blocked we do nothing)
 * 		finally we remove start from queue and update our distance by 1
 * 
 * We represent nodes by int[3], node[0] = x, node[1] = y node[2] = distance
 * 
 * x, y coordinate system start from the top left down right so that we think of grid as an array of rows
 * */
public class BFSGrid {
	
	public Vector<int[]> distanceToGoal(char[][] grid, int[] startNode){
		Queue <int []> nodeQueue = new LinkedList<int[]> ();
		Hashtable<int[], int[]> prevs = new Hashtable<>();
		//prevs.put(startNode, null);
		nodeQueue.add(startNode);
		
		int width = grid[0].length;
		int height = grid.length;
		
		int[] currentNode;
		int x, y;
		char neighborC = 'X';
		
		while(nodeQueue.peek() != null){
			currentNode = nodeQueue.remove();
			x = currentNode[0];
			y = currentNode[1];
			//No need to visit the currentNode twice, therefore mark it as blocked
			grid[y][x] = 'B';
			//printArray2D(grid);
			//Check top, right, down, left
			
			//Check top neighbor
			//First check that we are not at top of grid
			//next check we have not visited top neighbor
			if(y > 0 && !(grid[y - 1][x] == 'B')){
				neighborC = grid[y - 1][x];
				if(neighborC == 'G')//if at goal return distance
				{
					int[] temp = new int[3];
					temp[0] = x;
					temp[1] = y - 1;
					temp[2] = currentNode[2] + 1;
					prevs.put(temp, currentNode);
					return reconstructPath(temp, prevs);
				}
				//if empty add node to queue to be checked later
				else if(neighborC == 'E')
				{
					int[] temp = new int[3];
					temp[0] = x;
					temp[1] = y - 1;
					temp[2] = currentNode[2] + 1;
					nodeQueue.add(temp);
					prevs.put(temp, currentNode);
				}
			}
			
			//Check right neighbor
			//Check we aren't all the way right
			//check we haven't already visited neighbor
			if(x < width - 1 && grid[y][x + 1] != 'B'){
				neighborC = grid[y][x + 1];
				//If goal return distance
				if(neighborC == 'G')
				{
					int[] temp = new int[3];
					temp[0] = x + 1;
					temp[1] = y;
					temp[2] = currentNode[2] + 1;
					prevs.put(temp, currentNode);
					return reconstructPath(temp, prevs);
				}
				//if empty add to toVisit queue
				else if(neighborC == 'E')
				{
					int[] temp = new int[3];
					temp[0] = x + 1;
					temp[1] = y;
					temp[2] = currentNode[2] + 1;
					nodeQueue.add(temp);
					prevs.put(temp, currentNode);
				}
			}
			
			//Check bottom neighbor
			if(y < height - 1 && grid[y + 1][x] != 'B'){
				neighborC = grid[y + 1][x];
				
				if(neighborC == 'G')
				{
					int[] temp = new int[3];
					temp[0] = x;
					temp[1] = y + 1;
					temp[2] = currentNode[2] + 1;
					prevs.put(temp, currentNode);
					return reconstructPath(temp, prevs);
				}
				else if(neighborC == 'E')
				{
					int[] temp = new int[3];
					temp[0] = x;
					temp[1] = y + 1;
					temp[2] = currentNode[2] + 1;
					nodeQueue.add(temp);
					prevs.put(temp, currentNode);
				}
			}
			
			//Check left neighbor
			if(x > 0 && grid[y][x - 1] != 'B'){
				neighborC = grid[y][x - 1];
				
				if(neighborC == 'G')
				{
					int[] temp = new int[3];
					temp[0] = x - 1;
					temp[1] = y;
					temp[2] = currentNode[2] + 1;
					prevs.put(temp, currentNode);
					return reconstructPath(temp, prevs);
				}
				else if(neighborC == 'E')
				{
					int[] temp = new int[3];
					temp[0] = x - 1;
					temp[1] = y;
					temp[2] = currentNode[2] + 1;
					nodeQueue.add(temp);
					prevs.put(temp, currentNode);
				}
			}
		}
		return null;
	}
	
	private Vector<int[]> reconstructPath(int[] goal, Hashtable<int[], int[]> prevs){
		Vector<int[]> path = new Vector<>();
			path.add(goal);
			int[] prev = prevs.get(goal);
			path.add(prev);
			while(prevs.containsKey(prev)){
				prev = prevs.get(prev);
				path.add(prev);
			}
		
		return path;
	}
	
	public void printPath(Vector<int[]> path){
		ListIterator<int[]> it = path.listIterator();
		int[] temp;
		while(it.hasNext()){
			it.next();
		}
		System.out.println("Path: ");
		while(it.hasPrevious()){
			temp = it.previous();
			System.out.print("(" + temp[0] + ", "+temp[1] + "); ");
		}
		System.out.println("");
	}
	
	public void printArray(int[] array){
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i]);
		}
		System.out.println("");
	}
	
	public void printArray2D(char[][] array){
		if(array.length == 0){
			System.out.println("{}");
			return;
		}
		
		System.out.println("{");
		for(int i = 0; i < array.length; i++){
			System.out.print("  {");
			for(int j = 0; j < array[i].length; j++){
				System.out.print("" + array[i][j] + ", ");
			}
			System.out.print("}");
			System.out.println("");
		}

		System.out.println("}");
		System.out.println("");
	}
	
	public static void main(String[] args) {
		BFSGrid bfsg = new BFSGrid();
		
		char[][] input = {
				{'S', 'E', 'E'},
				{'E', 'E', 'E'},
				{'E', 'E', 'G'}
			};
		int[] start = {0,0,0};
		
		System.out.println("Input: ");
		bfsg.printArray2D(input);
		System.out.print("Output: ");
		bfsg.printPath(bfsg.distanceToGoal(input, start));
		System.out.println("");
		
		char[][] input2 = {
				{'S', 'E', 'E', 'E'},
				{'B', 'B', 'B', 'E'},
				{'E', 'E', 'E', 'E'},
				{'E', 'B', 'B', 'B'},
				{'E', 'E', 'E', 'G'}
			};
		
		int[] start2 = {0,0,0};
		//Expected out: 13
		System.out.println("Input: ");
		bfsg.printArray2D(input2);
		System.out.print("Output: ");
		bfsg.printPath(bfsg.distanceToGoal(input2, start2));
		System.out.println("");
		
		char[][] input3 = {
				{'S', 'E', 'E', 'E'},
				{'B', 'B', 'B', 'E'},
				{'E', 'E', 'E', 'E'},
				{'E', 'B', 'B', 'E'},
				{'E', 'E','E', 'G'}
			};
		//Expected out: 7
		System.out.println("Input: ");
		bfsg.printArray2D(input3);
		System.out.print("Output: ");
		bfsg.printPath(bfsg.distanceToGoal(input3, start));
		System.out.println("");
		

	}

}
