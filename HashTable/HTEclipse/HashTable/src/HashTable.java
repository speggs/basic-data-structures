import java.util.ArrayList;

//unidirectional Linked List of type agnostic Key value pairs
class HashNode<keyType, valueType>{
	keyType key;
	valueType value;
	
	HashNode<keyType, valueType> next;
	
	public HashNode(keyType pKey, valueType pValue){
		key = pKey;
		value = pValue;
	}
}

//type agnostic HashTable, stores key, values in Linked Lists, the heads of which
//are stored in an ArrayList. Load factor is .7, beyond which the ArrayList is doubled in
//size and the contents of teh buckets are redistributed. Uses Java's HashCode 
//property for objects as the Hash Code negating and % Buckets.length() as the compressor
public class HashTable<keyType, valueType> {
	
	private ArrayList<HashNode<keyType, valueType>> buckets;
	private int numBuckets;
	//total number of key value pairs
	private int size; 
	private double loadFactor;
	
	public HashTable(){
		buckets = new ArrayList<>();
		numBuckets = 10;
		size = 0;
		loadFactor = 0.7;
		
		for(int i =0; i < numBuckets; i++){
			buckets.add(null);
		}
	}
	
	public HashTable(int pSize){
		buckets = new ArrayList<>();
		numBuckets = pSize;
		size = 0;
		loadFactor = 0.7;
		
		for(int i =0; i < numBuckets; i++){
			buckets.add(null);
		}
	}
	
	public HashTable(double pLoadFactor){
		buckets = new ArrayList<>();
		numBuckets = 10;
		size = 0;
		loadFactor = pLoadFactor;
		
		for(int i =0; i < numBuckets; i++){
			buckets.add(null);
		}
	}
	
	public HashTable(int pSize, double pLoadFactor){
		buckets = new ArrayList<>();
		numBuckets = pSize;
		size = 0;
		loadFactor = pLoadFactor;
		
		for(int i =0; i < numBuckets; i++){
			buckets.add(null);
		}
	}
	
	public int getSize(){
		return size;
	}
	
	public boolean isEmpty(){
		return size == 0;
		
	}
	
	private int hashFunction(keyType key){
		int hashCode = key.hashCode();
		int bucketIndex = hashCode % numBuckets;
		return bucketIndex;
	}
	
	public valueType remove(keyType key){
		int bucketIndex = hashFunction(key);
		
		HashNode<keyType, valueType> head = buckets.get(bucketIndex);
		
		//used to mend bucket list when our node is removed
		HashNode<keyType, valueType> prev = null;
		while(head != null){
			if(head.key.equals(key)){
				break;
			}
			
			prev = head;
			head = head.next;
		}
		
		if(head == null){
			return null;
		}
		
		size--;
		
		if(prev != null){//if removing middle element, mend the list
			prev.next = head.next;
		}else{//else removing head element, so attach new head to buckets array
			buckets.set(bucketIndex, head.next);
		}
		return head.value;
	}
	
	public valueType get(keyType key){
		int bucketIndex = hashFunction(key);
		
		HashNode<keyType, valueType> head = buckets.get(bucketIndex);
		
		while(head != null){
			if(head.key.equals(key)){
				return head.value;
			}
		}
		return null;
	}
	
	public void add(keyType key, valueType value){
		int bucketIndex = hashFunction(key);
		HashNode<keyType, valueType> head = buckets.get(bucketIndex);
		
		while(head != null){
			//HashTable already contains key, no need to add
			if(head.key.equals(key)){
				return;
			}
			head = head.next;
		}
		
		size++;
		
		head = buckets.get(bucketIndex);
		HashNode<keyType, valueType> newNode = new HashNode<keyType, valueType>(key, value);
		newNode.next = head;
		buckets.set(bucketIndex, newNode);
		
		//Time to resize, we have exceeded the load factor
		if((1.0*size)/numBuckets >= loadFactor){
			ArrayList<HashNode<keyType, valueType>> temp = buckets;
			buckets = new ArrayList<>();
			numBuckets *= 2;
			size = 0;
			for(int i = 0; i < numBuckets; i++){
				buckets.add(null);
			}
			
			for(HashNode<keyType, valueType> iterHeadNode : temp){
				while(iterHeadNode != null){
					add(iterHeadNode.key, iterHeadNode.value);
					iterHeadNode = iterHeadNode.next;
				}
			}
		}
		
	}
	
	public void printHashTable(){
		int bucketInd = 0;
		for(HashNode<keyType, valueType> iterNode : buckets){
			
			System.out.print("Bucket No "+ bucketInd+":");
			while(iterNode != null){
				System.out.print("(" + iterNode.key + ":" + iterNode.value+"), ");
				iterNode = iterNode.next;
			}
			System.out.println("");
			bucketInd++;
		}
	}
	
	public static void main(String[] args){
//		HashTable<Integer, String> ht = new HashTable<>();
//		HashTable<String, Integer> ht = new HashTable<>();
		HashTable<String, Integer> ht = new HashTable<>(3.0);
		
		String[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
		for(int i = 1; i < alphabet.length; i++){
			ht.add(alphabet[i], i);
		}
		ht.add("J", 11);
		
        System.out.println(ht.getSize());
        System.out.println(ht.remove("J"));
        System.out.println(ht.getSize());
        System.out.println(ht.remove("J"));
        System.out.println(ht.getSize());
        System.out.println(ht.isEmpty());
		ht.printHashTable();
		
/*		HashTable<String, Integer>map = new HashTable<>();
        map.add("this",1 );
        map.add("coder",2 );
        map.add("this",4 );
        map.add("hi",5 );
        System.out.println(map.getSize());
        System.out.println(map.remove("this"));
        System.out.println(map.remove("this"));
        System.out.println(map.getSize());
        System.out.println(map.isEmpty());*/
		
	}
	
}
