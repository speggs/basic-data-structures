class Solution {
    int[] indexes;
	int[] pairs;
    
	public Solution(){
		
	}
	
    public int minSwapsCouples(int[] row) {
        indexes = new int[row.length];
        pairs = new int[row.length];
		
		for(int i = 0; i < row.length; i+=2){
			pairs[i] = i+1;
			pairs[i+1] = i;
		}
		
        for(int i = 0; i < row.length; i ++){
            indexes[row[i]] = i;
        }
        
        return recur(row, 0);
    }
    
    public int recur(int[] row, int index){
        int opt1, opt2;
        int a, b, temp;
		print("index: "+ index + " array: ");
		printArray(row, index);
        a = row[index]; 
        b = row[index + 1];

        if( pairs[a] == b){
            if((row.length - index) == 2){//last pair
                return 0;
            }else{
                return recur(row, index + 2);
            }
        }else{
			temp = indexes[pairs[a]];
            swap(row, index +1, temp);
            opt1 = recur(row, index + 2);
            swap(row, index + 1, temp);

			temp = indexes[pairs[b]];
            swap(row, index, temp);
            opt2 = recur(row, index + 2);
            swap(row, index, temp);
            
            if(opt1 < opt2){
                return opt1 + 1;
            }else{
                return opt2 + 1;
            }
        }
    }

    public void swap(int[] arr, int i, int j){
        int temp = arr[i];
        arr[i] = arr[j];
        arr[j] = temp;
		
        indexes[arr[i]] = i;
        indexes[arr[j]] = j;
    }
	
	void print(String s){
		System.out.println(s);
	}
	
	static void printArray(int arr[], int ind)
    {
		
       // System.out.print("index: ");
		//for (int i=0; i < arr.length; ++i){
       //     System.out.print("(" + i + ") ");
		//}
       // System.out.println();
        //System.out.print("array: ");
        for (int i=ind; i < arr.length; ++i){
            System.out.print(arr[i] + ", ");
		}
        System.out.println();
    }
	
	
	public static void main(String[] args) {
     
		int arr[] = {0,2,1,3};
		
		Solution sln = new Solution();
	 
		System.out.print("Min swaps required is " + sln.minSwapsCouples(arr));
	}
}