import java.util.Scanner;

 class LLNode{
        LLNode next;
        LLNode prev;
        int data;
        
        public LLNode(int pData){
            data = pData;
            next = null;
            prev = null;
        }
		
		public LLNode(int pData, LLNode pPrev){
            data = pData;
            next = null;
            prev = pPrev;
        }
        
        public void setData(int pData){
            data = pData;
        }
        
        public int getData(){
            return data;
        }
        
        public void setNext(LLNode pNext){
            next = pNext;
        }
        
        public LLNode getNext(){
            return next;
        }
        
        public void setPrev(LLNode pPrev){
            prev = pPrev;
        }
        
        public LLNode getPrev(){
            return prev;
        }
}
 
 class LL
 {
	private LLNode root;

	public LL()
	{root = null;  }

	public void insert(int data) { 
		if(root == null){
			root = new LLNode(data);
		}else{
			insert(data, root);
		}
	}

	private void insert(int data, LLNode parent){
		if(parent.getNext() == null){
			parent.setNext(new LLNode(data, parent));
		}else{
			insert(data, parent.getNext());
		}
	} 

	public int countLLNodes()
	{
		LLNode temp = root;
		int count = 0;
		while(temp != null){
			count++;
			temp = temp.getNext();
		}
		return count;
	}



	public boolean search(int val)
	{
		LLNode temp = root;
		while(temp != null){
			if(temp.getData() == val){
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}

	public void print()
	{
		LLNode temp = root;
		while(temp != null){
			System.out.print(temp.getData() +" ");
			temp = temp.getNext();
		}
	}
 }

 
 public class LinkedList
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
        /* Creating object of LL */
        LL ll = new LL(); 
        /*  Perform tree operations  */
        System.out.println("Binary Tree Test\n");          
        char ch;        
        do    
        {
            System.out.println("\nLinked List Operations\n");
            System.out.println("1. insert ");
            System.out.println("2. search");
            System.out.println("3. count LLNodes");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer element to insert");
                ll.insert( scan.nextInt() );                     
                break;                          
            case 2 : 
                System.out.println("Enter integer element to search");
                System.out.println("Search result : "+ ll.search( scan.nextInt() ));
                break;                                          
            case 3 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break;     
            default : 
                System.out.println("Wrong Entry \n ");
                break;   
            }
            System.out.print("\nLinked List : ");
            ll.print();
 
            // System.out.println("\n\nDo you want to continue (Type y or n) \n");
            // ch = scan.next().charAt(0);    
        // } while (ch == 'Y'|| ch == 'y');        
		} while(true);
    }
 }