#include "lists.h"
#include "prints.h"
#include "utils.h"
#include <stdio.h>

using namespace std;

void testMult(){
	Node * in1 = makeMultiLL(7);
	printMultiLL(in1);

	Node * in2 = makeMultiLL(3);
	addChildAt(in1, in2, 3);
	printMultiLL2(in1);

	Node * in3 = makeMultiLL(2);
	addChildAt(in1, in3, 1);
	printMultiLL2(in1);
}

void flattenTest(){
	Node * in1 = makeMultiLL(10);
	Node * in2 = makeMultiLL(5);
	addChildAt(in1, in2, 5);
	Node * in3 = makeMultiLL(7);
	addChildAt(in2, in3, 3);
	printFlattenTest(in1);
}

void testCyc(){
	CycNode * in1 = makeCycLL(vector<int>({ 0, 1, 2, 3, 4, 5, 6, 7 }));
	printCycLL(in1);
	printf("\n\n");
	printCycLL(in1->next->next->next->next);
}

void testCycInsert(){
	//CycNode * in1 = makeCycLL(vector<int>({ 0, 1, 2, 3, 5, 6, 7 }));
	//printCycInsertTest(in1, 4);

	//CycNode * in2 = makeCycLL(vector<int>({ 1, 3, 4 }));
	//printCycInsertTest(in2->next, 2);

	//CycNode * in3 = makeCycLL(vector<int>({ 3, 3, 3 }));
	//printCycInsertTest(in3, 0);

	CycNode * in4 = makeCycLL(vector<int>({ 3, 5, 1 }));
	printCycInsertTest(in4, 0);
}

void testRand(){
	RandomListNode* in1 = makeRandLL(10);
	printRandLL(in1);
}

void testCopyRandomList(){

	RandomListNode* in1 = makeRandLL(10);
	printCopyRandomListTest(in1);
}

int main(){
	//testMult();
	//flattenTest();

	//testCyc();
	//testCycInsert();

	//testRand();
	testCopyRandomList(); 
}