#ifndef UTILS_H
#define UTILS_H

#include "lists.h"
#include <vector>
Node* makeMultiLL(int size);

Node* makeMultiLL(int size);
void addChildAt(Node* parent, Node* child, int intersect);

CycNode* makeCycLL(std::vector<int> arr);

RandomListNode * makeRandLL(int size);
#endif //UTILS_H