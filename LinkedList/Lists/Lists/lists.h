#ifndef LINKEDLIST_H
#define LINKEDLIST_H

#include <stddef.h>
#include <unordered_map>
#include <unordered_set>

class Node {
public:
	int val = NULL;
	Node* prev = NULL;
	Node* next = NULL;
	Node* child = NULL;

	Node() {}

	Node(int _val, Node* _prev, Node* _next, Node* _child);
	Node(int _val);
};

Node* flatten(Node* head);

class CycNode {
public:
	int val = NULL;
	CycNode* next = NULL;

	CycNode() {}

	CycNode(int _val, CycNode* _next) {
		val = _val;
		next = _next;
	}
};

CycNode* insert(CycNode* head, int insertVal);

struct RandomListNode {
	int label;
	RandomListNode *next, *random;
	RandomListNode(int x) : label(x), next(NULL), random(NULL) {}
	
};

RandomListNode *copyRandomList(RandomListNode *head);

#endif