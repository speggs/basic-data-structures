#ifndef PRINTFNCTS_H
#define PRINTFNCTS_H

#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "lists.h"

void printMultiLL(Node* head);
void printMultiLL2(Node * head);
void printFlattenTest(Node * in);
void printCycLL(CycNode * head);
void printCycInsertTest(CycNode* head, int insertVal);
void printRandLL(RandomListNode * head);
void printCopyRandomListTest(RandomListNode * in);
#endif //PRINTFNCTS_H