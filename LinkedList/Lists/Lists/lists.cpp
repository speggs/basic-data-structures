#include "lists.h"
using namespace std;

Node::Node(int _val, Node* _prev, Node* _next, Node* _child) {
	val = _val;
	prev = _prev;
	next = _next;
	child = _child;
}

Node::Node(int _val) {
	val = _val;
	prev = NULL;
	next = NULL;
	child = NULL;
}

Node* flatten(Node* head) {
	Node * curNode = head;
	Node * curChildNode = NULL;
	while (curNode != NULL){
		if (curNode->child != NULL){
			curNode->child = flatten(curNode->child);
			curChildNode = curNode->child;
			while (curChildNode->next != NULL){
				curChildNode = curChildNode->next;
			}
			curChildNode->next = curNode->next;
			if (curChildNode->next != NULL){
				curChildNode->next->prev = curChildNode;
			}
			curNode->next = curNode->child;
			curNode->next->prev = curNode;
			curNode->child = NULL;
		}
		curNode = curNode->next;
	}
	return head;
}

CycNode* insert(CycNode* head, int insertVal) {
	CycNode * newNode = new CycNode(insertVal, NULL);
	if (head == NULL){
		newNode->next = newNode;
		return newNode;
	}
	if (head->next == head){
		head->next = newNode;
		newNode->next = head;
		return head;
	}
	int min = 0;
	int max = 0;
	CycNode* endNode = NULL;
	CycNode* curNode = head->next;
	CycNode* prevNode = head;
	if (curNode->val < prevNode->val){
		if (insertVal < curNode->val || insertVal > prevNode->val){
			prevNode->next = newNode;
			newNode->next = curNode;
			return head;
		}
		curNode = curNode->next;
		prevNode = prevNode->next;
	}
	while (!(prevNode->val <= insertVal && insertVal <= curNode->val)){
		curNode = curNode->next;
		prevNode = prevNode->next;

		if (curNode->val < prevNode->val){
			if (insertVal < curNode->val || insertVal > prevNode->val){
				prevNode->next = newNode;
				newNode->next = curNode;
				return head;
			}
		}

		if (prevNode == head){
			break;
		}

	}
	prevNode->next = newNode;
	newNode->next = curNode;
	return head;
}

//1 Copy Old List node Labels into new list
//	if (CN->R)
//		{LinksLabelsMap[CN->L] = CN->R->L;
//		 LinkedToLabelsSet.insert(CN->R->L)}
//Now we have a set of ALL link labels by label
//2 iterate New List
//	if(LinkedToLabelsSet.count(CN->L) > 0
//		{LinkedToNodesMap[CN->L] = CN;}
//Now we have a set of ALL linked to nodes by label
//3 iterate New List again
//	if(LinksLabelsMap.count(CN->L) > 0){
//		CN->R = LinkedToNodesMap[LinksLabelsMap[CN->L]];	
//	}
//Now we have set up our random links by getting the linkTo label from our first set of link labels, and using it to retrieve the node pointer from our set of LinkedTo nodes
RandomListNode *copyRandomList(RandomListNode *head) {
	unordered_map<int, int> LinksLabelsMap;
	unordered_set<int> LinkedToLabelsSet;
	unordered_map<int, RandomListNode *> LinkedToNodesMap;

	if (head == NULL){
		return head;
	}
	//Current Node Old List
	RandomListNode * cNOL = head;
	RandomListNode * newHead = new RandomListNode(head->label);
	if (head->random != NULL){
		LinksLabelsMap[head->label] = head->random->label;
		LinkedToLabelsSet.insert(head->random->label);
	}
	RandomListNode * cNNL = newHead;

	while (cNOL->next != NULL){
		cNOL = cNOL->next;
		cNNL->next = new RandomListNode(cNOL->label);
		cNNL = cNNL->next;
		if (cNOL->random != NULL){
			LinksLabelsMap[cNOL->label] = cNOL->random->label;
			LinkedToLabelsSet.insert(cNOL->random->label);
		}
	}

	cNNL = newHead;
	while (cNNL != NULL){
		if (LinkedToLabelsSet.count(cNNL->label) > 0){
			LinkedToNodesMap[cNNL->label] = cNNL;
		}
		cNNL = cNNL->next;
	}

	cNNL = newHead;
	while (cNNL != NULL){
		if (LinksLabelsMap.count(cNNL->label) > 0){
			int linkTo = LinksLabelsMap[cNNL->label];
			cNNL->random = LinkedToNodesMap[linkTo];
		}
		cNNL = cNNL->next;
	}
	return newHead;
}