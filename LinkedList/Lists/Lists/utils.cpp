#include "utils.h"
using namespace std;

Node* makeMultiLL(int size){
	if (size <= 0){
		return NULL;
	}
	Node *head = new Node(0);
	if (size == 1){
		return head;
	}
	Node *curNode = new Node(1);
	head->next = curNode;
	curNode->prev = head;

	for (int i = 2; i < size; i++){
		curNode->next = new Node(i);
		curNode->next->prev = curNode;
		curNode = curNode->next;
	}
	return head;
}

void addChildAt(Node* parent, Node* child, int intersect){
	int ind = intersect;
	Node * curNode = parent;
	while (ind > 0){
		ind--;
		curNode = curNode->next;
	}

	curNode->child = child;
}

CycNode* makeCycLL(vector<int> vec){
	if (vec.size() == 0){
		return NULL;
	}
	CycNode *head = new CycNode(vec.at(0), NULL);
	CycNode * curNode = head;
	for (int i = 1; i < vec.size(); i++){
		curNode->next = new CycNode(vec.at(i), NULL);
		curNode = curNode->next;
	}
	curNode->next = head;
	return head;

}

RandomListNode * makeRandLL(int size){
	if (size == 0){
		return NULL;
	}
	RandomListNode *head = new RandomListNode(0);
	if (size == 1){
		return head;
	}
	RandomListNode *curNode = new RandomListNode(1);
	head->next = curNode;

	for (int i = 2; i < size; i++){
		curNode->next = new RandomListNode(i);
		curNode = curNode->next;
	}
	curNode = head;
	int randomLink = 0;
	RandomListNode * linkTo = NULL;
	for (int i = 1; i < size; i++){
		randomLink = rand() % 4 * size;
		linkTo = NULL;
		if (randomLink <= size){
			linkTo = head;
			for (int j = 1; j < randomLink; j++){
				linkTo = linkTo->next;
			}
		}
		curNode->random = linkTo;
		curNode = curNode->next;
	}
	return head;
}