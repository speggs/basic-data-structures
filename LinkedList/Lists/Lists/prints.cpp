#include "prints.h"
using namespace std;

int TestNo = 1;

void printMultiLL(Node * head){
	Node * curNode = head;
	for (int i = 0; i < 40; i++){
		if (curNode == NULL){
			printf("NULL");
			return;
		}
		printf("(%d)[%d]->", i, curNode->val);
		curNode = curNode->next;
	}
}

void printMultiLL2(Node * head){
	Node * curNode = head;
	vector<Node*> lists = vector<Node*>();
	lists.push_back(head);
	int numLists = 1;
	for (int i = 0; i < 40 && numLists > 0; i++){
		for (int j = 0; j < lists.size(); j++){
			if (lists.at(j) == NULL){
				printf("    ");
			}
			else{
				printf("[%d] ", lists.at(j)->val);
				if (lists.at(j)->child != NULL){
					lists.push_back(lists.at(j)->child);
					numLists++;
				}
				lists.at(j) = lists.at(j)->next;
				if (lists.at(j) == NULL){
					numLists--;
				}
			}
		}
		printf("\n");
	}
	printf("\n\n");
}

void printFlattenTest(Node * in){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printMultiLL2(in);
	printf("\nOutputHead: ");
	printMultiLL2(flatten(in));
	printf("\n\n");
	TestNo++;
}

void printCycLL(CycNode * head){
	CycNode * curNode = head;

	for (int i = 0; i < 40; i++){
		if (curNode == NULL){
			printf("NULL");
			return;
		}
		printf("(%d)[%d]->", i, curNode->val);
		curNode = curNode->next;
		if (curNode == head){
			break;
		}
	}
}

void printCycInsertTest(CycNode* head, int insertVal){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printCycLL(head);
	printf("\nInsertVal: %d\nOutputHead: ", insertVal);
	printCycLL(insert(head, insertVal));
	printf("\n\n");
	TestNo++;
}

void printRandLL(RandomListNode * head){
	RandomListNode * curNode = head;

	for (int i = 0; i < 40; i++){
		if (curNode == NULL){
			printf("NULL");
			return;
		}
		printf("(%d)[%d]", i, curNode->label);
		if (curNode->random == NULL){
			printf("{NULL}->");
		}
		else{
			printf("{%d}->", curNode->random->label);
		}
		curNode = curNode->next;
	}
}

void printCopyRandomListTest(RandomListNode * in){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printRandLL(in);
	printf("\nOutputHead: ");
	printRandLL(copyRandomList(in));
	printf("\n\n");
	TestNo++;
}