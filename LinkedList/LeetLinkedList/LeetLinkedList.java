import java.util.Scanner;

class MyLinkedList {
    class LLNode{
        LLNode next;
        LLNode prev;
        int data;
        
        public LLNode(int pData){
            data = pData;
            next = null;
            prev = null;
        }
		
		public LLNode(int pData, LLNode pPrev){
            data = pData;
            next = null;
            prev = pPrev;
        }
        
        public void setData(int pData){
            data = pData;
        }
        
        public int getData(){
            return data;
        }
        
        public void setNext(LLNode pNext){
            next = pNext;
        }
        
        public LLNode getNext(){
            return next;
        }
        
        public void setPrev(LLNode pPrev){
            prev = pPrev;
        }
        
        public LLNode getPrev(){
            return prev;
        }
		
		public void delete(){
			next = null;
			prev = null;
		}
    }
    
    
    
    
	private LLNode root;
    public int size = 0;

	private void insert(int data, LLNode parent){
		if(parent.getNext() == null){
			parent.setNext(new LLNode(data, parent));
		}else{
			insert(data, parent.getNext());
		}
        size++;
	} 

	public boolean search(int val)
	{
		LLNode temp = root;
		while(temp != null){
			if(temp.getData() == val){
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}
    
    public MyLinkedList() {
        root = null;
    }
    
    public int get(int index) {
        LLNode temp = root;
		for(int i=0; i < index; i++){
			if(temp == null){
				return -1;
			}
			temp = temp.getNext();
		}
        if(temp == null){
            return -1;
        }
		return temp.getData();
    }

    public LLNode getNode(int index) {
        LLNode temp = root;
		for(int i=0; i < index; i++){
			if(temp == null){
				return null;
			}
			temp = temp.getNext();
		}
        if(temp == null){
            return null;
        }
		return temp;
    }
    

    public void addAtHead(int val) {
        size++;
		LLNode temp = root;
		root = new LLNode(val);
        if(temp == null){
            return;
        }
		temp.setPrev(root);
		root.setNext(temp);
    }
    
    public void addAtTail(int val) {
        if(root == null){
            root = new LLNode(val);
        }else{
            LLNode temp = root;
            while(temp.getNext() != null){
                temp= temp.getNext();
            }
            temp.setNext(new LLNode(val, temp));
        }
       size++;
    }

    public void addAtIndex(int index, int val) {
		if(index == size){
			addAtTail(val);
		} else if(index == 0){
            addAtHead(val);
            return;
        }else if (0 < index && index < size){
            LLNode temp = getNode(index - 1);
            LLNode temp2 = temp.getNext();
            LLNode newNode = new LLNode(val, temp);
            temp.setNext(newNode);
            newNode.setNext(temp2);
            temp2.setPrev(newNode);
            size++;
        }
    }
    
    public void deleteAtIndex(int index) {
        LLNode temp;
        if(index < 0 || index > size - 1){
            return;
        }
        if(index == 0){
            temp = root;
            if(root.getNext() != null){
                root.getNext().setPrev(null);
            }
            root = root.getNext();
            temp.delete();
            size--;
        }else if(index == size -1){
            temp = getNode(index);
            temp.getPrev().setNext(null);
            temp.delete();
            size--;
        }else{
            temp = getNode(index);
            if(temp.getPrev() != null){
                temp.getPrev().setNext(temp.getNext());
            }
            if(temp.getNext() != null){
                temp.getNext().setPrev(temp.getPrev());
            }
            size--;
        }
            
    }
	
	public void print(){
		LLNode temp = root;
		int i = 0;
		System.out.println("SIZE:" + size);
		while(temp != null){
			System.out.print("(" + i + ")" + temp.getData()+"; ");
			temp = temp.getNext();
			i++;
		}
	}
}

    public boolean hasCycle(ListNode head) {
        ListNode n1 = head;
        ListNode n2 = head;
        while(n2 != null){
            if(n2.next == null){
                return false;
            }
            n2 = n2.next;
            if(n2.next == null){
                return false;
            }
            n2 = n2.next;
            n1 = n1.next;
            if(n1 == n2){
                return true;
            }
        }
        return false;
    }
	
	 public ListNode detectCycle(ListNode head) {
        int mu;
        int lam;
        if(hasCycle(head)){
           ListNode n1 = head.next;
            ListNode n2 = head.next;
            n2 = n2.next;
            while(n1 != n2){
                n1 = n1.next;
                n2 = n2.next;
                n2 = n2.next;
            }
            
            mu = 0;
            n1 = head;
            while(n1 != n2){
                n1 = n1.next;
                n2 = n2.next;
                mu++;
            }
            
            return getNode(mu, head);
            
        }else{
            return null;
        }
    }

 public class LeetLinkedList
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
        /* Creating object of LL */
       MyLinkedList ll = new MyLinkedList(); 
        /*  Perform tree operations  */
        System.out.println("Binary Tree Test\n");          
        char ch;        
        do    
        {
            System.out.println("\nLinked List Operations\n");
            System.out.println("1. Add At Head ");
            System.out.println("2. get");
            System.out.println("3. addAtTail");
            System.out.println("4. addAtIndex");
            System.out.println("5. deleteAtIndex");
            System.out.println("6. print");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer Add At Head");
                ll.addAtHead( scan.nextInt() );                     
                break;                          
            case 2 : 
                System.out.println("Enter integer element to get");
                System.out.println("get result : "+ ll.get( scan.nextInt() ));
                break;                                          
            case 3 :
                System.out.println("Enter integer Add At Tail");
				ll.addAtTail(scan.nextInt());
                break;  
			case 4 : 
                System.out.println("Enter 2 integers addAtIndex");
				int index = scan.nextInt();
				ll.addAtIndex(index, scan.nextInt());
                break; 
			case 5 : 
                System.out.println("Enter integer element to deleteAtIndex");
                ll.deleteAtIndex( scan.nextInt() );
                break; 
			case 6 : 
                ll.print();
                break; 				
            default : 
                System.out.println("Wrong Entry \n ");
                break;   
            }
            System.out.print("\nLinked List : ");
            ll.print();
 
            // System.out.println("\n\nDo you want to continue (Type y or n) \n");
            // ch = scan.next().charAt(0);    
        // } while (ch == 'Y'|| ch == 'y');        
		} while(true);
    }
 }