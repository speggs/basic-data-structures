#include "utils.h"
using namespace std;

ListNode* makeLL(bool hasCycle){
	ListNode *head = new ListNode(0);
	ListNode *curNode = new ListNode(1);
	head->next = curNode;
	ListNode* cycleStart = NULL;
	for (int i = 2; i < 7; i++){
		curNode->next = new ListNode(i);
		if (i == 4){
			cycleStart = curNode;
		}
		curNode = curNode->next;
	}
	if (hasCycle){
		curNode->next = cycleStart;
	}
	return head;
}

ListNode* makeLL(int size){
	if (size <= 0){
		return NULL;
	}
	ListNode *head = new ListNode(0);
	if (size == 1){
		return head;
	}
	ListNode *curNode = new ListNode(1);
	head->next = curNode;
	ListNode* cycleStart = NULL;
	for (int i = 2; i < size; i++){
		curNode->next = new ListNode(i);
		if (i == 4){
			cycleStart = curNode;
		}
		curNode = curNode->next;
	}
	return head;
}

ListNode* makeLL(vector<int> vec){
	if (vec.size() == 0){
		return NULL;
	}
	ListNode *head = new ListNode(vec.at(0));
	ListNode * curNode = head;
	for (int i = 1; i < vec.size(); i++){
		curNode->next = new ListNode(vec.at(i));
		curNode = curNode->next;
	}
	return head;
}

void intersectLists(ListNode * headA, ListNode* headB){
	ListNode * curA = headA;
	ListNode * curB = headB;

	while (curB->next != NULL){
		curB->val += 100;
		curB = curB->next;
	}
	curB->val += 100;

	for (int i = 0; i < 4; i++){
		if (curA->next != NULL && curA->next->next != NULL){
			curA = curA->next;
		}
	}

	curB->next = curA;
}

void concatenateLists(ListNode * headA, ListNode* headB){
	ListNode * curNode = headA;
	while (curNode->next != NULL){
		curNode = curNode->next;
	}
	curNode->next = headB;
}