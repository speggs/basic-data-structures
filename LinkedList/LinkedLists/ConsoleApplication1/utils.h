#ifndef UTILS_H
#define UTILS_H

#include "LinkedList.h"
#include "printFncts.h"
#include <vector>

//Utilities
ListNode* makeLL(bool hasCycle);
ListNode* makeLL(int size);
ListNode* makeLL(std::vector<int> vec);
void intersectLists(ListNode * headA, ListNode* headB);
void concatenateLists(ListNode * headA, ListNode* headB);

#endif //UTILS_H