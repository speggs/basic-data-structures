#include "LinkedList.h"
#include "printFncts.h"
#include "utils.h"

#include "MyLinkedList.h"
#include "MLLPrintFncts.h"
#include "MLLUtils.h"
using namespace std;

void hasCycleTest(){

	ListNode *in1 = makeLL(false);
	printLL(in1);
	printf("\nTestNo: %d\nhasCycle:%s", 1, hasCycle(in1) ? "true" : "false");
	printf("\n\n");

	ListNode *in2 = makeLL(true);
	printLL(in2);
	printf("\nTestNo: %d\nhasCycle:%s", 2, hasCycle(in2) ? "true" : "false");
	printf("\n\n");

	ListNode *in3 = NULL;
	printLL(in3);
	printf("\nTestNo: %d\nhasCycle:%s", 3, hasCycle(in3) ? "true" : "false");
	printf("\n\n");

	ListNode *in4 = &ListNode(0);
	printLL(in4);
	printf("\nTestNo: %d\nhasCycle:%s", 4, hasCycle(in4) ? "true" : "false");
	printf("\n\n");
}

void detectCycleTest(){
	ListNode *in1 = makeLL(false);
	printDetectCycleTest(in1, detectCycle(in1));

	ListNode *in2 = makeLL(true);
	printDetectCycleTest(in2, detectCycle(in2));

	ListNode *in3 = NULL;
	printDetectCycleTest(in3, detectCycle(in3));

	ListNode *in4 = &ListNode(0);
	printDetectCycleTest(in4, detectCycle(in4));
}

void getIntersectionNodeTest(){
	ListNode *inA1 = makeLL(false);
	ListNode* inB1 = makeLL(false);
	intersectLists(inA1, inB1);
	printGetIntersectionNodeTest(inA1, inB1, getIntersectionNode(inA1, inB1));
	
}

void removeNthFromEndTest(){
	ListNode* inHead1 = makeLL(false);
	int inN1 = 2;
	printRemoveNthFromEndTest(inHead1, inN1);

	ListNode* inHead2 = &ListNode(1);
	int inN2 = 1;
	printRemoveNthFromEndTest(inHead2, inN2);

	ListNode* inHead3 = makeLL(false);
	int inN3 = 7;
	printRemoveNthFromEndTest(inHead3, inN3);

	ListNode* inHead4 = makeLL(2);
	int inN4 = 1;
	printRemoveNthFromEndTest(inHead4, inN4);

}

void reverseListTest(){
	ListNode* inHead1 = makeLL(5);
	printReverseListTest(inHead1);

	printReverseListTest(NULL);

	ListNode* inHead2 = makeLL(1);
	printReverseListTest(inHead2);

	ListNode* inHead3 = makeLL(2);
	printReverseListTest(inHead3);
}

void removeElementsTest(){
	ListNode * inHead1 = makeLL(3);
	printRemoveElementsTest(inHead1, 1);


	ListNode * inHead2 = makeLL(3);
	ListNode * tempHead = makeLL(3);
	concatenateLists(inHead2, tempHead);
	tempHead = makeLL(3);
	concatenateLists(inHead2, tempHead);
	printRemoveElementsTest(inHead2, 2);

	printRemoveElementsTest(NULL, 2);

	ListNode * inHead3 = makeLL(1);
	printRemoveElementsTest(inHead3, 0);

	ListNode * inHead4 = makeLL(3);
	printRemoveElementsTest(inHead4, 0);

	ListNode * inHead5 = makeLL(3);
	printRemoveElementsTest(inHead5, 2);
}

void oddEvenListTest(){
	ListNode * inHead1 = makeLL(8);
	printOddEvenListTest(inHead1);

	printOddEvenListTest(NULL);

	ListNode* inHead2 = &ListNode(1);
	printOddEvenListTest(inHead2);

	ListNode* inHead3 = makeLL(2);
	printOddEvenListTest(inHead3);

	ListNode* inHead4 = makeLL(3);
	printOddEvenListTest(inHead4);

	ListNode* inHead5 = makeLL(5);
	printOddEvenListTest(inHead5);
}

void isPalindromeTest(){
	ListNode * inHead1 = makeLL(5);
	printIsPalindromeTest(inHead1);

	ListNode * inHead2 = makeLL(5);
	ListNode * tempLL = makeLL(5);
	tempLL = reverseList(tempLL);
	concatenateLists(inHead2, tempLL);
	printIsPalindromeTest(inHead2);

	ListNode * inHead3 = makeLL(3);
	tempLL = makeLL(2);
	tempLL = reverseList(tempLL);
	concatenateLists(inHead3, tempLL);
	printIsPalindromeTest(inHead3);


	printIsPalindromeTest(NULL);

	ListNode * inHead4 = &ListNode(0);
	printIsPalindromeTest(inHead4);
}

void addTests(){
	//MyLinkedList mll = MyLinkedList();
	//mll.addAtHead(1);
	//mll.addAtTail(3);
	//mll.addAtIndex(1, 2);
	//printAddTests(string("\n.addAtHead(1)\n.addAtTail(3)\naddAtIndex(1, 2)"), &mll);

	//printf("\nget(1): %d\n", mll.get(1));
	//mll.deleteAtIndex(1);
	//printAddTests(string("\ndeleteAtIndex(1)"), &mll);

	MyLinkedList mll2 = MyLinkedList(10);
	printAddTests(string("MyLinkedList(10)"), &mll2);
	mll2.deleteAtIndex(9);
	printAddTests(string("deleteAtIndex(9)\n"), &mll2);
	mll2.deleteAtIndex(8);
	printAddTests(string("deleteAtIndex(8)\n"), &mll2);

	mll2.deleteAtIndex(0);
	mll2.deleteAtIndex(0);
	mll2.deleteAtIndex(0);
	mll2.deleteAtIndex(0);
	printAddTests(string("\ndeleteAtIndex(0)\ndeleteAtIndex(0)\ndeleteAtIndex(0)\ndeleteAtIndex(0);"), &mll2);

	mll2.addAtIndex(0, 3);
	mll2.addAtIndex(0, 2);
	mll2.addAtIndex(0, 1);
	mll2.addAtHead(0);
	printAddTests(string("\naddAtIndex(0, 3)\naddAtIndex(0, 2)\naddAtIndex(0, 1)\naddAtHead(0) "), &mll2);

	//mll2.deleteAtIndex(8);
	//printAddTests(string("deleteAtIndex(8)\n"), &mll2);
}

void mergeTwoListsTest(){
	ListNode * in1 = makeLL(4);
	ListNode * in2 = makeLL(7);
	printMergeTwoListsTest(in1, in2);

	in1 = NULL;
	in2 = makeLL(3);
	printMergeTwoListsTest(in1, in2);
}

void addTwoNumbersTest(){
	ListNode * in1_1 = makeLL(3);
	ListNode * in1_2 = makeLL(3);
	printAddTwoNumbersTest(in1_1, in1_2);

	ListNode * in2_1 = makeLL(vector<int>({ 9, 9, 9 }));
	ListNode* in2_2 = makeLL(vector<int>({ 1 }));
	printAddTwoNumbersTest(in2_1, in2_2);

	ListNode* in3_1 = makeLL(vector<int>({ 1 }));
	ListNode * in3_2 = makeLL(vector<int>({ 9, 9, 9 }));
	printAddTwoNumbersTest(in3_1, in3_2);

	ListNode * in4_1 = makeLL(vector<int>({ 9, 9, 9 }));
	printAddTwoNumbersTest(in4_1, NULL);
	printAddTwoNumbersTest(NULL, in4_1);
	printAddTwoNumbersTest(NULL, NULL);

	ListNode* in5_1 = makeLL(vector<int>({ 5 }));
	ListNode * in5_2 = makeLL(vector<int>({ 5 }));
	printAddTwoNumbersTest(in5_1, in5_2);

	ListNode* in6_1 = makeLL(vector<int>({ 9, 8 }));
	ListNode * in6_2 = makeLL(vector<int>({ 1 }));
	printAddTwoNumbersTest(in6_1, in6_2);
}

void rotateRightTest(){
	ListNode * in1 = makeLL(5);
	printRotateRightTest(in1, 2);

	ListNode * in2 = makeLL(5);
	printRotateRightTest(in2, 7);

	printRotateRightTest(NULL, 7);

	ListNode * in3 = makeLL(5);
	printRotateRightTest(in3, 5);
}

int main(){
//	hasCycleTest();
//	detectCycleTest();
	//getIntersectionNodeTest();
	//removeNthFromEndTest();
	//reverseListTest();
	//removeElementsTest();
	//oddEvenListTest();
	//isPalindromeTest();

	//addTests();

	//mergeTwoListsTest();
	//addTwoNumbersTest();
	rotateRightTest();
}