#ifndef UTILS_H
#define UTILS_H

#include <vector>
#include "MyLinkedList.h"
#include "MLLPrintFncts.h"

//Utilities
void addToMLL(MyLinkedList * list, int size);
void addArrToMLL(MyLinkedList * list, std::vector<int> vals);
#endif //UTILS_H