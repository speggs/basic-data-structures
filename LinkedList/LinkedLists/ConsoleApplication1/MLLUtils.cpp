#include "MLLUtils.h"
using namespace std;

void addToMLL(MyLinkedList *list, int size){
	for (int i = 0; i <= size; i++){
		list->addAtTail(i);
	}
}

void addArrToMLL(MyLinkedList * list, vector<int> vals){
	for (int i = 0; i < vals.size(); i++){
		list->addAtTail(vals[i]);
	}
}