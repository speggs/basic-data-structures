#include "MLLPrintFncts.h"
using namespace std;

int MLLTestNo = 1;

//void printMLL(MyListNode * head){
//	MyListNode * curNode = head;
//	for (int i = 0; i < 40; i++){
//		if (curNode == NULL){
//			printf("NULL");
//			return;
//		}
//		printf("(%d)[%d]->", i, curNode->val);
//		curNode = curNode->next;
//	}
//}

void printMLL(MyLinkedList * list){
	for (int i = 0; i < 40; i++){
		if (list->get(i) == -1){
			printf("-1/NULL");
			return;
		}
		printf("(%d)[%d]->", i, list->get(i));
	}
}

void printAddTests(string desc, MyLinkedList * outList){
	printf("MLLTestNo: %d\nOperations: %s\n",MLLTestNo, desc.c_str());
	printf("Output: ");
	printMLL(outList);
	printf("\n\n");
	MLLTestNo++;
}