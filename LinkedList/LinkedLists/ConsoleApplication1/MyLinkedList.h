#ifndef MYLINKEDLIST_H
#define MYLINKEDLIST_H

#include "stddef.h"

struct MyListNode{
	int val;
	MyListNode *next;
	MyListNode *prev;
	MyListNode(int x) :val(x), next(NULL), prev(NULL){}
};

class MyLinkedList{
public:
	MyLinkedList();
	MyLinkedList(int pSize);
	int get(int index);
	void addAtHead(int val);
	void addAtTail(int val);
	void addAtIndex(int index, int val);
	void deleteAtIndex(int index);


	MyListNode * head;
	int size;
};

#include "MLLPrintFncts.h"
#include "MLLUtils.h"
#endif //MYLINKEDLIST_H