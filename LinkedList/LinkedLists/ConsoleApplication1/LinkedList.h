#ifndef LINKEDLIST_H
#define LINKEDLIST_H

//#include <vector>
#include "stddef.h"


struct ListNode{
	int val;
	ListNode *next;
	ListNode(int x) :val(x), next(NULL){}
};

#include "printFncts.h"
#include "utils.h"


//Solutions
ListNode *detectCycle(ListNode *head);
bool hasCycle(ListNode *head);
ListNode *getIntersectionNode(ListNode *headA, ListNode *headB);
ListNode* removeNthFromEnd(ListNode* head, int n);
ListNode* reverseList(ListNode *head);
ListNode* removeElements(ListNode* head, int val);
ListNode* oddEvenList(ListNode* head);
bool isPalindrome(ListNode* head);
ListNode* mergeTwoLists(ListNode* l1, ListNode* l2);
ListNode* addTwoNumbers(ListNode* l1, ListNode* l2);
ListNode* rotateRight(ListNode* head, int k);

#endif //LINKEDLIST_H