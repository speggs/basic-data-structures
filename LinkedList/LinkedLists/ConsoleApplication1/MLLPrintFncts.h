#ifndef MLLPRINTFNCTS_H
#define MLLPRINTFNCTS_H

#include <string>
#include <stdio.h>
#include <iostream>

#include "MyLinkedList.h"

//Printers
//void printMLL(MyListNode * head);
void printMLL(MyLinkedList* list);
void printAddTests(std::string desc, MyLinkedList* outList);

#endif //MLLPRINTFNCTS_H