#ifndef PRINTFNCTS_H
#define PRINTFNCTS_H

#include <string>
#include <stdio.h>
#include <iostream>

#include "LinkedList.h"

//Printers
void printLL(ListNode * head);
void printDetectCycleTest(ListNode* in, ListNode * out);
void printGetIntersectionNodeTest(ListNode* inA, ListNode* inB, ListNode* out);
void printRemoveNthFromEndTest(ListNode * inHead, int inN);
void printReverseListTest(ListNode * inHead);
void printRemoveElementsTest(ListNode* inHead, int inVal);
void printOddEvenListTest(ListNode* inHead);
void printIsPalindromeTest(ListNode* inHead);
void printMergeTwoListsTest(ListNode* in1, ListNode* in2);
void printAddTwoNumbersTest(ListNode* in1, ListNode* in2);
void printRotateRightTest(ListNode* inHead, int inK);

#endif //PRINTFNCTS_H