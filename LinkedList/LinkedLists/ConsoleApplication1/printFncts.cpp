#include "printFncts.h"
using namespace std;

int TestNo = 1;

void printLL(ListNode * head){
	ListNode * curNode = head;
	for (int i = 0; i < 40; i++){
		if (curNode == NULL){
			printf("NULL");
			return;
		}
		printf("(%d)[%d]->", i, curNode->val);
		curNode = curNode->next;
	}
}

void printDetectCycleTest(ListNode* in, ListNode * out){
	printf("\nTestNo: %d\nInput: ", TestNo);
	printLL(in);
	if (out == NULL){

		printf("\nout: NULL\n\n");
	}
	else{
		printf("\nout: %d\n\n", out->val);
	}
	TestNo++;
}

void printGetIntersectionNodeTest(ListNode* inA, ListNode* inB, ListNode* out){
	printf("\nTestNo: %d\nInputA: ", TestNo);
	printLL(inA);
	printf("\nInputB: ");
	printLL(inB);

	if (out == NULL){

		printf("\nout: NULL\n\n");
	}
	else{
		printf("\nout: %d\n\n", out->val);
	}

	TestNo++;
}

void printRemoveNthFromEndTest(ListNode * inHead, int inN){
	printf("\nTestNo: %d\ninN: %d\nInputHead: ", TestNo, inN);
	printLL(inHead);
	printf("\nOutputHead: ");
	printLL(removeNthFromEnd(inHead, inN));
	printf("\n\n");
	TestNo++;
}

void printReverseListTest(ListNode* inHead){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printLL(inHead);
	printf("\nOutputHead: ");
	printLL(reverseList(inHead));
	printf("\n\n");
	TestNo++;
}

void printRemoveElementsTest(ListNode* inHead, int inVal){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printLL(inHead);
	printf("\ninVal: %d\nOutputHead: ", inVal);
	printLL(removeElements(inHead, inVal));
	printf("\n\n");
	TestNo++;
}

void printOddEvenListTest(ListNode* inHead){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printLL(inHead);
	printf("\nOutputHead: ");
	printLL(oddEvenList(inHead));
	printf("\n\n");
	TestNo++;
}

void printIsPalindromeTest(ListNode* inHead){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printLL(inHead);
	printf("\nOutput: %s\n\n", isPalindrome(inHead) ? "TRUE" : "FALSE");
	TestNo++;
}

void printMergeTwoListsTest(ListNode* in1, ListNode* in2){
	printf("\nTestNo: %d\nInputA: ", TestNo);
	printLL(in1);
	printf("\nInputB: ");
	printLL(in2);

	ListNode * out = mergeTwoLists(in1, in2);

	printf("\noutput List: ");
	printLL(out);
	printf("\n\n");

	TestNo++;
}

void printAddTwoNumbersTest(ListNode* in1, ListNode* in2){
	printf("\nTestNo: %d\nInputA: ", TestNo);
	printLL(in1);
	printf("\nInputB: ");
	printLL(in2);

	ListNode * out = addTwoNumbers(in1, in2);

	printf("\noutput List: ");
	printLL(out);
	printf("\n\n");

	TestNo++;
}

void printRotateRightTest(ListNode* inHead, int inK){
	printf("\nTestNo: %d\nInputHead: ", TestNo);
	printLL(inHead);
	printf("\ninK: %d\nOutput: ", inK);
	printLL(rotateRight(inHead, inK));
	printf("\n\n");
	TestNo++;
}