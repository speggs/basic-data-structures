#include "MyLinkedList.h"
using namespace std;


MyLinkedList::MyLinkedList() {
	head = NULL;
	size = 0;
}

MyLinkedList::MyLinkedList(int pSize) {
	addToMLL(this, pSize);
}

int MyLinkedList::get(int index) {
	MyListNode * curNode = head;
	if (index > size - 1) {
		return -1;
	}

	for (int i = 0; i < index && curNode != NULL; i++){
		curNode = curNode->next;
	}
	if (curNode == NULL){
		return -1;
	}
	return curNode->val;
}

void MyLinkedList::addAtHead(int val) {
	MyListNode * newNode = new MyListNode(val);
	newNode->next = head;
	if (head != NULL){
		head->prev = newNode;
	}
	head = newNode;
	size++;
}

void MyLinkedList::addAtTail(int val) {
	MyListNode * newNode = new MyListNode(val);
	if (head == NULL){
		head = newNode;
		return;
	}
	MyListNode * curNode = head;

	while (curNode->next != NULL){
		curNode = curNode->next;
	}
	curNode->next = newNode;
	newNode->prev = curNode;
	size++;
}


void MyLinkedList::addAtIndex(int index, int val) {
	if (index == 0){
		addAtHead(val);
		return;
	}

	if (index > size){
		return;
	}
	MyListNode * newNode = new MyListNode(val);
	MyListNode * curNode = head;

	for (int i = 0; i < index - 1 && curNode != NULL; i++){
		curNode = curNode->next;
	}

	if (curNode == NULL){
		return;
	}

	if (curNode->next != NULL){
		curNode->next->prev = newNode;
	}
	newNode->next = curNode->next;

	curNode->next = newNode;
	newNode->prev = curNode;

	size++;
}

/** Delete the index-th node in the linked list, if the index is valid. */
void MyLinkedList::deleteAtIndex(int index) {
	if (index >= size){
		return;
	}
	MyListNode * curNode = head;
	MyListNode * toDelete = NULL;

	if (index == 0){
		if (head == NULL){
			return;
		}
		if (head->next != NULL){
			head->next->prev = NULL;
		}
		toDelete = head;
		head = head->next;

		delete toDelete;
		size--;
		return;
	}

	for (int i = 0; i < index - 1 && curNode != NULL; i++){
		curNode = curNode->next;
	}
	if (curNode == NULL || curNode->next == NULL){
		return;
	}

	if (curNode->next->next != NULL){
		curNode->next->next->prev = curNode;
	}
	toDelete = curNode->next;
	curNode->next = curNode->next->next;
	delete toDelete;

	size--;
}