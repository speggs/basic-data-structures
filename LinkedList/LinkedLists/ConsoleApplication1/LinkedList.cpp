#include "LinkedList.h"
using namespace std;


//struct ListNode{
//	int val;
//	ListNode *next;
//	ListNode(int x) :val(x), next(NULL){}
//};

ListNode *detectCycle(ListNode *head){
	ListNode * fast = head;
	ListNode * slow = head;
	bool reset = false;

	if (head == NULL){
		return NULL;
	}

	while (true){
		if (fast->next != NULL){
			if (!reset){
				fast = fast->next;
			}
		}
		else{
			return NULL;
		}

		if (fast->next != NULL){
			fast = fast->next;
		}
		else{
			return NULL;
		}

		slow = slow->next;

		if (fast == slow){
			if (reset){
				return slow;
			}
			else{
				slow = head;
				reset = true;
				if (slow == fast){
					return slow;
				}
			}
		}
	}
}

bool hasCycle(ListNode *head) {
	ListNode * fast = head;
	ListNode * slow = head;

	if (head == NULL){
		return false;
	}

	while (true){
		if (fast->next != NULL){
			fast = fast->next;
		}
		else{
			return false;
		}

		if (fast->next != NULL){
			fast = fast->next;
		}
		else{
			return false;
		}

		slow = slow->next;

		if (fast == slow){
			return true;
		}
	}
}

//1 test if there is an intersection (go to end of lists)
//2 reverse B
//3 create cycle from new B tail to A head
//4 detect cycle
//5 Break link from NBT to AH
//6 Reverse
ListNode *getIntersectionNode(ListNode *headA, ListNode *headB) {
	ListNode * curA = headA;
	ListNode* curB = headB;
	ListNode* ret = NULL;

	if (headA == NULL || headB == NULL){
		return NULL;
	}

	while (curA->next != NULL){
		curA = curA->next;
	}

	while (curB->next != NULL){
		curB = curB->next;
	}
	if (curB != curA){
		return NULL;
	}

	reverseList(headB);
	headB->next = headA;
	ret = detectCycle(curB);
	headB->next = NULL;
	reverseList(curB);
	return ret;
}

ListNode* removeNthFromEnd(ListNode* head, int n) {
	ListNode * curNode = head;
	ListNode * trailerNode = head;

	int i = 0;

	while (curNode != NULL){
		if (i > n){
			trailerNode = trailerNode->next;
		}
		else{
			i++;
		}
		curNode = curNode->next;
	}

	if (trailerNode == head && i<=n){
		trailerNode = head->next;
		head = NULL;
		return trailerNode;
	}

	if (trailerNode->next != NULL){
		trailerNode->next = trailerNode->next->next;
	}
	return head;
}


ListNode * reverseList(ListNode *head){
	if (head == NULL || head->next == NULL){
		return head;
	}
	ListNode * newHead = head;
	ListNode * curNode = head->next;
	ListNode * tempNext = head->next->next;
	head->next = NULL;

	while (curNode != NULL){
		curNode->next = newHead;
		newHead = curNode;
		curNode = tempNext;
		if (tempNext != NULL){
			tempNext = tempNext->next;
		}
	}

	return newHead;

	/*ListNode * curNode = head;
	ListNode * tempPrev = NULL;
	ListNode * tempNext = curNode->next;
	while (curNode != NULL){
	curNode->next = tempPrev;
	tempPrev = curNode;

	curNode = tempNext;
	if (curNode == NULL){
	return tempPrev;
	}
	tempNext = tempNext->next;
	}*/

}

ListNode* removeElements(ListNode* head, int val) {
	ListNode* newHead = head;
	while (newHead != NULL && newHead->val == val){
		newHead = newHead->next;
	}
	if (newHead == NULL){
		return NULL;
	}
	ListNode* curNode = newHead->next;
	ListNode* prevNode = newHead;

	while (curNode != NULL){
		if (curNode->val == val){
			curNode = curNode->next;
			prevNode->next = curNode;
		}
		else{
			curNode = curNode->next;
			prevNode = prevNode->next;
		}
	}

	return newHead;
}

ListNode* oddEvenList(ListNode* head) {
	if (head == NULL || head->next == NULL){
		return head;
	}
	ListNode* evenHead = head->next;
	ListNode* curOdd = head;
	ListNode* curEven = head->next;
	ListNode* curNode = head->next->next;
	bool odd = true;
	while (curNode != NULL){
		if (odd){
			curOdd->next = curNode;
			curOdd = curOdd->next;
			odd = false;
		}
		else{
			curEven->next = curNode;
			curEven = curEven->next;
			odd = true;
		}
		curNode = curNode->next;


	}
	curOdd->next = evenHead;
	if (!odd){
		curEven->next = NULL;
	}
	return head;
}

bool isPalindrome(ListNode* head) {
	if (head == NULL || head->next == NULL){
		return true;
	}
	ListNode* curNodeFw = head;
	ListNode* curNodeBk = head;
	ListNode* middle = NULL;

	while (1){
		if (curNodeBk->next == NULL){
			break;
		}
		if (curNodeBk->next->next == NULL)
		{
			curNodeBk = curNodeBk->next;
			break;
		}
		curNodeBk = curNodeBk->next->next;
		curNodeFw = curNodeFw->next;
	}
	//now CurNodeBk is at last node
	//cNFw is at the middle node
	middle = curNodeFw->next;
	curNodeFw->next = NULL;
	curNodeFw = head;

	reverseList(middle);

	while (curNodeBk != NULL){
		if (curNodeBk->val != curNodeFw->val){
			return false;
		}
		curNodeBk = curNodeBk->next;
		curNodeFw = curNodeFw->next;
	}
	return true;
}

ListNode* mergeTwoLists(ListNode* l1, ListNode* l2) {
	if (l1 == NULL){
		return l2;
	}
	if (l2 == NULL){
		return l1;
	}
	ListNode* curNode1 = l1;
	ListNode* curNode2 = l2;
	ListNode* newListEnd = NULL;
	ListNode* newListHead = NULL;

	if (l1->val <= l2->val){
		newListEnd = l1;
		newListHead = l1;
		curNode1 = curNode1->next;
	}
	else{
		newListEnd = l2;
		newListHead = l2;
		curNode2 = curNode2->next;
	}

	while (curNode1 != NULL && curNode2 != NULL){
		if (curNode1->val <= curNode2->val){
			newListEnd->next = curNode1;
			curNode1 = curNode1->next;
			newListEnd = newListEnd->next;
		}
		else{
			newListEnd->next = curNode2;
			curNode2 = curNode2->next;
			newListEnd = newListEnd->next;
		}
	}

	if (curNode1 != NULL){
		newListEnd->next = curNode1;
	}
	if (curNode2 != NULL){
		newListEnd->next = curNode2;
	}

	return newListHead;
}

ListNode* addTwoNumbers(ListNode* l1, ListNode* l2) {
	if (l1 == NULL){
		return l2;
	}
	if (l2 == NULL){
		return l1;
	}
	ListNode* curNode1 = l1;
	ListNode* curNode2 = l2;
	bool carryTheOne = false;
	bool needNewNode = false;
	int sum = 0;

	while (curNode1 != NULL && curNode2 != NULL){
		sum = curNode1->val + curNode2->val;
		if (carryTheOne){
			sum++;
			carryTheOne = false;
		}
		if (sum >= 10){
			carryTheOne = true;
			sum -= 10;
		}
		curNode1->val = sum;
		curNode2 = curNode2->next;
		if (curNode1->next == NULL){
			needNewNode = true;
			break;
		}
		curNode1 = curNode1->next;
	}
	if (curNode2 != NULL){
		curNode1->next = curNode2;
		if (carryTheOne){
			while (curNode2 != NULL && carryTheOne){
				curNode2->val++;
				carryTheOne = false;
				if (curNode2->val >= 10){
					carryTheOne = true;
					curNode2->val -= 10;
				}
				if (curNode2->next == NULL && carryTheOne){
					curNode2->next = new ListNode(1);
					carryTheOne = false;
				}
				curNode2 = curNode2->next;
			}
		}
	}
	else if (curNode1->next != NULL && carryTheOne){
		while (curNode1 != NULL && carryTheOne){
			curNode1->val++;
			carryTheOne = false;
			if (curNode1->val >= 10){
				carryTheOne = true;
				curNode1->val -= 10;
			}
			if (curNode1->next == NULL && carryTheOne){
				curNode1->next = new ListNode(1);
				carryTheOne = false;
			}
			curNode1 = curNode1->next;
		}
	}

	if (carryTheOne){
		if (needNewNode){
			curNode1->next = new ListNode(1);
		}
		else{
			curNode1->val++;
			if (curNode1->val >= 10){
				curNode1->val -= 10;
				curNode1->next = new ListNode(1);
			}
		}
	}

	return l1;
}

ListNode* rotateRight(ListNode* head, int k) {
	if (head == NULL || head->next == NULL){
		return head;
	}
	int size = 1;
	ListNode* curNode = head;
	ListNode * newTail = head;
	ListNode * retHead = head;
	while (curNode->next != NULL){
		curNode = curNode->next;
		size++;
	}

	int realK = k % size;
	if (realK == 0){
		return head;
	}
	int indFromLeft = size - realK;
	retHead = retHead->next;
	while (indFromLeft > 1){
		indFromLeft--;
		newTail = newTail->next;
		retHead = retHead->next;
	}
	newTail->next = NULL;
	curNode->next = head;
	return retHead;

}