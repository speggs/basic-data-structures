import java.util.Scanner;

 class LLNode{
        LLNode next;
        LLNode prev;
        int data;
        
        public LLNode(int pData){
            data = pData;
            next = null;
            prev = null;
        }
		
		public LLNode(int pData, LLNode pPrev){
            data = pData;
            next = null;
            prev = pPrev;
        }
        
        public void setData(int pData){
            data = pData;
        }
        
        public int getData(){
            return data;
        }
        
        public void setNext(LLNode pNext){
            next = pNext;
        }
        
        public LLNode getNext(){
            return next;
        }
        
        public void setPrev(LLNode pPrev){
            prev = pPrev;
        }
        
        public LLNode getPrev(){
            return prev;
        }
		
		public void delete(){
			next = null;
			prev = null;
		}
}
 
 class LL
 {
	private LLNode root;
	private int size = 0;
	
	public LL()
	{root = null;  }

	public void insert(int data) { 
		if(root == null){
			root = new LLNode(data);
		}else{
			insert(data, root);
		}
		size++;
	}

	private void insert(int data, LLNode parent){
		if(parent.getNext() == null){
			parent.setNext(new LLNode(data, parent));
		}else{
			insert(data, parent.getNext());
		}
	} 

	public boolean search(int val)
	{
		LLNode temp = root;
		while(temp != null){
			if(temp.getData() == val){
				return true;
			}
			temp = temp.getNext();
		}
		return false;
	}
	
	public int getAt(int index){
		LLNode temp = root;
		for(int i=0; i < index; i++){
			if(temp == null){
				return -1;
			}
			temp = temp.getNext();
		}
		return temp.getData();
	}
	
	    /** Add a node of value val before the first element of the linked list. After the insertion, the new node will be the first node of the linked list. */
    public void addAtHead(int val) {
		LLNode temp = root;
		root = new LLNode(val);
		if(temp == null){
			return;
		}
		temp.setPrev(root);
		root.setNext(temp);
		size++;
	}
    
    /** Add a node of value val before the index-th node in the linked list. If index equals to the length of linked list, the node will be appended to the end of linked list. If index is greater than the length, the node will not be inserted. */
    public void addAtIndex(int index, int val) {
        LLNode temp = root;
		LLNode newNode;
		if(index == size){
			insert(val);
			return;
		}
		for(int i=0; i < index; i++){
			if(temp == null){
				return;
			}
			temp = temp.getNext();
		}
		newNode = new LLNode(data, temp);
		newNode.setNext(temp.getNext());
		if(newNode.getNext() != null){
			newNode.getNext().setPrev(newNode);
		}
		temp.setNext(newNode);
		size++;
    }
    
    /** Delete the index-th node in the linked list, if the index is valid. */
    public void deleteAtIndex(int index) {
        LLNode temp = root;
		for(int i=0; i < index; i++){
			if(temp == null){
				return;
			}
			temp = temp.getNext();
		}
		if(temp.getNext() != null){
			temp.getPrev().setNext(temp.getNext());
			temp.getNext().setPrev(temp.getPrev());
			
		}
		temp.delete();
		size--;
    }
 }

 
 public class SimpleLinkedList
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
        /* Creating object of LL */
        LL ll = new LL(); 
        /*  Perform tree operations  */
        System.out.println("Binary Tree Test\n");          
        char ch;        
        do    
        {
            System.out.println("\nLinked List Operations\n");
            System.out.println("1. insert ");
            System.out.println("2. search");
            System.out.println("3. count LLNodes");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer element to insert");
                ll.insert( scan.nextInt() );                     
                break;                          
            case 2 : 
                System.out.println("Enter integer element to search");
                System.out.println("Search result : "+ ll.search( scan.nextInt() ));
                break;                                          
            case 3 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break;  
			case 4 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break; 
			case 5 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break; 
			case 6 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break; 
			case 7 : 
                System.out.println("LLNodes = "+ ll.countLLNodes());
                break; 				
            default : 
                System.out.println("Wrong Entry \n ");
                break;   
            }
            System.out.print("\nLinked List : ");
            ll.print();
 
            // System.out.println("\n\nDo you want to continue (Type y or n) \n");
            // ch = scan.next().charAt(0);    
        // } while (ch == 'Y'|| ch == 'y');        
		} while(true);
    }
 }