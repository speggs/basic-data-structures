#ifndef BINARYSEARCHTREE_H
#define BINARYSEARCHTREE_H

#include <stddef.h>
#include "stddef.h"
#include <vector>
#include <queue>
#include <unordered_map>

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

class BSTIterator {
public:
	BSTIterator(TreeNode *root);
	bool hasNext();
	int next();
private:
	std::vector<int> inorder;
	std::vector<int>::iterator bSTit;
};

struct TreeNodeCount {
	int val;
	int count;
	int dups;
	TreeNodeCount *left;
	TreeNodeCount *right;
	TreeNodeCount(int x) : val(x), count(1), dups(1), left(NULL), right(NULL) {}
};

class KthLargest {
private:
	TreeNodeCount * root;
	int key;
public:
	KthLargest(int k, std::vector<int> nums);
	int add(int val);
};

#include "printers.h"
#include "utils.h"

bool isValidBST(TreeNode* root);
TreeNode* insertIntoBST(TreeNode* root, int val);
TreeNode* deleteNode(TreeNode* root, int key);
TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q);
bool containsNearbyAlmostDuplicate(std::vector<int>& nums, int k, int t);
bool isBalanced(TreeNode* root);
int maxDepth(TreeNode* root);
int minDepth(TreeNode* root);
TreeNode* sortedArrayToBST(std::vector<int>& nums);
TreeNode * addChildrenVec(std::vector<int> nums, int start, int end);

#endif //BINARYSEARCHTREE_H