#ifndef UTILS_H
#define UTILS_H

#include "binarySearchTree.h"
#include <vector>

TreeNode * makeBST(int height);
void addChildren(TreeNode * root, int height);

std::vector<int> preorderTraversal(TreeNode* root);
void addToVecPre(std::vector<int>& vec, TreeNode* root);

std::vector<int> inorderTraversal(TreeNode* root);
void addToVecIn(std::vector<int>& vec, TreeNode* root);

std::vector<int> postorderTraversal(TreeNode* root);
void addToVecPost(std::vector<int>& vec, TreeNode* root);

std::vector<std::vector<int>> levelOrder(TreeNode* root);

#endif //UTILS_H