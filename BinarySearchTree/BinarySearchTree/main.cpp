#include "binarySearchTree.h"
#include "printers.h"
#include "utils.h"
#include <stdio.h>

using namespace std;

void testMakeBST(){
	TreeNode * in = makeBST(3);
	printBT(in);
}

void testIsValidBST(){
	TreeNode * in = makeBST(3);
	printIsValidBSTTest(in);

	TreeNode * in2 = makeBST(3);
	in2->val = 1;
	printIsValidBSTTest(in2);
}

void testBSTIterator(){
	TreeNode * in = makeBST(3);
	printBSTIteratorTest(in);
}

void testInsertBST(){
	TreeNode * in = makeBST(3);
	printInsertBSTTest(in);
}

void testDeleteNode(){
	TreeNode * in = makeBST(3);
	int key = 1;
	printDeleteNodeTest(in, key);

	int key2 = 2;
	printDeleteNodeTest(in, key2);

	int key3 = 4;
	printDeleteNodeTest(in, key3);
}

void testKthLargest(){
	int TestNo = 1;
	vector<int> nums = vector<int>({ 1, 2, 3, 4, 5, 6, 7 });
	int key = 3;
	KthLargest kl = KthLargest(key, nums);
	printf("Test: %d\nInput nums: \n", TestNo);
	printVector(nums);
	printf("\nInput Key: %d", key);
	printf("\nadd(0): %d", kl.add(0));
	printf("\nadd(8): %d", kl.add(8));
	printf("\nadd(8): %d", kl.add(8));
	printf("\nadd(8): %d", kl.add(8));
	printf("\nadd(8): %d", kl.add(8));
	printf("\nadd(9): %d", kl.add(9));
	printf("\nadd(9): %d", kl.add(9));
	printf("\nadd(9): %d", kl.add(9));
	printf("\nadd(9): %d", kl.add(9));

	printf("\n\n\n");
	TestNo++;

	vector<int> nums2 = vector<int>({});
	int key2 = 1;
	KthLargest kl2 = KthLargest(key2, nums2);
	printf("Test: %d\nInput nums: \n", TestNo);
	printVector(nums);
	printf("\nInput Key: %d", key);
	printf("\nadd(-3): %d", kl2.add(-3));
	printf("\nadd(-2): %d", kl2.add(-2));
	printf("\nadd(-4): %d", kl2.add(-4));
	printf("\nadd(-0): %d", kl2.add(-0));
	printf("\nadd(-4): %d", kl2.add(-4));
}

void testLowestCommonAncestor(){
	TreeNode* in = makeBST(3);
	TreeNode* p = in->left->left;
	TreeNode* q = in->right->right;
	printLCATest(in, p, q);


	TreeNode* in2 = makeBST(4);
	TreeNode* p2 = in2->right->left->left;
	TreeNode* q2 = in2->right->right->right;
	printLCATest(in2, p2, q2);

	TreeNode* in3 = new TreeNode(2);
	in3->left = new TreeNode(1);
	printLCATest(in3, in3, in3->left);
}

void testContainsNearbyAlmostDuplicate(){
	vector<int> nums = vector<int>({ 1, 2, 3, 1 });
	int k = 3;
	int t = 0;
	printCNALDTest(nums, k, t);

	vector<int> nums2 = vector<int>({ 1,0,1,1 });
	int k2 = 1;
	int t2 = 2;
	printCNALDTest(nums2, k2, t2);

	vector<int> nums3 = vector<int>({ 1, 5, 9, 1, 5, 9});
	int k3 = 2;
	int t3 = 3;
	printCNALDTest(nums3, k3, t3);
		
	vector<int> nums4 = vector<int>({ -1, 2147483647 });
	int k4 = 1;
	int t4 = 2147483647;
	printCNALDTest(nums4, k4, t4);
}

void testIsBalanced(){
	TreeNode * in = makeBST(3);
	printIsBalancedTest(in);

	delete in->left->right;
	in->left->right = NULL;
	in->left->left->left = new TreeNode(-7);
	printIsBalancedTest(in);
}

void testSortedArrayToBST(){
	//vector<int> in = vector<int>({ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12 });
	//printSortedArrayToBSTTest(in);


	vector<int> in2 = vector<int>({ -10, -3, 0, 5, 9 });
	printSortedArrayToBSTTest(in2);
}

int main(){
	//testMakeBST();
	//testIsValidBST();
	//testBSTIterator();
	//testInsertBST();
	//testDeleteNode();
	//testKthLargest();
	//testLowestCommonAncestor();
	//testContainsNearbyAlmostDuplicate();
	//testIsBalanced();
	testSortedArrayToBST();
}