#include "printers.h"
using namespace std;

int TestNo = 1;

void printBT(TreeNode * root){

	vector<vector<int>> vals = vector<vector<int>>();

	printBTLvlOrder(root, 0, vals);
	printf("\n\n");
	int lvls = vals.size();
	for (int i = 0; i < lvls; i++){
		for (int k = 0; k < lvls - i - 1; k++){
			printf("   ");
		}
		int lvlSize = vals.at(i).size();
		for (int j = 0; j < lvlSize; j++){
			printf("(%d) ", vals.at(i).at(j));
			for (int k = 0; k < lvls - i - 1; k++){
				printf("   ");
			}
		}
		printf("\n");
		vals.at(i).clear();
	}
	vals.clear();
}

void printBTLvlOrder(TreeNode * root, int level, vector<vector<int>>& vals){


	if (vals.size() <= level){
		vals.push_back(vector<int>());
	}
	if (root == NULL){
		vals.at(level).push_back(-1);
		return;
	}
	vals.at(level).push_back(root->val);

	printBTLvlOrder(root->left, level + 1, vals);
	printBTLvlOrder(root->right, level + 1, vals);
}

void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void printVector(vector<vector<int>> vec){
	int vecSize = vec.size();
	for (int i = 0; i < vecSize; i++){
		printVector(vec.at(i));
		printf("\n");
	}
}

void printBTOrders(TreeNode * in){
	printBT(in);
	vector<int> inorder = inorderTraversal(in);
	printf("\nIn: ");
	printVector(inorder);

	vector<int> preorder = preorderTraversal(in);
	printf("\npre: ");
	printVector(preorder);
	printf("\n");

	vector<int> postorder = postorderTraversal(in);
	printf("\nPost: ");
	printVector(postorder);
	printf("\n");
}

void printIsValidBSTTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	if (isValidBST(in)){
		printf("\noutput: True");
	}
	else{
		printf("\noutput: False");

	}
	printf("\n\n");
	TestNo++;
}

void printBSTIteratorTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	BSTIterator it = BSTIterator(in);
	printf("Out: \n");
	while (it.hasNext()){
		printf("(%d) ", it.next());
	}

	printf("\n\n");
	TestNo++;
}

void printInsertBSTTest(TreeNode * in){

	printf("\nTestNo: %d\n%d\nInputRoot: ", TestNo);
	printBT(in); 
	
	vector<int> preorder = preorderTraversal(in);
	printf("\nInputVec: \n");
	printVector(preorder);

	vector<int>::iterator pit = preorder.begin();
	TreeNode * root = NULL;

	while (pit != preorder.end()){
		root = insertIntoBST(root, *pit);
		pit++;
	}

	printf("\nOutput: \n");
	printBT(root);

	printf("\n\n");
	TestNo++;
}

void printDeleteNodeTest(TreeNode * in, int key){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	printf("\nInputKey: %d\n", key);

	TreeNode* out = deleteNode(in, key);

	printf("\nOutput: \n");
	printBT(out);

	printf("\n\n");
	TestNo++;
}

void printLCATest(TreeNode* in, TreeNode* p, TreeNode* q){
	printf("\nTestNo: %d\nInputRoot: \n", TestNo);
	printBT(in);

	printf("\nP: (%d) Q: (%d)", p->val, q->val);
	TreeNode * out = lowestCommonAncestor(in, p, q);

	printf("\noutput: %d", out->val);
	printf("\n\n");

	TestNo++;
}

void printCNALDTest(vector<int> nums, int k, int t){
	printf("\nTestNo: %d\nInputNums: ", TestNo);
	printVector(nums);

	printf("\nInputK: %d T: %d\n", k, t);

	if (containsNearbyAlmostDuplicate(nums, k, t)){
		printf("\noutput: True");
	}
	else{
		printf("\noutput: False");

	}

	printf("\n\n");
	TestNo++;
	
}

void printIsBalancedTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	if (isBalanced(in)){
		printf("\noutput: True");
	}
	else{
		printf("\noutput: False");
	}
	printf("\n\n");
	TestNo++;
}

void printSortedArrayToBSTTest(vector<int> in){
	printf("\nTestNo: %d\nInputVec: \n", TestNo);
	printVector(in);

	TreeNode * out = sortedArrayToBST(in);

	printf("\nOutput: \n");
	printBT(out);

	printf("\n\n");
	TestNo++;
}
