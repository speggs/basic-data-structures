#ifndef PRINTERS_H
#define PRINTERS_H

#include <string>
#include <stdio.h>
#include <iostream>

#include "binarySearchTree.h"

//printers
void printBT(TreeNode * root);
void printBTLvlOrder(TreeNode * root, int level, std::vector<std::vector<int>>& vals);
void printVector(std::vector<int> vec);
void printVector(std::vector<std::vector<int>> vec);
void printBTOrders(TreeNode * in);

//test printers
void printIsValidBSTTest(TreeNode * in);
void printBSTIteratorTest(TreeNode * in);
void printInsertBSTTest(TreeNode * in);
void printDeleteNodeTest(TreeNode * in, int key);
void printLCATest(TreeNode* in, TreeNode* p, TreeNode* q);
void printCNALDTest(std::vector<int> nums, int k, int t);
void printIsBalancedTest(TreeNode * in);
void printSortedArrayToBSTTest(std::vector<int> in);

#endif //PRINTERS_H