#include "utils.h"
using namespace std;

TreeNode * makeBST(int height){
	if (height <= 0){
		return NULL;
	}
	int rtVal = pow(2, (height - 1));
	TreeNode * root = new TreeNode(rtVal);
	addChildren(root, height - 1);
	return root;
}

void addChildren(TreeNode * root, int height){
	if (height <= 0){
		return;
	}
	int diff = pow(2, (height - 1));
	int lVal = root->val - diff;
	int rVal = root->val + diff;
	root->left = new TreeNode(lVal);
	root->right = new TreeNode(rVal);
	addChildren(root->left, height - 1);
	addChildren(root->right, height - 1);
}

vector<int> preorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecPre(ret, root);
	return ret;
}

void addToVecPre(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}
	vec.push_back(root->val);
	addToVecPre(vec, root->left);
	addToVecPre(vec, root->right);
}

vector<int> inorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecIn(ret, root);
	return ret;
}

void addToVecIn(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}

	addToVecIn(vec, root->left);
	vec.push_back(root->val);
	addToVecIn(vec, root->right);
}

vector<int> postorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecPost(ret, root);
	return ret;
}

void addToVecPost(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}
	addToVecPost(vec, root->left);
	addToVecPost(vec, root->right);
	vec.push_back(root->val);
}

vector<vector<int>> levelOrder(TreeNode* root) {
	queue<TreeNode *> toAdd;
	queue<int> toAddLevels;
	int level = 0;
	vector<vector<int>> vals = vector<vector<int>>();

	if (root == NULL){
		return vals;
	}

	toAdd.push(root);
	toAddLevels.push(level);

	while (!toAdd.empty()){
		TreeNode * curNode = toAdd.front();
		toAdd.pop();
		int curLevel = toAddLevels.front();
		toAddLevels.pop();

		if (curNode->left != NULL){
			toAdd.push(curNode->left);
			toAddLevels.push(curLevel + 1);
		}
		if (curNode->right != NULL){
			toAdd.push(curNode->right);
			toAddLevels.push(curLevel + 1);
		}

		if (vals.size() <= curLevel){
			vals.push_back(vector<int>());
		}
		vals.at(curLevel).push_back(curNode->val);
	}

	return vals;
}