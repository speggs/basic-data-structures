#include "binarySearchTree.h"
using namespace std;

bool isValidBST(TreeNode* root) {
	if (root == NULL){
		return true;
	}
	vector<int> inorder = inorderTraversal(root);
	if (inorder.size() == 1){
		return true;
	}
	vector<int>::iterator it = inorder.begin();
	int prev = *it;
	it++;
	while (it != inorder.end()){
		if (prev >= *it){
			return false;
		}
		prev = *it;
		it++;
	}
	return true;
}

BSTIterator::BSTIterator(TreeNode *root){
	inorder = inorderTraversal(root);
	bSTit = inorder.begin();
}

bool BSTIterator::hasNext(){
	return bSTit != inorder.end();
}

int BSTIterator::next(){
	int ret = *bSTit;
	bSTit++;
	return ret;
}

TreeNode* searchBST(TreeNode* root, int val) {
	if (root == NULL){
		return NULL;
	}
	if (root->val == val){
		return root;
	} else if (val < root->val){
		return searchBST(root->left, val);
	} else {
		return searchBST(root->right, val);
	}
}

TreeNode* insertIntoBST(TreeNode* root, int val) {
	if (root == NULL){
		root = new TreeNode(val);
		return root;
	}
	if (root->val == val){
		return root;
	}else if (val < root->val){
		if (root->left == NULL){
			root->left = new TreeNode(val);
		}
		else{
			insertIntoBST(root->left, val);
		}
	}
	else{
		if (root->right == NULL){
			root->right = new TreeNode(val);
		}
		else{
			insertIntoBST(root->right, val);
		}
	}
	return root;
}

TreeNode * orphanAndReturnSuccessor(TreeNode * root){
	if (root->left == NULL){
		return root;
	}
	TreeNode* curParent = root;
	TreeNode* curNode = root->left;
	while (curNode->left != NULL){
		curNode = curNode->left;
		curParent = curParent->left;
	}
	curParent->left = curNode->right;
	return curNode;
}

TreeNode* deleteNode(TreeNode* root, int key) {
	if (root == NULL){
		return NULL;
	}

	if (key < root->val){
		root->left = deleteNode(root->left, key);
	}
	else if (root->val < key){
		root->right = deleteNode(root->right, key);
	}

	if (root->val == key){
		if (root->left != NULL && root->right != NULL){
			TreeNode * ret = orphanAndReturnSuccessor(root->right);
			ret->left = root->left;
			if (ret != root->right){
				ret->right = root->right;
			}
			delete root;
			return ret;
		}
		else if (root->left != NULL){
			TreeNode * ret = root->left;
			delete root;
			return ret;
		}
		else if (root->right != NULL){
			TreeNode * ret = root->right;
			delete root;
			return ret;
		}
		else {
			delete root;
			return NULL;
		}
	}
	return root;
}

TreeNodeCount* insertIntoBSCT(TreeNodeCount* root, int val) {
	if (root == NULL){
		root = new TreeNodeCount(val);
		return root;
	}
	if (root->val == val){
		root->count++;
		root->dups++;
		return root;
	}
	else if (val < root->val){
		if (root->left == NULL){
			root->left = new TreeNodeCount(val);
			root->count++;
		}
		else{
			root->count++;
			insertIntoBSCT(root->left, val);
		}
	}
	else{
		if (root->right == NULL){
			root->right = new TreeNodeCount(val);
			root->count++;
		}
		else{
			root->count++;
			insertIntoBSCT(root->right, val);
		}
	}
	return root;
}

int findKthLargest(TreeNodeCount * root, int k){
	int kcn = 0;
	if (root->right == NULL){
		kcn = 1;
	}
	else{
		kcn = 1 + root->right->count;
	}

	if (kcn <= k && k <= kcn + (root->dups - 1)){
		return root->val;
	}
	else if (k < kcn){
		return findKthLargest(root->right, k);
	}
	else{
		return findKthLargest(root->left, k - (kcn + root->dups - 1));
	}
}

KthLargest::KthLargest(int k, vector<int> nums){
	key = k;
	if (nums.size() == 0){
		root = NULL;
	}
	else{
		vector<int>::iterator it = nums.begin();
		root = new TreeNodeCount(*it);
		it++;
		while (it != nums.end()){
			insertIntoBSCT(root, *it);
			it++;
		}
	}
}

int KthLargest::add(int val){
	if (root == NULL){
		root = new TreeNodeCount(val);
		return val;
	}

	insertIntoBSCT(root, val);
	return findKthLargest(root, key);
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
	TreeNode* curNode = root;
	if (root == NULL){
		return NULL;
	}

	if (root == p || root == q){
		return root;
	}
	
	if (p->val < root->val && q->val < root->val){
		return lowestCommonAncestor(root->left, p, q);
	}

	if (root->val < p->val && root->val < q->val){
		return lowestCommonAncestor(root->right, p, q);
	}

	return root;
}

bool containsNearbyAlmostDuplicate(vector<int>& nums, int k, int t) {
	int numsSize = nums.size();
	for (int i = 0; i < k && i < numsSize; i++){
		for (int j = i + 1; j <= k && j < numsSize; j++){
			signed long long int li = nums.at(i);
			signed long long int lj = nums.at(j);
			if (abs(li - lj) <= t){
				return true;
			}
		}
	}
	for (int i = 0; (i + k) < numsSize; i++){
		signed long long int newNum = nums.at(i + k);
		for (int j = i; j < (i + k) && j < numsSize; j++){
			signed long long int lj = nums.at(j);
			if (abs(newNum - lj) <= t){
				return true;
			}
		}
	}
	return false;
}

bool isBalanced(TreeNode* root) {
	if (root == NULL){
		return true;
	}
	int depthL = maxDepth(root->left);
	int depthR = maxDepth(root->right);
	if (abs(depthL - depthR) > 1){
		return false;
	}
	else{
		return isBalanced(root->left) && isBalanced(root->right);
	}
	return true;
}

int maxDepth(TreeNode* root) {
	if (root == NULL){
		return 0;
	}

	int maxL = maxDepth(root->left);
	int maxR = maxDepth(root->right);
	return max(maxL, maxR) + 1;
}

int minDepth(TreeNode* root) {
	if (root == NULL){
		return 0;
	}

	int minL = minDepth(root->left);
	int minR = minDepth(root->right);
	return min(minL, minR) + 1;
}

TreeNode * addChildrenVec(vector<int> nums, int start, int end){
	int mid = ceil((start + end) / 2);
	if (start > end){
		return NULL;
	}
	TreeNode * ret = new TreeNode(nums.at(mid));
	if (start == end){
		return ret;
	}
	if (mid != start){
		ret->left = addChildrenVec(nums, start, mid - 1);
	}
	if (mid != end){
		ret->right = addChildrenVec(nums, mid + 1, end);
	}
	return ret;
}

TreeNode* sortedArrayToBST(vector<int>& nums) {
	if (nums.size() == 0){
		return NULL;
	}

	int mid = ceil(nums.size() / 2);
	TreeNode * root = new TreeNode(nums.at(mid));
	if (mid != 0){
		root->left = addChildrenVec(nums, 0, mid - 1);
	}
	if (mid != nums.size() - 1){
		root->right = addChildrenVec(nums, mid + 1, nums.size() - 1);
	}
	return root;
}