import java.util.Scanner;

 class BTNode{
    BTNode left, right;
    int data;
    int height;
    
    public BTNode(){
    	left = null;
    	right = null;
    	data = 0;
    	height = 0;
    }
    
    BTNode(int n){
    	left = null;
    	right = null;
    	data = n;
    	height = 0;
    }
}
 
 /*
 *	Pblm : given array of values, create BALANCE bi tree and print each row of tree in order.
 * sln: 1. Breadth first search algo to create binary tree.
		2. Breadth first search algo to travers tree and add nodes
		https://www.sanfoundry.com/java-program-implement-self-balancing-binary-search-tree/
 */
 
 class BT{
	private BTNode root;
	String[] rows;
     public BT(){
    	 root = null;  
     }

     public boolean isEmpty(){ 
    	 return root == null;
     }
     
     public void clear(){
    	 root = null;
    	 //GC will get this right?
     }
	 
     public void insert(int data) { 
    	 root = insert(data, root);
	}
     
    private int height(BTNode node){
    	if(node == null){
    		return -1;
    	}else{
    		return node.height;
    	}
    }
	
    private int max(int lhs, int rhs){
    	return lhs > rhs ? lhs : rhs;
    }
    
	private BTNode insert(int data, BTNode tree){
		//if tree is empty just insert here
		if(tree == null){
			tree = new BTNode(data);
			//if tree not empty, insert to left or right based on value
		}else if(data < tree.data){
			tree.left = insert(data, tree.left);
			//if left insertion of new node has ubalanced tree
			if(height(tree.left) - height(tree.right) == 2){
				if(data < tree.left.data){
					//if the node was inserted left of left, we pull up left up to parent
					tree = rotateWithLeftChild(tree);
				}else{
					//if the node was inserted right of left, we pull up LR, put L and P as LR's kids,
					//and put LR's old kids and P and L's new kids
					tree = doubleWithLeftChild(tree);
				}
			}
		}else if(data > tree.data){//data greater than current node
			tree.right = insert(data, tree.right);
			//right insertion has unbalanced tree
			if(height(tree.right) - height(tree.left) == 2){
				if(data < tree.right.data){
					//If inserted left of right, pull up RL making P and R it's new L and R
					//Then pull up RL's old kids, RLL and RLR as new kids of P and R respectively
					tree = doubleWithRightChild(tree);
				}else{
					//If inserted right of right, pull up right tree
					tree = rotateWithRightChild(tree);
				}
			}
		}else{
			;
		}
		tree.height = max(height(tree.left), height(tree.right)) + 1;
		return tree;
	} 
	
//	1 P's new left is LC's old right
//	2 LC's new right is P
//	3 Return LC
//	 replaces parent with a lower value
//			P				LC
//		   /				  \
//		  LC		==>		   P
//	      \				      /
//		   LCRC			    LCRC
	private BTNode rotateWithLeftChild(BTNode parent){
		BTNode lChild = parent.left;
		parent.left = lChild.right;
		lChild.right = parent;
		
		parent.height = max(height(parent.left), height(parent.right)) + 1;
		lChild.height = max(height(lChild.left), height(lChild.right)) + 1;
		
		return lChild;
	}
	
	private BTNode rotateWithRightChild(BTNode parent){
		BTNode rChild = parent.right;
		parent.right = rChild.left;
		rChild.left = parent;
		
		parent.height = max(height(parent.left), height(parent.right)) + 1;
		rChild.height = max(height(rChild.left), height(rChild.right)) + 1;
		
		return rChild;
	}
	
	// 1 replace LC with higher value
	// 2 replace parent with new LC
	// new height is old height - 1
	//		   P				P			 LR		
	//		  /               / 		   /   \	
	//		L				LR  		  L	    P
	//	   	 \		  		/ \	 	      \		/	
	//	  	 LR		==>	   L  LRR 	==>	  LRL  LRR	
	//      /	 \	   		\
	//	  LRL  LRR		    LRL
	private BTNode doubleWithLeftChild(BTNode parent){
		parent.left = rotateWithRightChild(parent.left);
		return rotateWithLeftChild(parent);
	}
	
	//1 replace RC with lower value
	//2 replace parent with new RC
	private BTNode doubleWithRightChild(BTNode parent){
		parent.right = rotateWithLeftChild(parent.right);
		return rotateWithRightChild(parent);
	}

 public int countNodes()
 {
     return countNodes(root);
 }

 private int countNodes(BTNode root)
 {
     if (root == null)
         return 0;
     else
     {
         int count = 1;
         count += countNodes(root.left);
         count += countNodes(root.right);
         return count;
     }
 }
 
 public boolean search(int val)
 {
     return search(root, val);
 }
 
 private boolean search(BTNode root, int val)
 {
	 System.out.println("\nsearching for "+ val);
 if(root == null){
 System.out.println("\nnull");
		 return false;
	 }
     if (root.data == val){
         return true;
	 }else if(val < root.data && root.left != null){
		 return search(root.left, val);
	 }else if(val > root.data && root.right != null){
		 return search(root.right, val);
	 }
     return false;         
 }
 
 public void inorder()
 {
    System.out.print("inorder: ");
    inorder(root);
 }
 
 private void inorder(BTNode root)
 {
     if (root != null)
     {
         inorder(root.left);
         System.out.print(root.data +" ");
         inorder(root.right);
     }
 }
 
 public void preorder()
 {
    System.out.print("preorder: ");
     preorder(root);
 }
 private void preorder(BTNode root)
 {
     if (root != null)
     {
         System.out.print(root.data +" ");
         preorder(root.left);             
         preorder(root.right);
     }
 }
 
 public void postorder()
 {
    System.out.print("postorder: ");
     postorder(root);
 }
 private void postorder(BTNode root)
 {
     if (root != null)
     {
         postorder(root.left);             
         postorder(root.right);
         System.out.print(root.data +" ");
     }
 }
 
 public void printRows(){
	 rows = new String[height(root) + 1];
	 String spacing = "";
	 for(int i = 0; i < rows.length; i++){
		 rows[i] = "";
	 }
	 getRows(root);
	 for(int i = 0; i < rows.length; i+=2){
		 spacing += "   ";
	 }
	 
	 for(int i = 0; i < rows.length; i++){
		 System.out.print(spacing.substring(0, spacing.length() - ((i*2) < spacing.length()? (i*2) : spacing.length())));
		 System.out.println(rows[i]);
	 }
 }
 
 private void getRows(BTNode root){
	 if(root != null){
		 getRows(root.left);
		 getRows(root.right);
		 rows[rows.length - root.height - 1] += ""+ root.data + ", ";
	 }
 }

public String solvePblm(int[] input){
	for(int i = 0; i < input.length; i++){
		insert(input[i]);
	}
	printRows();
	return "";
}

public String addArray(int[] input){
	for(int i = 0; i < input.length; i++){
		insert(input[i]);
	}
		inorder();
		System.out.println("");
		preorder();
		System.out.println("");
		postorder();
		System.out.println("");
		return "";
	}
}

 
 public class BTProblem
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
		
		
//		int[] input = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26};
		int[] input = {13,14,12,15,11,16,10,17,9,18,8,19,7,20,6,21,5,22,4,23,3,24,2,25,1,26};
		String output = "";

/* Creating object of BT */
BT bt = new BT(); 
/*  Perform tree operations  */
System.out.println("Binary Tree Test\n");          
char ch;        
do    
{
    System.out.println("\nBinary Tree Operations\n");
    System.out.println("1. insert ");
    System.out.println("2. search");
    System.out.println("3. count nodes");
    System.out.println("4. check empty");
    System.out.println("5. solvePblm");
    System.out.println("6. addArray");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer element to insert");
        bt.insert( scan.nextInt() );                     
        break;                          
    case 2 : 
        System.out.println("Enter integer element to search");
        System.out.println("Search result : "+ bt.search( scan.nextInt() ));
        break;                                          
    case 3 : 
        System.out.println("Nodes = "+ bt.countNodes());
        break;     
    case 4 : 
        System.out.println("Empty status = "+ bt.isEmpty());
        break;  
	case 5 :
        System.out.println("solvePblm = "+ bt.solvePblm(input));
		break;
	case 6 :
        System.out.println("addArray = "+ bt.addArray(input));
		break;
    default : 
        System.out.println("Wrong Entry \n ");
        break;   
    }
    /*  Display tree  */ 
   // System.out.print("\nPost order : ");
   // bt.postorder();
   // System.out.print("\nPre order : ");
    //bt.preorder();
    System.out.print("\nIn order : ");
            bt.inorder();
 
            // System.out.println("\n\nDo you want to continue (Type y or n) \n");
    // ch = scan.next().charAt(0);    
// } while (ch == 'Y'|| ch == 'y');        
		} while(true);
    }
 }