import java.util.Scanner;

 class BTNode{
    BTNode left, right;
    int data;
    
    public BTNode(){
        left = null;
        right = null;
        data = 0; 
	}
    
    public BTNode(int n) {
         left = null;
         right = null;
         data = n;
		 System.out.println("\nnew node created with value " + n);
	}
    
     public void setLeft(BTNode n){left = n;}

     public void setRight(BTNode n){right = n;}

     public BTNode getLeft() { return left; }

     public BTNode getRight()  { return right; }

     public void setData(int d) { data = d;}

     public int getData() { return data;}
}
 
 /*
 *	Pblm : given array of values, create BALANCE bi tree and print each row of tree in order.
 * sln: 1. Breadth first search algo to create binary tree.
		2. Breadth first search algo to travers tree and add nodes
		
 */
 
 class BT
 {
     private BTNode root;
	public int depth = 0;
	private int insDepth = 0;
	
     public BT()
     {root = null;  }

     public boolean isEmpty()  { return root == null;}
	 
     public void insert(int data) { 
			if(root == null){
				root = new BTNode(data);
				depth = 1;
			}else{
				insDepth = 1;
				insert(data, root);
				if(insDepth > depth){
					System.out.println("New Depth: " + insDepth);
					depth = insDepth;
					insDepth = 0;
				}
			}
		}
		
		private void insert(int data, BTNode parent){
			insDepth++;
			if (data < parent.getData()){
				if(parent.getLeft() == null){
					parent.setLeft(new BTNode(data));
				}else{
					insert(data, parent.getLeft());
				}
			}else if (data > parent.getData()){
				if(parent.getRight() == null){
					parent.setRight(new BTNode(data));
				}else{
					insert(data, parent.getRight());
				}
			}
		} 

     public int countNodes()
     {
         return countNodes(root);
     }

     private int countNodes(BTNode r)
     {
         if (r == null)
             return 0;
         else
         {
             int l = 1;
             l += countNodes(r.getLeft());
             l += countNodes(r.getRight());
             return l;
         }
     }
	 
     public boolean search(int val)
     {
         return search(root, val);
     }
	 
     private boolean search(BTNode r, int val)
     {
		 System.out.println("\nsearching for "+ val);
		 if(r == null){
		 System.out.println("\nnull");
			 return false;
		 }
         if (r.getData() == val){
             return true;
		 }else if(val < r.getData() && r.getLeft() != null){
			 return search(r.getLeft(), val);
		 }else if(val > r.getData() && r.getRight() != null){
			 return search(r.getRight(), val);
		 }
         return false;         
     }
	 
     public void inorder()
     {
        System.out.print("inorder: ");
         inorder(root);
     }
	 
     private void inorder(BTNode r)
     {
         if (r != null)
         {
             inorder(r.getLeft());
             System.out.print(r.getData() +" ");
             inorder(r.getRight());
         }
     }
	 
     public void preorder()
     {
        System.out.print("preorder: ");
         preorder(root);
     }
     private void preorder(BTNode r)
     {
         if (r != null)
         {
             System.out.print(r.getData() +" ");
             preorder(r.getLeft());             
             preorder(r.getRight());
         }
     }
	 
     public void postorder()
     {
        System.out.print("postorder: ");
         postorder(root);
     }
     private void postorder(BTNode r)
     {
         if (r != null)
         {
             postorder(r.getLeft());             
             postorder(r.getRight());
             System.out.print(r.getData() +" ");
         }
     }     

	public String solvePblm(int[] input){
		return "";
	}
	
	public String addArray(int[] input){
		for(int i = 0; i < input.length; i++){
			insert(input[i]);
		}
		inorder();
		System.out.println("");
		preorder();
		System.out.println("");
		postorder();
		System.out.println("");
		return "";
	}
}

 
 public class BinaryTreeProblem
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
		
		
		int[] input = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,22,23,24,25,26};
		
		String output = "";
		
        /* Creating object of BT */
        BT bt = new BT(); 
        /*  Perform tree operations  */
        System.out.println("Binary Tree Test\n");          
        char ch;        
        do    
        {
            System.out.println("\nBinary Tree Operations\n");
            System.out.println("1. insert ");
            System.out.println("2. search");
            System.out.println("3. count nodes");
            System.out.println("4. check empty");
            System.out.println("5. solvePblm");
            System.out.println("6. addArray");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer element to insert");
                bt.insert( scan.nextInt() );                     
                break;                          
            case 2 : 
                System.out.println("Enter integer element to search");
                System.out.println("Search result : "+ bt.search( scan.nextInt() ));
                break;                                          
            case 3 : 
                System.out.println("Nodes = "+ bt.countNodes());
                break;     
            case 4 : 
                System.out.println("Empty status = "+ bt.isEmpty());
                break;  
			case 5 :
                System.out.println("solvePblm = "+ bt.solvePblm(input));
				break;
			case 6 :
                System.out.println("addArray = "+ bt.addArray(input));
				break;
            default : 
                System.out.println("Wrong Entry \n ");
                break;   
            }
            /*  Display tree  */ 
           // System.out.print("\nPost order : ");
           // bt.postorder();
           // System.out.print("\nPre order : ");
            //bt.preorder();
            System.out.print("\nIn order : ");
            bt.inorder();
 
            // System.out.println("\n\nDo you want to continue (Type y or n) \n");
            // ch = scan.next().charAt(0);    
        // } while (ch == 'Y'|| ch == 'y');        
		} while(true);
    }
 }