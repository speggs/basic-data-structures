#include "utils.h"
using namespace std;

TreeNode * makeBT(int height){
	if (height <= 0){
		return NULL;
	}
	int rtVal = pow(2, (height - 1));
	TreeNode * root = new TreeNode(rtVal);
	addChildren(root, height - 1);
	return root;
}

void addChildren(TreeNode * root, int height){
	if (height <= 0){
		return;
	}
	int diff = pow(2, (height - 1));
	int lVal = root->val - diff;
	int rVal = root->val + diff;
	root->left = new TreeNode(lVal);
	root->right = new TreeNode(rVal);
	addChildren(root->left, height - 1);
	addChildren(root->right, height - 1);
}

TreeLinkNode * makeBTL(int height){
	if (height <= 0){
		return NULL;
	}

	queue<TreeLinkNode *> addTo;
	queue<int> addToLevels;
	int nodeNum = 1;

	TreeLinkNode * root = new TreeLinkNode(nodeNum++);
	addTo.push(root);
	addToLevels.push(1);

	while (1){
		TreeLinkNode * curNode = addTo.front();
		addTo.pop();
		int curLevel = addToLevels.front();
		addToLevels.pop();
		if (curLevel >= height){
			break;
		}

		curNode->left = new TreeLinkNode(nodeNum++);
		addTo.push(curNode->left);
		addToLevels.push(curLevel + 1);
		
		curNode->right = new TreeLinkNode(nodeNum++);
		addTo.push(curNode->right);
		addToLevels.push(curLevel + 1);
	}
	return root;
}

TreeNode * makeBTSym(int height){
	if (height <= 0){
		return NULL;
	}
	int rtVal = pow(2, (height - 1));
	TreeNode * root = new TreeNode(rtVal);
	addChildrenSym(root, height - 1);
	return root;
}

void addChildrenSym(TreeNode * root, int height){
	if (height <= 0){
		return;
	}
	int diff = pow(2, (height - 1));
	int lVal = diff;
	int rVal = diff;
	root->left = new TreeNode(lVal);
	root->right = new TreeNode(rVal);
	addChildrenSym(root->left, height - 1);
	addChildrenSym(root->right, height - 1);
}