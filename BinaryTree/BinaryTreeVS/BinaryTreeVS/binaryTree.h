#ifndef BINARYTREE_H
#define BINARYTREE_H

#include <stddef.h>
#include "stddef.h"
#include <vector>
#include <queue>
#include <unordered_map>

struct TreeNode {
	int val;
	TreeNode *left;
	TreeNode *right;
	TreeNode(int x) : val(x), left(NULL), right(NULL) {}
};

struct TreeLinkNode {
	int val;
	TreeLinkNode *left, *right, *next;
	TreeLinkNode(int x) : val(x), left(NULL), right(NULL), next(NULL) {}
};

#include "printers.h"
#include "utils.h"

std::vector<int> preorderTraversal(TreeNode* root);
void addToVecPre(std::vector<int>& vec, TreeNode* root);

std::vector<int> inorderTraversal(TreeNode* root);
void addToVecIn(std::vector<int>& vec, TreeNode* root);

std::vector<int> postorderTraversal(TreeNode* root);
void addToVecPost(std::vector<int>& vec, TreeNode* root);

std::vector<std::vector<int>> levelOrder(TreeNode* root);

bool isSymmetric(TreeNode* root);
std::vector<std::vector<int>> levelOrderNulls(TreeNode* root);

bool hasPathSum(TreeNode* root, int sum);

TreeNode* buildTree(std::vector<int>& inorder, std::vector<int>& postorder);

TreeNode* buildTree(std::vector<int>& inorder, std::vector<int>& postorder, int start, int end, int& post);

TreeNode* buildTree2(std::vector<int>& inorder, std::vector<int>& preorder);
TreeNode* buildTree2(std::vector<int>& inorder, std::vector<int>& preorder, int start, int end, int &pre);

void connect(TreeLinkNode *root);

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q);
std::vector<TreeNode*> pathToNode(TreeNode * root, TreeNode * p);
std::vector<TreeNode*> reconstructPath(TreeNode * goal, std::unordered_map<TreeNode*, TreeNode*> pathsMap);

std::string serialize(TreeNode* root);
std::vector<std::string> splitString(std::string str, std::string delim);
TreeNode* deserialize(std::string data);
TreeNode* strToNode(std::string nodeVal);


#endif