#include "binaryTree.h"
using namespace std;


vector<int> preorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecPre(ret, root);
	return ret;
}

void addToVecPre(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}
	vec.push_back(root->val);
	addToVecPre(vec, root->left);
	addToVecPre(vec, root->right);
}

vector<int> inorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecIn(ret, root);
	return ret;
}

void addToVecIn(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}

	addToVecIn(vec, root->left);
	vec.push_back(root->val);
	addToVecIn(vec, root->right);
}

vector<int> postorderTraversal(TreeNode* root) {
	vector<int> ret = vector<int>();
	addToVecPost(ret, root);
	return ret;
}

void addToVecPost(vector<int>& vec, TreeNode* root){
	if (root == NULL){
		return;
	}
	addToVecPost(vec, root->left);
	addToVecPost(vec, root->right);
	vec.push_back(root->val);
}

vector<vector<int>> levelOrder(TreeNode* root) {
	queue<TreeNode *> toAdd;
	queue<int> toAddLevels;
	int level = 0;
	vector<vector<int>> vals = vector<vector<int>>();
	
	if (root == NULL){
		return vals;
	}

	toAdd.push(root);
	toAddLevels.push(level);

	while (!toAdd.empty()){
		TreeNode * curNode = toAdd.front();
		toAdd.pop();
		int curLevel = toAddLevels.front();
		toAddLevels.pop();

		if (curNode->left != NULL){
			toAdd.push(curNode->left);
			toAddLevels.push(curLevel + 1);
		}
		if (curNode->right != NULL){
			toAdd.push(curNode->right);
			toAddLevels.push(curLevel + 1);
		}

		if (vals.size() <= curLevel){
			vals.push_back(vector<int>());
		}
		vals.at(curLevel).push_back(curNode->val);
	}

	return vals;
}

int maxDepth(TreeNode* root) {
	if (root == NULL){
		return 0;
	}

	int maxL = maxDepth(root->left);
	int maxR = maxDepth(root->right);
	return max(maxL, maxR) + 1;
}

bool isSymmetric(TreeNode* root) {
	vector<vector<int>> lvlOrder = levelOrderNulls(root);
	int levels = lvlOrder.size();
	for (int i = 0; i < levels; i++){

		vector<int> lvlVec = lvlOrder.at(i);
		int lvlSize = lvlVec.size();
		int k = lvlSize - 1;
		for (int j = 0; j < lvlSize && j < k; j++, k--){
			if (lvlVec.at(j) != NULL && lvlVec.at(k) != NULL){
				if (lvlOrder.at(i).at(j) != lvlOrder.at(i).at(k)){
					return false;
				}
			}
			else if (lvlVec.at(j) == NULL && lvlVec.at(k) == NULL){
			}
			else{
				return false;
			}
		}
	}
	return true;
}

vector<vector<int>> levelOrderNulls(TreeNode* root) {
	queue<TreeNode *> toAdd;
	queue<int> toAddLevels;
	int level = 0;
	vector<vector<int>> vals = vector<vector<int>>();

	if (root == NULL){
		return vals;
	}

	toAdd.push(root);
	toAddLevels.push(level);

	while (!toAdd.empty()){
		TreeNode * curNode = toAdd.front();
		toAdd.pop();
		int curLevel = toAddLevels.front();
		toAddLevels.pop();

		if (curNode != NULL){
			toAdd.push(curNode->left);
			toAddLevels.push(curLevel + 1);
		}
		if (curNode != NULL){
			toAdd.push(curNode->right);
			toAddLevels.push(curLevel + 1);
		}

		if (vals.size() <= curLevel){
			vals.push_back(vector<int>());
		}
		if (curNode != NULL){
			vals.at(curLevel).push_back(curNode->val);
		}
		else{
			vals.at(curLevel).push_back(NULL);
		}
	}

	return vals;
}


bool hasPathSum(TreeNode* root, int sum) {
	if (root == NULL){
		return false;
	}

	if (root->left == NULL && root->right == NULL){
		return sum == root->val;
	}

	bool ret = hasPathSum(root->left, sum - root->val) || hasPathSum(root->right, sum - root->val);
	return ret;
}

TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder) {
	int post = inorder.size() - 1;
	if (post == -1){
		return NULL;
	}
	return buildTree(inorder, postorder, 0, inorder.size() - 1, post);
}

TreeNode* buildTree(vector<int>& inorder, vector<int>& postorder, int start, int end, int &post){

	if (start > end){
		return NULL;
	}

	TreeNode* root = new TreeNode(postorder.at(post));
	post--;

	if (start == end){
		return root;
	}

	int pivotVal = root->val;
	int pivotInd = 0;

	for (int i = start; i <= end; i++){
		if (inorder.at(i) == pivotVal){
			pivotInd = i;
			break;
		}
	}
	root->right = buildTree(inorder, postorder, pivotInd + 1, end, post);
	root->left = buildTree(inorder, postorder, start, pivotInd - 1, post);

	return root;
}

TreeNode* buildTree2(vector<int>& inorder, vector<int>& preorder) {
	int pre = 0;
	if (inorder.size() == 0){
		return NULL;
	}
	return buildTree2(inorder, preorder, 0, inorder.size() - 1, pre);
}

TreeNode* buildTree2(vector<int>& inorder, vector<int>& preorder, int start, int end, int &pre){

	if (start > end){
		return NULL;
	}

	TreeNode* root = new TreeNode(preorder.at(pre));
	pre++;

	if (start == end){
		return root;
	}

	int pivotVal = root->val;
	int pivotInd = 0;

	for (int i = start; i <= end; i++){
		if (inorder.at(i) == pivotVal){
			pivotInd = i;
			break;
		}
	}

	root->left = buildTree2(inorder, preorder, start, pivotInd - 1, pre);
	root->right = buildTree2(inorder, preorder, pivotInd + 1, end, pre);

	return root;
}

void connect(TreeLinkNode *root) {
	if (root == NULL){
		return;
	}
	queue<TreeLinkNode *> toAdd;
	queue<int> toAddLevels;

	toAdd.push(root);
	toAddLevels.push(0);

	while (!toAdd.empty()){
		TreeLinkNode * curNode = toAdd.front();
		toAdd.pop();
		int curLevel = toAddLevels.front();
		toAddLevels.pop();

		if (curNode->left != NULL){
			toAdd.push(curNode->left);
			toAddLevels.push(curLevel + 1);
		}
		if (curNode->right != NULL){
			toAdd.push(curNode->right);
			toAddLevels.push(curLevel + 1);
		}

		if (!toAddLevels.empty() && toAddLevels.front() == curLevel){
			curNode->next = toAdd.front();
		}
		else{
			curNode->next = NULL;
		}
	}
}

TreeNode* lowestCommonAncestor(TreeNode* root, TreeNode* p, TreeNode* q) {
	if (root == NULL){
		return NULL;
	}

	if (p == root || q == root){
		return root;
	}

	vector<TreeNode*> pathP = pathToNode(root, p);
	vector<TreeNode*> pathQ = pathToNode(root, q);
	vector<TreeNode*>::reverse_iterator ritP = pathP.rbegin();
	vector<TreeNode*>::reverse_iterator ritQ = pathQ.rbegin();

	while (ritP != pathP.rend() && ritQ != pathQ.rend()){
		if (*ritP == *ritQ){
			++ritP;
			++ritQ;
		}
		else{
			--ritP;
			--ritQ;
			return *ritP;
		}
		if (ritP == pathP.rend() || ritQ == pathQ.rend()){
			if (*ritP != *ritQ){
				--ritP;
				--ritQ;
			}
			return *ritP;
		}
	}

	return NULL;
}

vector<TreeNode*> pathToNode(TreeNode * root, TreeNode * p){
	vector<TreeNode *> ret = vector<TreeNode*>();
	if (root == NULL){
		return ret;
	}
	queue<TreeNode *> toVisit;
	unordered_map<TreeNode*, TreeNode*> pathsMap;
	toVisit.push(root);

	while (!toVisit.empty()){
		TreeNode * curNode = toVisit.front();
		toVisit.pop();

		if (curNode->left != NULL){
			toVisit.push(curNode->left);
			pathsMap[curNode->left] = curNode;
			if (curNode->left == p){
				ret = reconstructPath(p, pathsMap);
				return ret;
			}
		}

		if (curNode->right != NULL){
			toVisit.push(curNode->right);
			pathsMap[curNode->right] = curNode;
			if (curNode->right == p){
				ret = reconstructPath(p, pathsMap);
				return ret;
			}
		}
	}
}

vector<TreeNode*> reconstructPath(TreeNode * goal, unordered_map<TreeNode*, TreeNode*> pathsMap){
	vector<TreeNode *> path = vector<TreeNode*>();
	path.push_back(goal);
	TreeNode * prev;
	if (pathsMap.count(goal) > 0){
		prev = pathsMap[goal];
		path.push_back(prev);
	}
	else{
		return path;
	}
	while (pathsMap.count(prev) > 0){
		prev = pathsMap[prev];
		path.push_back(prev);
	}
	return path;
}

// Encodes a tree to a single string.
string serialize(TreeNode* root) {
	string ret = "";//"]";
	queue<TreeNode*> toVisit;
	
	if (root == NULL){
		return "NULL";
	}

	toVisit.push(root);

	while (!toVisit.empty()){
		TreeNode* curNode = toVisit.front();
		toVisit.pop();
		if (curNode == NULL){
			ret += "NULL";
			if (!toVisit.empty()){
				ret += ",";
			}
			continue;
		}
		else{
			ret += to_string(curNode->val) + ",";
		}

		toVisit.push(curNode->left);
		toVisit.push(curNode->right);
	}
	//ret += "]";
	return ret;
}

// Decodes your encoded data to tree.
TreeNode* deserialize(string data) {
	vector<string> vals = splitString(data, ",");
	queue<TreeNode*> addTo;
	vector<string>::iterator valIt = vals.begin();
	TreeNode* root = strToNode(*valIt);
	if (root == NULL){
		return NULL;
	}

	addTo.push(root);
	if (valIt != vals.end()){
		valIt++;
	}
	else{
		return root;
	}

	while (!addTo.empty()){
		TreeNode* curNode = addTo.front();
		addTo.pop();

		curNode->left = strToNode(*valIt);
		if (valIt != vals.end()){
			valIt++;
		}
		else{
			return root;
		}
		if (curNode->left != NULL){
			addTo.push(curNode->left);
		}

		curNode->right = strToNode(*valIt);
		if (valIt != vals.end()){
			valIt++;
		}
		else{
			return root;
		}
		if (curNode->right != NULL){
			addTo.push(curNode->right);
		}
	}
	return root;
}

TreeNode* strToNode(string nodeVal){
	if (nodeVal.compare("NULL") == 0){
		return NULL;
	}

	TreeNode * ret = new TreeNode(atoi(nodeVal.c_str()));
	return ret;
}

vector<string> splitString(string str, string delim){
	size_t pos = 0;
	vector<string> ret = vector<string>();
	string tok;
	while ((pos = str.find(delim)) != string::npos){
		tok = str.substr(0, pos);
		ret.push_back(tok);
		str.erase(0, pos + delim.length());
	}
	ret.push_back(str);
	return ret;
}