#include "printers.h"
using namespace std;

int TestNo = 1;

void printBT(TreeNode * root){

	vector<vector<int>> vals = vector<vector<int>>();

	printBTLvlOrder(root, 0, vals);
	printf("\n\n");
	int lvls = vals.size();
	for (int i = 0; i < lvls; i++){
		for (int k = 0; k < lvls - i - 1; k++){
			printf("   ");
		}
		int lvlSize = vals.at(i).size();
		for (int j = 0; j < lvlSize; j++){
			printf("(%d) ", vals.at(i).at(j));
			for (int k = 0; k < lvls - i - 1; k++){
				printf("   ");
			}
		}
		printf("\n");
		vals.at(i).clear();
	}
	vals.clear();
}

void printBTLvlOrder(TreeNode * root, int level, vector<vector<int>>& vals){


	if (vals.size() <= level){
		vals.push_back(vector<int>());
	}
	if (root == NULL){
		vals.at(level).push_back(-1);
		return;
	}
	vals.at(level).push_back(root->val);

	printBTLvlOrder(root->left, level + 1, vals);
	printBTLvlOrder(root->right, level + 1, vals);
}

void printVector(vector<int> vec){
	printf("{");
	for (vector<int>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%d, ", *it);
	}
	printf("}");
}

void printVector(vector<vector<int>> vec){
	int vecSize = vec.size();
	for (int i = 0; i < vecSize; i++){
		printVector(vec.at(i));
		printf("\n");
	}
}

void printPreorderTraversalTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	vector<int> out = preorderTraversal(in);

	printf("\noutput Vec: ");
	printVector(out);
	printf("\n\n");

	TestNo++;
}

void printLevelOrderTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	vector<vector<int>> out = levelOrder(in);

	printf("\noutput Vec: ");
	printVector(out);
	printf("\n\n");

	TestNo++;
}

void printIsSymmetricTest(TreeNode * in){
	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	if (isSymmetric(in)){
		printf("\noutput: True");
	}
	else{
		printf("\noutput: False");

	}
	
	printf("\n\n");

	TestNo++;
}

void printHasPathToSumTest(TreeNode * in, int sum){

	printf("\nTestNo: %d\nInputSum: %d\nInputRoot: ", TestNo, sum);
	printBT(in);

	if (hasPathSum(in, sum)){
		printf("\noutput: True");
	}
	else{
		printf("\noutput: False");

	}

	printf("\n\n");

	TestNo++;
}

void printBTOrders(TreeNode * in){
	printBT(in);
	vector<int> inorder = inorderTraversal(in);
	printf("\nIn: ");
	printVector(inorder);

	vector<int> preorder = preorderTraversal(in);
	printf("\npre: ");
	printVector(preorder);
	printf("\n");

	vector<int> postorder = postorderTraversal(in);
	printf("\nPost: ");
	printVector(postorder);
	printf("\n");
}

void printBuildTreeTest(TreeNode * in){

	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	vector<int> inorder = inorderTraversal(in);
	printf("\nIn: ");
	printVector(inorder);

	vector<int> postorder = postorderTraversal(in);
	printf("\nPost: ");
	printVector(postorder);

	TreeNode * out = buildTree(inorder, postorder);
	printf("\nOutputRoot: ");
	printBT(out);

	TestNo++;
}

void printBuildTreeTest(vector<int> inorder, vector<int> postorder){

	printf("\nTestNo: %d", TestNo);

	printf("\nIn: ");
	printVector(inorder);

	printf("\nPost: ");
	printVector(postorder);

	TreeNode * out = buildTree(inorder, postorder);
	printf("\nOutputRoot: ");
	printBT(out);

	TestNo++;
}

void printBuildTree2Test(TreeNode * in){

	printf("\nTestNo: %d\nInputRoot: ", TestNo);
	printBT(in);

	vector<int> inorder = inorderTraversal(in);
	printf("\nIn: ");
	printVector(inorder);

	vector<int> preorder = preorderTraversal(in);
	printf("\nPre: ");
	printVector(preorder);

	TreeNode * out = buildTree2(inorder, preorder);
	printf("\nOutputRoot: ");
	printBT(out);

	TestNo++;
}

void printBuildTree2Test(vector<int> inorder, vector<int> preorder){

	printf("\nTestNo: %d", TestNo);

	printf("\nIn: ");
	printVector(inorder);

	printf("\nPre: ");
	printVector(preorder);

	TreeNode * out = buildTree2(inorder, preorder);
	printf("\nOutputRoot: ");
	printBT(out);

	TestNo++;
}

void printBTLLvlOrder(TreeLinkNode * root){
	if (root == NULL){
		printf("NULL");
		return;
	}

	queue<TreeLinkNode *> toPrint;
	queue<int> toPrintLevels;
	int printLevel = 0;
	toPrint.push(root);
	toPrintLevels.push(0);

	while (!toPrint.empty()){
		TreeLinkNode * curNode = toPrint.front();
		toPrint.pop();
		int curLevel = toPrintLevels.front();
		toPrintLevels.pop();
		
		if (curLevel > printLevel){
			printf("\n");
			printLevel = curLevel;
		}

		if (curNode->next != NULL){
			printf("(%d)->[%d] ", curNode->val, curNode->next->val);
		}
		else{
			printf("(%d)->[NULL] ", curNode->val);
		}

		if (curNode->left != NULL){
			toPrint.push(curNode->left);
			toPrintLevels.push(curLevel + 1);
		}

		if (curNode->right != NULL){
			toPrint.push(curNode->right);
			toPrintLevels.push(curLevel + 1);
		}
	}
}

void printConnectTest(TreeLinkNode * in){
	printf("\nTestNo: %d\nInput: \n", TestNo);
	printBTLLvlOrder(in);

	connect(in);

	printf("\nOutput: \n");
	printBTLLvlOrder(in);

	printf("\n\n");

	TestNo++;
}

void printLCATest(TreeNode* in, TreeNode* p, TreeNode* q){
	printf("\nTestNo: %d\nInputRoot: \n", TestNo);
	printBT(in);

	printf("\nP: (%d) Q: (%d)", p->val, q->val);
	TreeNode * out = lowestCommonAncestor(in, p, q);

	printf("\noutput: %d", out->val);
	printf("\n\n");

	TestNo++;
}

void printSerializeTest(TreeNode* in){
	printf("\nTestNo: %d\nInputRoot: \n", TestNo);
	printBT(in);

	string out = serialize(in);
	printf("\noutput: %s", out.c_str());
	printf("\n\n");
	
	TestNo++;
}

void printStrVec(vector<string> vec){
	printf("{");
	for (vector<string>::iterator it = vec.begin(); it != vec.end(); ++it){
		printf("%s, ", (*it).c_str());
	}
	printf("}");
}

void printSplitStringTest(TreeNode* in){
	printf("\nTestNo: %d\nInputRoot: \n", TestNo);
	printBT(in);

	string outSer = serialize(in);
	printf("\noutputSer: \n%s", outSer.c_str());
	printf("\n");

	vector<string> outSS = splitString(outSer, ",");
	printStrVec(outSS);
	printf("\n\n");

	TestNo++;
}

void printDeserializeTest(TreeNode* in){
	printf("\nTestNo: %d\nInputRoot: \n", TestNo);
	printBT(in);

	string outSer = serialize(in);
	printf("\noutputSer: \n%s", outSer.c_str());
	printf("\n");

	TreeNode * out = deserialize(outSer);
	printf("\noutRoot: \n");
	printBT(out);

	printf("\n\n");

	TestNo++;
}