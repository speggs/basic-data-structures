#ifndef UTILS_H
#define UTILS_H

#include "binaryTree.h"
#include <vector>

TreeNode * makeBT(int height);
void addChildren(TreeNode * root, int height);

TreeNode * makeBTSym(int height);
void addChildrenSym(TreeNode * root, int height);

TreeLinkNode * makeBTL(int height);

#endif //UTILS_H