#ifndef PRINTERS_H
#define PRINTERS_H

#include <string>
#include <stdio.h>
#include <iostream>
#include <vector>

#include "binaryTree.h"

//printers
void printBT(TreeNode * root);
void printBTLvlOrder(TreeNode * root, int level, std::vector<std::vector<int>>& vals);
void printVector(std::vector<int> vec);
void printVector(std::vector<std::vector<int>> vec);
void printBTOrders(TreeNode * in);
void printBTLLvlOrder(TreeLinkNode * root);

//test printers
void printPreorderTraversalTest(TreeNode * in);
void printLevelOrderTest(TreeNode * in);
void printIsSymmetricTest(TreeNode * in);
void printHasPathToSumTest(TreeNode * in, int sum);
void printBuildTreeTest(TreeNode * in);
void printBuildTreeTest(std::vector<int> inorder, std::vector<int> postorder);
void printBuildTree2Test(TreeNode * in);
void printBuildTree2Test(std::vector<int> inorder, std::vector<int> preorder);
void printConnectTest(TreeLinkNode * in);
void printLCATest(TreeNode* in, TreeNode* p, TreeNode* q);
void printSerializeTest(TreeNode* in);
void printSplitStringTest(TreeNode* in);
void printDeserializeTest(TreeNode* in);
#endif //PRINTERS_H