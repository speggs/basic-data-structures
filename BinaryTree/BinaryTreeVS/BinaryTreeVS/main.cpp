#include "binaryTree.h"
#include "printers.h"
#include "utils.h"
#include <stdio.h>

using namespace std;

void testMakeBT(){
	TreeNode * root = makeBT(2);
	printBT(root);
}

void testPreorderTraversal(){

	TreeNode * in = makeBT(1);
	printPreorderTraversalTest(in);

	TreeNode * in1 = makeBT(2);
	printPreorderTraversalTest(in1);

	TreeNode * in2 = makeBT(3);
	printPreorderTraversalTest(in2);

	TreeNode * in3 = makeBT(4);
	printPreorderTraversalTest(in3);

}

void testLevelOrder(){
	TreeNode* in = makeBT(3);
	printLevelOrderTest(in);
}

void testIsSymetric(){
	TreeNode * in = makeBT(4);
	printIsSymmetricTest(in);


	TreeNode * in2 = makeBTSym(4);
	printIsSymmetricTest(in2);
}

void testHasPathToSum(){
	TreeNode * in = makeBT(3);
	printHasPathToSumTest(in, 9);

	printHasPathToSumTest(in, 109);
}

void testBuildTree(){
	//TreeNode * in = makeBT(3);
	//printBuildTreeTest(in);

	vector<int> inorder = vector<int>({ 2, 1 });
	vector<int> postorder = vector<int>({ 2, 1 });
	printBuildTreeTest(inorder, postorder);

}

void testBuildTree2(){
	//TreeNode * in = makeBT(3);
	//printBuildTree2Test(in);

	//vector<int> inorder = vector<int>({ 2, 1 });
	//vector<int> preorder = vector<int>({ 1, 2 });
	//printBuildTree2Test(inorder, preorder);

	vector<int> inorder = vector<int>({ 9, 3, 15, 20, 7 });
	vector<int> preorder = vector<int>({ 3, 9, 20, 15, 7 });
	printBuildTree2Test(inorder, preorder);

}

void testConnect(){
	TreeLinkNode * in = makeBTL(4);
	printConnectTest(in);
}

void testLowestCommonAncestor(){
	//TreeNode* in = makeBT(3);
	//TreeNode* p = in->left->left;
	//TreeNode* q = in->right->right;
	//printLCATest(in, p, q);


	//TreeNode* in2 = makeBT(4);
	//TreeNode* p2 = in2->right->left->left;
	//TreeNode* q2 = in2->right->right->right;
	//printLCATest(in2, p2, q2);

	TreeNode* in3 = new TreeNode(1);
	in3->left = new TreeNode(2);
	printLCATest(in3, in3, in3->left);
}

void testSerialize(){
	TreeNode * in = makeBT(3);
	printSerializeTest(in);
}

void testSplitString(){
	TreeNode * in = makeBT(2);
	printSplitStringTest(in);
}

void testDeserialize(){
	//TreeNode * in = makeBT(2);
	//printDeserializeTest(in);

	//TreeNode *in1 = makeBT(1);
	//printDeserializeTest(in1);

	//TreeNode * in2 = new TreeNode(1);
	//in2->left = new TreeNode(2);
	//printDeserializeTest(in2);

	TreeNode * in4 = new TreeNode(1);
	in4->left = new TreeNode(2);
	in4->right = new TreeNode(3);
	in4->right->left = new TreeNode(4);
	in4->right->right = new TreeNode(5);
	printDeserializeTest(in4);
}

int main(){
	//testMakeBT();
	//testPreorderTraversal();
	//testLevelOrder();
	//testIsSymetric();
	//testHasPathToSum();
	//testBuildTree();
	//testBuildTree2();
	//testConnect();
	//testLowestCommonAncestor();

	//testSerialize();
	//testSplitString();
	testDeserialize();
}