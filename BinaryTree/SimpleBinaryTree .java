import java.util.Scanner;

 class BTNode{
    BTNode left, right;
    int data;
    
    public BTNode(){
        left = null;
        right = null;
        data = 0; 
	}
    
    public BTNode(int n) {
         left = null;
         right = null;
         data = n;
	}
    
     public void setLeft(BTNode n){left = n;}

     public void setRight(BTNode n){right = n;}

     public BTNode getLeft() { return left; }

     public BTNode getRight()  { return right; }

     public void setData(int d) { data = d;}

     public int getData() { return data;}
}
 
 class BT
 {
     private BTNode root;

     public BT()
     {root = null;  }

     public boolean isEmpty()  { return root == null;}
	 
     public void insert(int data) { 
			if(root == null){
				root = new BTNode(data);
			}else{
				insert(data, root);
			}
		}
		
		private void insert(int data, BTNode parent){
			if (data < parent.getData()){
				if(parent.getLeft() == null){
					parent.setLeft(new BTNode(data));
				}else{
					insert(data, parent.getLeft());
				}
			}else if (data > parent.getData()){
				if(parent.getRight() == null){
					parent.setRight(new BTNode(data));
				}else{
					insert(data, parent.getRight());
				}
			}
		} 
	 
     public boolean search(int val)
     {
         return search(root, val);
     }
	 
     private boolean search(BTNode r, int val)
     {
		 if(r == null){
			 return false;
		 }
         if (r.getData() == val){
             return true;
		 }else if(val < r.getData() && r.getLeft() != null){
			 return search(r.getLeft(), val);
		 }else if(val > r.getData() && r.getRight() != null){
			 return search(r.getRight(), val);
		 }
         return false;         
     }  
 }

 
 public class BinaryTree
 {
     public static void main(String[] args)
    {            
        Scanner scan = new Scanner(System.in);
        BT bt = new BT();      
        char ch;        
        do    
        {
            System.out.println("1. insert ");
            System.out.println("2. search");
            System.out.println("3. count nodes");
            System.out.println("4. check empty");
 
            int choice = scan.nextInt();            
            switch (choice)
            {
            case 1 : 
                System.out.println("Enter integer element to insert");
                bt.insert( scan.nextInt() );                     
                break;                          
            case 2 : 
                System.out.println("Enter integer element to search");
                System.out.println("Search result : "+ bt.search( scan.nextInt() ));
                break;                                          
            case 3 : 
                System.out.println("Nodes = "+ bt.countNodes());
                break;     
            case 4 : 
                System.out.println("Empty status = "+ bt.isEmpty());
                break;            
            default : 
                System.out.println("Wrong Entry \n ");
                break;   
            }
			
		} while(true);
    }
 }