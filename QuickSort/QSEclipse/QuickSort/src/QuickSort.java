
public class QuickSort {
	//Before: 1 2 4 5 4 2 1 4 5 2 1 3=pivot
	//After: 1 2 2 1 2 1 3=pivot 4 5 4 4 5
	int partition(int array[], int low, int high){
		//Pivot choice is arbitrary, here we pick high
		int pivot = array[high];
		int i = low - 1;
		
		//this loop goes through teh range low to high, swapping elem lower than pivot
		//to the first partition
		for(int j = low; j < high; j++){
			if(array[j] <= pivot){
				//initially i is zero, each time an item is found less than the pivot
				//i is incremented, so that when the pivot is swapped to i at the end it 
				//is in the correct position
				i++;
				
				swap(array, i, j);
				
			}
		}
		//lows are low, highs are high, now put the pivot between the two partitions
		swap(array, i+1, high);
		
		return i+1;
	}
	
	int reversePartition(int array[], int low, int high){
		//Pivot choice is arbitrary, here we pick high
		int pivot = array[high];
		int i = low - 1;
		
		//this loop goes through teh range low to high, swapping elem lower than pivot
		//to the first partition
		for(int j = low; j < high; j++){
			if(array[j] >= pivot){
				//initially i is zero, each time an item is found less than the pivot
				//i is incremented, so that when the pivot is swapped to i at the end it 
				//is in the correct position
				i++;
				
				swap(array, i, j);
				
			}
		}
		//lows are low, highs are high, now put the pivot between the two partitions
		swap(array, i+1, high);
		
		return i+1;
	}
	
	void sort(int array[], int low, int high){
		//we know we are done when low and high meet
		if(low < high){
			int partitionInd = partition(array, low, high);
			
			sort(array, low, partitionInd - 1);
			sort(array, partitionInd + 1, high);
		}
	}
	
	void sort(int array[]){
		sort(array, 0, array.length -1);
	}
	
	void reverseSort(int array[], int low, int high){
		//we know we are done when low and high meet
		if(low < high){
			int partitionInd = reversePartition(array, low, high);
			
			reverseSort(array, low, partitionInd - 1);
			reverseSort(array, partitionInd + 1, high);
		}
	}
	
	void reverseSort(int array[]){
		reverseSort(array, 0, array.length -1);
	}
	
	static void printArray(int array[]){
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i] + ", ");
		}
		System.out.println("");
	}
	
	static void printCharArray(int array[]){
		for(int i = 0; i < array.length; i++){
			System.out.print("" + (char)array[i] + ", ");
		}
		System.out.println("");
	}
	
	void swap(int array[], int i, int j){
		int temp = array[i];
		array[i] = array[j];
		array[j] = temp;
	}
	
	public static void main(String args[]){
		int input[] = {13, 14, 12, 11, 10, 9, 15, 16, 17,18, 8, 7, 6, 5, 19, 20, 21, 22, 4, 3, 2, 1, 23, 24, 25, 26};
		int input2[] = {'b','r','s','u','i','t','q','k','l','c','g','h','v','z','n','y','o','p','d','e','w','x','m','a','j','f'};

		QuickSort qs = new QuickSort();
		qs.sort(input);
		printArray(input);
		
		qs.reverseSort(input);
		printArray(input);
		
		qs.sort(input2);
		printCharArray(input2);
		
		qs.reverseSort(input2);
		printCharArray(input2);
	}
}
