class MergeSort{
    void merge(int arr[], int left, int mid, int right)
    {
        int temp1size = mid - left + 1;
        int temp2size = right - mid;
 
        int temp1[] = new int [temp1size];
        int temp2[] = new int [temp2size];
 
        for (int i=0; i<temp1size; ++i){
            temp1[i] = arr[left + i];
		}
        for (int j=0; j<temp2size; ++j){
            temp2[j] = arr[mid + 1+ j];
		}
 
        int i = 0, j = 0;
        int k = left;
        while (i < temp1size && j < temp2size) {
            if (temp1[i] <= temp2[j]){
                arr[k] = temp1[i];
                i++;
            } else {
                arr[k] = temp2[j];
                j++;
            }
            k++;
        }
 
        while (i < temp1size){
            arr[k] = temp1[i];
            i++;
            k++;
        }
 
        while (j < temp2size)
        {
            arr[k] = temp2[j];
            j++;
            k++;
        }
    }
 
	//sort arr from index left to index right
    void sort(int arr[], int left, int right)
    {
        if (left < right)
        {
            int mid = (left+right)/2;
 
			//called recursively down to array.length = 1
            sort(arr, left, mid);
            sort(arr , mid+1, right);
 
			//once array are only 1 number begin merging
            merge(arr, left, mid, right);
        }
    }
 
    static void printArray(int arr[])
    {
		
        System.out.print("index: ");
		for (int i=0; i < arr.length; ++i){
            System.out.print("(" + i + ") ");
		}
        System.out.println();
        System.out.print("array: ");
        for (int i=0; i < arr.length; ++i){
            System.out.print(arr[i] + ", ");
		}
        System.out.println();
    }
 
    public static void main(String args[])
    {
        int arr[] = {12, 11, 13, 5, 6, 7};
 
        System.out.println("Given Array");
        printArray(arr);
 
        MergeSort ob = new MergeSort();
        ob.sort(arr, 0, arr.length-1);
 
        System.out.println("\nSorted array");
        printArray(arr);
    }
}