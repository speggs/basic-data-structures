//O(nLogn) best worst average
//space: O(n)
// best for LL because pointers are easily rearrange
//ms is better for arrays because things are swapped in place and no extra space, faster access
//paradigm: divide and conquer
//TODO implement this for LL
public class MergeSort {
	
	void merge(int array[], int left, int mid, int right){
		int sizeLeft = mid - left + 1;
		int sizeRight = right - mid;
		
		int tempLeft[] = new int[sizeLeft];
		int tempRight[] = new int[sizeRight];
		
		//copy array from left to mid into tempLeft
		for(int i = 0; i < sizeLeft; i++){
			tempLeft[i] = array[left + i];
		}
		//copy array from mid + 1 to right into tempRight
		for(int j = 0; j < sizeRight; j++){
			tempRight[j] = array[mid + 1 + j];
		}
		
		int i = 0, j = 0;
		
		int k = left;
		//scan through both tempArrays until one is exhausted
		while( i < sizeLeft && j < sizeRight){
			//if left is less than right, copy it to left + incrs of k
			if(tempLeft[i] <= tempRight[j]){
				array[k] = tempLeft[i];
				i++;
			}else{//else copy over from tempRight
				array[k] = tempRight[j];
				j++;
			}
			k++;
		}
		//copy over whichever temp array was not exhausted into the remainder of left - right part of array
		while(i < sizeLeft){
			array[k] = tempLeft[i];
			k++;
			i++;
		}
		
		while(j < sizeRight){
			array[k] = tempRight[j];
			k++;
			j++;
		}
		
	}
	
	void reverseMerge(int array[], int left, int mid, int right){
		int sizeLeft = mid - left + 1;
		int sizeRight = right - mid;
		
		int tempLeft[] = new int[sizeLeft];
		int tempRight[] = new int[sizeRight];
		
		for(int i = 0; i < sizeLeft; i++){
			tempLeft[i] = array[left + i];
		}
		for(int i = 0; i < sizeRight; i++){
			tempRight[i] = array[mid + 1 + i];
		}
		
		int i = 0, j = 0;
		
		int k = left;
		while( i < sizeLeft && j < sizeRight){
			if(tempLeft[i] >= tempRight[j]){
				array[k] = tempLeft[i];
				i++;
			}else{
				array[k] = tempRight[j];
				j++;
			}
			k++;
		}
		
		while(i < sizeLeft){
			array[k] = tempLeft[i];
			k++;
			i++;
		}
		
		while(j < sizeRight){
			array[k] = tempRight[j];
			k++;
			j++;
		}
	}
	
	void sort(int[] array){
		sort(array, 0, array.length - 1);
	}
	
	void sort(int[] array, int left, int right){
		//recursive call until down to single value
		if(left < right){
			int mid = (left + right) / 2;
			
			sort(array, left, mid);
			sort(array, mid + 1, right);
			
			//once down to single value sort the length = 2 arrays,
			//then return up the call stack sorting the now sorted temp arrays
			merge(array, left, mid, right);
		}
	}
	
	void reverseSort(int[] array){
		reverseSort(array, 0, array.length - 1);
	}
	
	void reverseSort(int[] array, int left, int right){
		int mid = (left + right) / 2;
		if(left < right){
			reverseSort(array, left, mid);
			reverseSort(array, mid + 1, right);
			reverseMerge(array, left, mid, right);
		}
	}
	
	static void printArray(int array[]){
		for(int i = 0; i < array.length; i++){
			System.out.print("" + array[i] + ", ");
		}
		System.out.println("");
	}
	
	static void printCharArray(int array[]){
		for(int i = 0; i < array.length; i++){
			System.out.print("" + (char)array[i] + ", ");
		}
		System.out.println("");
	}
	
	public static void main(String args[]){
		int input[] = {13, 14, 12, 11, 10, 9, 15, 16, 17, 18, 8, 7, 6, 5, 19, 20, 21, 22, 4, 3, 2, 1, 23, 24, 25, 26};
		int input2[] = {'b','r','s','u','i','t','q','k','l','c','g','h','v','z','n','y','o','p','d','e','w','x','m','a','j','f'};

		MergeSort ms = new MergeSort();
		ms.sort(input);
		printArray(input);
		
		ms.reverseSort(input);
		printArray(input);
		
		ms.sort(input2);
		printCharArray(input2);
		
		ms.reverseSort(input2);
		printCharArray(input2);
	}
}
